<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManyFieldFromTransferEpins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_epins', function (Blueprint $table) {
            $table->integer('epin_id')->after('receiver_id');
			$table->string('epin')->after('epin_id');
			$table->string('epin_type')->after('epin');
			$table->string('epin_type_id')->nullable()->after('epin_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_epins', function (Blueprint $table) {
            //
        });
    }
}
