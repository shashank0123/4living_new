<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseeDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchisee_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reg_form');
            $table->string('profile_pic');
            $table->string('pan_card');
            $table->string('adhaar_front');
            $table->string('adhaar_back');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchisee_documents');
    }
}
