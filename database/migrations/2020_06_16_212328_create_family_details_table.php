<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('member_name')->nullable();
            $table->string('member_relation')->nullable();
            $table->string('member_date_of_birth')->nullable();
            $table->string('member_marital_status')->nullable();
            $table->timestamps();
        });

        Schema::table('member_detail', function (Blueprint $table) {
            $table->string('family_member')->default('0');
            $table->string('gender_type')->nullable();
            $table->string('fh_type')->nullable();
            $table->string('fh_name')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('occupation_name')->nullable();
            $table->string('occupation_type')->nullable();
            $table->string('occupation_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_details');

        Schema::table('member_detail', function (Blueprint $table) {
            //
        });

    }
}
