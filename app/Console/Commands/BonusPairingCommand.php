<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Member;
use App\PairingData;
use App\Repositories\BonusRepository;
use StdClass;

class BonusPairingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bonus:pairing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Pairing Daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = new BonusRepository;
        $memberlist = array();
        $totalmembers = Member::select('id', 'package_id', 'direct_id', 'position', 'parent_id')->get();
        foreach ($totalmembers as $key => $value) {
            $memberlist[$value->id] = $value;
        }
        foreach ($memberlist as $key => $value) {
            if (isset($memberlist[$value->parent_id])){
                $position = $value->position."_side";
                $memberlist[$value->parent_id]->$position = $value;
            }
        }
        foreach ($memberlist as $key => $value) {
            $data = $this->calculatepairingdata($memberlist, $value->id, $value->id);
            $pairingdata = PairingData::where('member_id', $value->id)->where('date', date('d-m-Y'))->first();
            if (!$pairingdata){
                $pairingdata = new PairingData;
                $pairingdata->member_id = $value->id;
                $pairingdata->date = date('d-m-Y');
            }
            $pairingdata->leftcount            = $data->leftcount;
            $pairingdata->rightcount            = $data->rightcount;
            $pairingdata->leftdirectcount       = $data->leftdirectcount;
            $pairingdata->rightdirectcount      = $data->rightdirectcount;
            $pairingdata->leftactivecount       = $data->leftactivecount;
            $pairingdata->rightactivecount      = $data->rightactivecount;
            $pairingdata->leftwashoutcount      = $data->leftwashoutcount;
            $pairingdata->rightwashountcount    = $data->rightwashountcount;
            $pairingdata->save();
            
        }

        Member::where('package_id', '>=', 2)->chunk(100, function ($members) use ($repo) {
            foreach ($members as $member) {
                $repo->calculatePairing($member);
            }
        });
        Member::where('package_id', '>=', 2)->chunk(100, function ($members) use ($repo) {
            foreach ($members as $member) {
                $repo->calculateDirectV2($member);
            }
        });

        // echo "Command done";
    }

    public function calculatepairingdata($memberlist, $id, $original_id)
    {
        $obj = new StdClass;
        
        $obj->leftcount              = 0; 
        $obj->leftdirectcount        = 0; 
        $obj->leftactivecount        = 0; 
        $obj->leftwashoutcount       = 0; 
        

        $obj->rightcount             = 0; 
        $obj->rightdirectcount       = 0; 
        $obj->rightactivecount       = 0; 
        $obj->rightwashountcount     = 0; 

        if (isset($memberlist[$id]->left_side)){
            $data = $this->calculatepairingdata($memberlist, $memberlist[$id]->left_side->id ,$original_id);
            $obj->leftcount = $data->leftcount + $data->rightcount + 1;
            if ($memberlist[$id]->package_id > 0){
                $obj->leftactivecount = $data->leftactivecount + $data->rightactivecount + 1;
            }
            else
                $obj->leftactivecount = $data->leftactivecount + $data->rightactivecount;

            if ($memberlist[$id]->direct_id == $original_id){
                $obj->leftdirectcount = $data->leftdirectcount + $data->rightdirectcount + 1;

            }
            else
                $obj->leftdirectcount = $data->leftdirectcount + $data->rightdirectcount;
        }
        else
            $obj->leftcount = 0;



        if (isset($memberlist[$id]->right_side)){
            $data = $this->calculatepairingdata($memberlist, $memberlist[$id]->right_side->id ,$original_id);
            $obj->rightcount = $data->leftcount + $data->rightcount + 1;
            if ($memberlist[$id]->package_id > 1){
                $obj->rightactivecount = $data->leftactivecount + $data->rightactivecount + 1;
            }
            else
                $obj->rightactivecount = $data->leftactivecount + $data->rightactivecount;


            if ($memberlist[$id]->direct_id == $original_id){
                $obj->rightdirectcount = $data->leftdirectcount + $data->rightdirectcount + 1;

            }
            else
                $obj->rightdirectcount = $data->leftdirectcount + $data->rightdirectcount;
        }
        else
            $obj->rightcount = 0;

        return $obj;
    }
}
