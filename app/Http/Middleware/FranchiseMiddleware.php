<?php

namespace App\Http\Middleware;

use App;
use Closure;
use View;

class FranchiseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   public function handle($request, Closure $next) {
        $language = $request->route()->parameter('lang');

        if(!isset($langage))
        $language = 'en';    
        
        App::setLocale($language);

        View::share('lang', $language);
        return $next($request);
    }
}
