<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faqs;
use App\doctorspanel;
use App\StaticPage;
use Illuminate\Support\Facades\Input;


class StaticPagesController extends Controller
{
    public function viewFAQList(){
    	$faq = Faqs::orderBy('created_at','DESC')->get();
    	return view('back.ecommerce.faq',compact('faq'));
    }
	
    public function createFAQ()
    {
        return view('back.ecommerce.add-faq');
    }
	
	public function storeFAQ(Request $request){
    	$faq = new Faqs;
    	$faq->questions = $request->question;
    	$faq->answers = $request->answer;
    	$faq->status = $request->status;
    	$faq->save();
    	return redirect()->back()->with('message','Data Inserted Successfully');
    }

    public function editFAQ($id)
    {
        $faq = Faqs::find($id);
        return view('back.ecommerce.edit-faq',compact('faq'));
    }
	
    public function updateFAQ(Request $request, $id)
    {
        $faq = Faqs::find($id);        
        $faq->questions = $request->question;
        $faq->answers = $request->answer;
        $faq->status = $request->status;
        $faq->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }

    public function destroyFAQ($id)
    {
        $faq = Faqs::find($id);
        $faq->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }
	
	
    public function viewPageList(){
        $pages = StaticPage::all();
        return view('back.ecommerce.static-pages',compact('pages'));
    }
	
	public function createPages()
    {
       
        return view('back.ecommerce.add-static-pages');
    }

    public function editPage($id)
    {
        $page = StaticPage::find($id);
        return view('back.ecommerce.edit-static-pages',compact('page'));
    }
	
	public function storePage(Request $request, $id)
    {
		
        $page = new StaticPage; 
		$page->menu_name = $request->menu_name;
		$page->page_name = $request->page_name;
        $page->page_heading = $request->page_heading;
		$str = strtolower($request->page_name);
		$page->slug = preg_replace('/\s+/', '-', $str);
	    $page->description = $request->description;
        $page->save();
        return redirect()->back()->with('message','Data Successfully Updated');
    }

    public function updatePage(Request $request, $id)
    {
        $page = StaticPage::find($id); 
        $page->page_heading = $request->page_heading;
        $page->description = $request->description;
        $page->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }
	
	
	// start code doctor panel 
	
	/* 	* function rout 'Admin\StaticPagesController@viewDoctorPanelList'
		* show all list data
	*/
	
	public function viewDoctorPanelList(){
    	$doctor_panel = doctorspanel::orderBy('created_at','DESC')->get();
		
    	return view('back.ecommerce.doctor_panel',compact('doctor_panel'));
    }
	
	/* 	* function rout 'Admin\StaticPagesController@createDoctorPanel'
		* Add data from 
	*/
	
	public function createDoctorPanel()
    {
        return view('back.ecommerce.add-doctor-panel');
    }
	
	/* 
	* function rout 'Admin\StaticPagesController@storeDoctorPanel'
	* stor data 
	*/
	
	public function storeDoctorPanel(Request $request){
    
		$doctorpanel = new doctorspanel;

        // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/doctorspanel/', time().$file->getClientOriginalName());
           $doctorpanel->image=time().$file->getClientOriginalName();           
        }
		
    	$doctorpanel->name = $request->name;
    	$doctorpanel->designation = $request->designation;
		$doctorpanel->description = $request->description;
		$doctorpanel->panel_type = $request->panel_type;
		$doctorpanel->status = $request->status;
      	$doctorpanel->save();
    	return redirect()->action('Admin\StaticPagesController@viewDoctorPanelList')->with('message','Data Inserted Successfully');
    }
	
	/* 
	* function rout 'Admin\StaticPagesController@editDoctorPanel'
	* edit data form  
	*/
	
	public function editDoctorPanel($id)
    {
        $doctorspannel = doctorspanel::find($id);
        return view('back.ecommerce.edit-doctor-panel',compact('doctorspannel'));
    }
	/* 
	* function rout 'Admin\StaticPagesController@updateDoctorPanel'
	* update data through id
	*/
	public function updateDoctorPanel(Request $request, $id)
    {
		$doctorspannel = doctorspanel::find($id);
         // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/doctorspanel/', time().$file->getClientOriginalName());
           $doctorspannel->image=time().$file->getClientOriginalName();           
        }
		$doctorspannel->name = $request->name;
		$doctorspannel->designation = $request->designation;
		$doctorspannel->description = $request->description;
		$doctorspannel->panel_type = $request->panel_type;
        $doctorspannel->status = $request->status;
        $doctorspannel->update();
	      
        return redirect()->action('Admin\StaticPagesController@viewDoctorPanelList')->with('message','Data Successfully Updated');
    }
	/* 
	* function rout 'Admin\StaticPagesController@destroyDoctorPanel'
	* delete data through id
	*/
	public function destroyDoctorPanel($id)
    {
        $doctorpanel = doctorspanel::find($id);
        $doctorpanel->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }
	
	
	
}
