<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\MemberRepository;
use App\Models\Member;
use App\Models\MemberDetail;
use App\Models\BonusDirect;
use App\Models\BonusPairing;
use App\Models\TransferEpoint;
use App\Models\CustomerOrder;
use App\Models\CustomerOrderProduct;
use App\Models\CustomerAddress;
use App\User;
use App\Invoice;
use App\Cart;
use App\Order;
use App\OrderItem;
use App\FranchiseeProduct;
use App\PointValue;
use StdClass;


class SiteController extends Controller
{
    public function __construct() {
        $this->middleware('admin', ['except' => [
            'getLogin',
            'postLogin',
            'getLogout'
        ]]);
    }

    public function getLogin () {
        return view('back.login');
    }
    
    public function getIndex () {
        return view('back.home');
    }

    public function getAccountSettings () {
        return view('back.settings');
    }

    public function getLogout () {
        if ($user = \Sentinel::getUser()) {
            \Sentinel::logout($user);
        }
        return view('back.login');
    }

    public function postLogin () {
        $data = \Input::get('data');

        

        try {
            $user = \Sentinel::authenticate([
                'username'  =>  $data['username'],
                'password'  =>  $data['password']
            ], (isset($data['remember'])));

            if (!$user) {
                throw new \Exception('Username / Password do not match.', 1);
                return false;
            }

            $permissions = $user->permissions;
            if (!isset($permissions['admin'])) {
                throw new \Exception('Cannot login here.', 1);
                return false;
            } else if ($permissions['admin'] != 1) {
                throw new \Exception('Cannot login here.', 1);
                return false;
            }
        } catch (\Exception $e) {
            \Sentinel::logout();
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        return \Response::json([
            'type'      =>  'success',
            'message'   =>  'Redirecting to dashboard..',
            'redirect'  =>  route('admin.home'),
        ]);
    }

    public function postUpdateAccount () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();

        if ($data['password'] != '') {
            try {
                \Sentinel::update($user, [
                    'password'  =>  trim($data['password'])
                ]);
            } catch (\Exception $e) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  $e->getMessage()
                ]);
            }
        }

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Account updated.'
        ]);
    }


    public function getActiveMembers() {
        $members = Member::where('package_id','>',1)->get();
        return view('back.active',compact('members'));
    }

    public function getInvoice($id){

        $user=Member::find($id);

        return view('back.invoice',compact('user'));
    }

    public function getMemberList () {
        return view('back.member.list');
    }

    public function getMemberWallet () {
        return view('back.wallet.list');    
    }

    public function getWalletStatement ($id) {
        return view('back.wallet.statementList')->with('id', $id);
    }

    public function getMemberRegister () {
        return view('back.member.register');
    }

    public function getMemberRegisterCommon () {
        return view('back.member.register2');
    }

    public function getWithdrawAddStatement () {
        return view('back.withdraw.addStatement');
    }

    public function getWithdrawList () {
        return view('back.withdraw.list');
    }

    public function getDirectPage () {

        $bonus_direct = BonusDirect::orderBy('created_at','DESC')->paginate(10);
        return view('back.income.daily-income',compact('bonus_direct'));
    }

    public function getSearchedDirectPage (Request $request) {
        $bonus_direct = "";
        if($request->type == 'to'){
            $bonus_direct = BonusDirect::where('username','LIKE','%' .$request->search. '%')->get(); 
        }
        elseif($request->type == 'from'){
            $bonus_direct = BonusDirect::where('from_username','LIKE','%' .$request->search. '%')->get(); 
        }
        else{
            $bonus_direct = BonusDirect::where('from_username','LIKE','%' .$request->search. '%')->orWhere('username','LIKE',$request->search)->get(); 
        }
        
        return view('back.income.daily-income',compact('bonus_direct'));
    }

    public function getSearchedWeeklyPage (Request $request) {
        $bonus_pairing = "";        
        $bonus_pairing = BonusPairing::where('username','LIKE','%' .$request->search. '%')->orderBy('created_at','DESC')->paginate(10);
        
        return view('back.income.weekly-income',compact('bonus_pairing'));
    }

    public function getWeeklyPage () {
        $bonus_pairing = BonusPairing::all();
        return view('back.income.weekly-income',compact('bonus_pairing'));
    }

    public function getTransferAddStatement () {
        return view('back.transfer.addStatement');
    }

    public function getEpointTransferAddStatement (){
        return view('back.transfer.addEpointStatement');
    }

    public function getEpointTransferStatement (){
        $epoints = TransferEpoint::all();
        return view('back.transfer.epointList',compact('epoints'));
    }

    public function getSearchedEpointsList (Request $request){
        $epoints = array();
        $i = 0;
        if($request->search == 'admin'){
            $epoints = TransferEpoint::where('sender_id','admin')->orWhere('receiver_id','admin')->get();
        }
        else{
            $member = Member::where('username','LIKE','%' . $request->search . '%')->get();
            if(!empty($member)){
                foreach($member as $m){
                    $getepoints = TransferEpoint::where('sender_id',$m->id)->orWhere('receiver_id',$m->id)->get();
                    if(!empty($getepoints)){
                        foreach($getepoints as $get)
                            $epoints[$i++] = $get;
                    }
                }
            }
        }
        
        return view('back.transfer.epointList',compact('epoints'));
    }

    public function getEpointTransferList (){
        return view('back.transfer.epointList');
    }

    public function getTransferList () {
        return view('back.transfer.list');
    }

    public function getBonusAddStatement () {
        return view('back.bonus.addStatement');
    }

    public function getBonusList () {
        return view('back.bonus.list');
    }

    public function getBonusPairingList () {
        $bonus = BonusPairing::all();
        return view('back.bonus.pairing-list',compact('bonus'));
    }

    public function getSearchedBonusPairingList (Request $request) {
        $bonus = BonusPairing::where('username','LIKE','%' .$request->search. '%')->get();
        return view('back.bonus.pairing-list',compact('bonus'));
    }

    public function getPackageSettings () {
        return view('back.package.settings');
    }

    public function getSharesSettings () {
        return view('back.shares.settings');
    }

    public function getSharesSellAdmin () {
        return view('back.shares.sell');
    }

    public function getSharesSell () {
        return view('back.shares.sellList');
    }

    public function getSharesBuy () {
        return view('back.shares.buyList');
    }

    public function getSharesSplit () {
        return view('back.shares.split');
    }

    public function getSharesLock () {
        return view('back.shares.lockList');
    }
    

    public function getMemberEdit ($id) {
        $instance = new MemberRepository;
        if (!$model = $instance->findById(trim($id))) {
            return redirect()->route('admin.member.list')->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  'Member not found.'
            ]);
        }
        return view('back.member.edit')->with('model', $model);
    }
    
    public function getMemberInfo ($id) {
        $member = Member::where('id',$id)->first();
        $member_detail = MemberDetail::where('member_id',$id)->first();
        $user = User::where('id',$member->user_id)->first();
        
        return view('back.member.member-detail',compact('member', 'member_detail','user'));
    }

    public function runCron () {
        if (\Input::get('type') == 'pairing') {
            \Artisan::call('bonus:pairing');
        } else if (\Input::get('type') == 'checkGroup') {
            \Artisan::call('member:group');
        } else if (\Input::get('type') == 'group') {
            \Artisan::call('bonus:group');
        } else if (\Input::get('type') == 'freeze') {
            \Artisan::call('shares:freeze');
        } else if (\Input::get('type') == 'direct') {
            \Artisan::call('bonus:direct');
        }

        return \Response::json([
            'type' => 'success',
            'message' => 'Job completed successfully'
        ]);
    }

    public function createAnnouncement () {
        return view('back.announcement.create');
    }

    public function getAnnouncementList () {
        return view('back.announcement.list');
    }

    public function maintenance () {
        if (\App::isDownForMaintenance()) {
            \Artisan::call('up');
        } else {
            \Artisan::call('down');
        }

        return \Response::json([
            'type'  =>  'information',
            'message' =>  'MT status toggled, refresh to see changes.'
        ]);
    }

    /**
     * Upload Image
     * @param  Request $req [description]
     * @return [type]       [description]
     */
    public function uploadImage (Request $req) {
        $validator = \Validator::make($req->all(), [
            'imageFile' => 'max:2048|mimes:jpeg,png,jpg',
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'type'  =>  'error',
                'message' =>  'File must be smaller than 2mb and in jpg or png format.'
            ]);
        }
        $file = $req->file('imageFile');
        $destinationPath = 'uploads/images/' . date('m') . '/';
        $filename = time() . '-' . $file->getClientOriginalName();
        $file->move(public_path() . '/' . $destinationPath, $filename);
        $image = $destinationPath . $filename;
        return \Response::json([
            'type'  =>  'success',
            'url'   =>  asset($image)
        ]);
    }

    public function getCoinWallet () {
        return view('back.coin.wallet');
    }

    public function getCoinTransaction () {
        return view('back.coin.transaction');
    }

    public function getAllMembers (Request $request) {
        $page = 1;        
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }
        $count_member = Member::count();
        $members = Member::leftJoin('users','users.id','member.user_id')->where('users.is_ban',0)->select('member.id','users.username','users.first_name','member.direct_id','member.created_at','member.register_by')->orderBy('member.created_at','DESC')->paginate(10);
        
        return view('back.member.updateMember',compact('members','count_member','page'));
    }

    public function getEditUser ($id) {
        $member = MemberDetail::where('member_id',$id)->first();
        $getMember=Member::where('id',$id)->first();        
        return view('back.member.edit-detail',compact('member','getMember'));
    }

    public function getEditBankAccount ($id) {
        $member = MemberDetail::where('member_id',$id)->first();
        return view('back.member.edit-bank',compact('member'));
    }
    public function getResetPassword ($id) {
        $member = Member::where('id',$id)->first();
        $user = User::where('id',$member->user_id)->first();
        return view('back.member.resetPassword',compact('user'));
    }

    public function getSearchedMember (Request $request) {
        $message = "Member Not Found";
        $members = Member::where('username','LIKE','%' .$request->search_username. '%')->get();
        
        return view('back.member.searched-Member',compact('members'));     
    }   


    public function checkFrom(Request $request){
        $response = new StdClass;
        $response->name = "No Name";
        $response->message = "Something Went Wrong";
        $user = $request->uname;

        $check = Member::where('username',$user)->first();

        if($check){
         $response->name = $check->name; 
         $response->message = "Success";
     }  
     else {
        $response->message = "INVALID MEMBER ID";
    }   
    return response()->json($response); 
}

public function checkTo(Request $request){
    $response = new StdClass;
    $response->name = "No Name";
    $response->message = "Something Went Wrong";
    $user = $request->uname;
    
    $check = Member::where('username',$user)->first();

    if($check){
     $response->name = $check->name; 
     $response->message = "Success";
 }  
 else {
    $response->message = "INVALID MEMBER ID";
}   
return response()->json($response); 
}

public function genrateInvoiceId(Request $request){
    $invoice = new Invoice;
    $check = Invoice::where('member_id',$request->id)->where('package_amount',$request->package_amount)->first();
    if(!empty($check)){
    }
    else{
        $invoice->member_id = $request->member_id;
        $invoice->package_amount = $request->package_amount;
        $invoice->invoice_number = $request->i_no;

        $invoice->save();
        if($invoice){
            return \Response::json('message','Invoice Generated');
        }
    }
    return \Response::json('message','Invoice Generated');
}

public function getProductPurchaseRequest($type){
    if($type == 'franchisee'){
        $orders = Order::where('order_status','Pending')->where('user_type','Franchisee')->get();
        $user_type = 'Franchisee';
    }
    else{
        $orders = Order::where('order_status','Pending')->where('user_type','IBD')->get();
        $user_type = 'IBD';
    }
    return view('back.products.purchase-request',compact('orders','user_type'));
}


public function changeOrderStatus(Request $request){

    $id = $request->order_id;
    $status = $request->status;
    $order = Order::find($id);
    $message = 'Something Went Wrong';

    if($order){
        $order->order_status = $status;
        $order->update();

        if($status == 'Delivered'){

            $result = OrderItem::leftJoin('products','products.id','order_items.item_id')->where('order_items.order_id',$order->id)->select('order_items.quantity','products.pv','products.bv','order_items.item_id')->get();
            
            $pv_value = 0;
            $bv_value = 0;
            $user = User::where('id',$order->user_id)->first();

            if(!empty($result)){
                foreach($result as $res){
                    $pv_value +=$res->pv*$res->quantity;
                    $bv_value +=$res->bv*$res->quantity;

                    $check =  FranchiseeProduct::where('user_id',$user->id)->where('product_id',$res->item_id)->first();
                    if(!empty($check)){
                        $check->quantity = $check->quantity+$res->quantity;
                        $check->update();
                    }
                    else{
                        $fran_pro = new FranchiseeProduct;

                        $fran_pro->user_id = $user->id;
                        $fran_pro->username = $user->username;
                        $fran_pro->product_id = $res->item_id;
                        $fran_pro->quantity = $res->quantity;

                        $fran_pro->save();
                    }
                }            
            }

            $pvs = new PointValue;
            $pvs->pv = $pv_value;
            $pvs->bv = $bv_value;
            $pvs->user_id = $user->username;
            $pvs->save();         



        }
        $message = 'Status Changed Successfully';
    }
    return response()->json(['message'=>$message]);
}

public function getProductPurchaseHistory(){
 $orders = Order::where('order_status','!=','Pending')->get(); 
 return view('back.products.purchase-history',compact('orders'));
}

public function getOrdersList(){
    $orders = CustomerOrder::join('customer_addresses','customer_addresses.id','customer_orders.billing_address')->select('customer_orders.*','customer_addresses.name','customer_addresses.phone')->get();

    return view('back.orders.list',compact('orders'));
}

public function getOrderDetail($id){
    $order = CustomerOrder::find($id);

    $products = CustomerOrderProduct::where('order_id',$id)->get();

    $shipping = CustomerAddress::where('id',$order->shipping_address)->first();
    $billing = CustomerAddress::where('id',$order->billing_address)->first();

    return view('back.orders.edit',compact('order','products','shipping','billing'));
}

public function updateOrderStatus(Request $request,$id){
    $order = CustomerOrder::where('id',$id)->update(
        array(
            'order_status' => $request->order_status
        ));

    return redirect()->back()->with('message','Order status updated successfully.');
}

}
