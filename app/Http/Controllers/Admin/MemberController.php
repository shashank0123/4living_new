<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SharesRepository;
use App\Repositories\BonusRepository;
use Yajra\Datatables\Facades\Datatables;
use App\UploadProof;
use App\Models\Epoint;
use App\Models\Member;
use App\Models\MemberDetail;
use App\User;
use Hash;

class MemberController extends Controller
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;

    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $BonusRepository;

    /**c
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        PackageRepository $PackageRepository,
        SharesRepository $SharesRepository,
        BonusRepository $BonusRepository
    ) {
        $this->middleware('admin');
        $this->MemberRepository = $MemberRepository;
        $this->PackageRepository = $PackageRepository;
        $this->SharesRepository = $SharesRepository;
        $this->BonusRepository = $BonusRepository;
    }

    /**
     * Datatable member admin
     * @return object
     */
    public function getList () {
		 
        return $this->MemberRepository->findAll(true);
    }

    /**
     * Datatable member wallet admin
     * @return object
     */
    public function getWalletList () {
        return $this->MemberRepository->findWalletList(true);
    }

    /**
     * Datatable member wallet statement
     * @return [type] [description]
     */
    public function getWalletStatementList ($id) {
        $query = \DB::table('Member_Wallet_Statement')
        ->where('member_id', trim($id));
        return Datatables::queryBuilder($query)
        ->editColumn('register_amount', function ($model) {
            return number_format($model->register_amount, 2);
        })
        ->editColumn('promotion_amount', function ($model) {
            return number_format($model->promotion_amount, 2);
        })
        ->make(true);
    }

    /**
     * Register member (ROOT)
     * @return json
     */
    public function register ($type) {
        $data = \Input::get('data');
        $data['package_id'] = 1;
        
        
       if (!$package = $this->PackageRepository->findById($data['package_id'])) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Package not found.'
            ]);
        }

        if (($type != 'root' && $type != 'common') || !isset($type)) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Type not found.'
            ]);
        }
        try {
            $user = \Sentinel::registerAndActivate([
                'email'   => $data['email'],
                'username'  =>  $data['username'],
                'password'  =>  $data['password'],
                'phone'  =>  $data['phone'],
                'permissions' =>  [
                    'member' => true,
                ]
            ]);

            if ($type == 'root') { // top member
					
                $member = $this->MemberRepository->saveModel(new \App\Models\Member, [
                    'username'  =>  $user->username,
                    'name'  =>  $data['username'],
                    'referral_id'  =>  $user->username,
                    'register_by' => 'admin',
                    'secret_password' =>  '123456',
                    'user_id'   =>  $user->id,
                    'package_id'    =>  $package->id,
                    'direct_id' =>  0,
                    'parent_id' =>  0,
                    'root_id'   =>  0,
                    'level'     =>  1,
                    'direct_percent'    =>  $package->direct_percent,
                    'pairing_percent'   =>  $package->pairing_percent,
                    'group_level'   =>  $package->group_level,
                    'max_pair'  =>  $package->max_pair,
                    'max_pairing_bonus' =>  $package->max_pairing_bonus,
                    'original_amount'  =>  $package->package_amount,
                    'package_amount' =>  $package->package_amount,
                    'position'  =>  1,
                    'password'  =>  $data['password'],
                ]);

                $this->MemberRepository->saveModel(new \App\Models\MemberWallet, [
                    'member_id' =>  $member->id,
                    'register_point'    =>  0,
                    'purchase_point'    =>  $package->purchase_point,
                    'promotion_point'   =>  0,
                    'cash_point'    =>  0,
                    'md_point'  =>  0
                ]);
				
				 $refcode = strtoupper(substr($user->username, 0, 1)).$member->id; 
				$totalStr = 10 - strlen($refcode);
				if($totalStr == 8){
				$randcode = rand(10000000, 99999999);
				}elseif($totalStr == 7){
					$randcode = rand(1000000, 9999999);
				}elseif($totalStr == 6){
					$randcode = rand(100000, 999999);
				}elseif($totalStr == 5){
					$randcode = rand(10000, 99999);
				}elseif($totalStr == 4){
					$randcode = rand(1000, 9999);
				}else{
					$randcode = rand(100, 999);
				}
				$newRefCode = $refcode.$randcode;
				$affectedMemberRows = \App\Models\Member::where('id', $member->id)->update(['referral_id' => $newRefCode]);
				
            } else { // lower member
                if (!$direct = $this->MemberRepository->findByUsername($data['direct_id'])) {
                    throw new \Exception('Member upline not found.', 1);
                    return false;
                }

                if (!$parent = $this->MemberRepository->findByUsername($data['parent_id'])) {
                    throw new \Exception('Member binary not found.', 1);
                    return false;
                }

                if (!$this->MemberRepository->checkIfPositionAvailable($parent, $data['position'])) {
                    throw new \Exception('Position no longer available.', 1);
                    return false;
                }

                $member = $this->MemberRepository->saveModel(new \App\Models\Member, [
                    'username'  =>  $user->username,
                    'name'  =>  $data['username'],
                    'referral_id'  =>  $user->username,
                    'register_by' => 'admin',
                    'secret_password' =>  '123456',
                    'user_id'   =>  $user->id,
                    'package_id'    =>  $package->id,
                    'direct_id' =>  0,
                    'parent_id' =>  0,
                    'root_id'   =>  0,
                    'level'     =>  1,
                    'direct_percent'    =>  $package->direct_percent,
                    'pairing_percent'   =>  $package->pairing_percent,
                    'group_level'   =>  $package->group_level,
                    'max_pair'  =>  $package->max_pair,
                    'max_pairing_bonus' =>  $package->max_pairing_bonus,
                    'original_amount'  =>  $package->package_amount,
                    'package_amount' =>  $package->package_amount,
                    'position'  =>  1,
                    'password'  =>  $data['password'],
                ]);
                $this->MemberRepository->saveModel(new \App\Models\MemberWallet, [
                    'member_id' =>  $member->id,
                    'register_point'    =>  0,
                    'purchase_point'    =>  $package->purchase_point,
                    'promotion_point'   =>  0,
                    'cash_point'    =>  0,
                    'md_point'  =>  0
                ]);

                if (env('APP_ENV') == 'local') { // local
                    $wallet = $member->wallet;
                    $this->MemberRepository->addNetwork($member);
                    $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
                } else { // production
                    dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
                    dispatch(new NetworkAfterRegisterJob($member))->onQueue('queue-network-register');
                }

                // remove cache for network
                \Cache::forget('member.' . $parent->id . '.children');

                $this->BonusRepository->calculateDirect($member, $direct);
                $this->BonusRepository->calculateOverride($member);
            }

            $this->MemberRepository->saveModel(new \App\Models\MemberDetail, [
                'member_id' =>  $member->id,
                'mobile_phone' => $data['phone']
            ]);

            $this->MemberRepository->saveModel(new \App\Models\MemberShares, [
                'member_id' =>  $member->id,
                'amount'    =>  0,
                'share_limit'   =>  $package->share_limit,
                'max_share_sale'    =>  $package->max_share_sale
            ]);

            //Send Success SMS to User
           
            $username = $data['username'];
            $password = $data['password'];
            $email = $data['email'];
            
            $mobile = $data['phone'];
            $message = "Welcome%20$username%20to%204LivinG%2E%20Your%20Customer%20Registration%20ID%20is%20$username%20%26%20Password%20is%20$password%2E%20For%20more%20details%20logon%20to%20www%2E4living%2Ein%2E%20Thank%20You%2E";
            $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=FRLVNG&dest=$mobile&msg=$message&priority=1";  
            
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);

            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) { 
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }


        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Register successful.'
        ]);
    }

    /**
     * Update member data
     * @return json
     */
    public function postUpdate ($id) {
        $data = \Input::get('data');
        $member = $this->MemberRepository->findById(trim($id));
        $user = $member->user;
        $userData = [];

        if (isset($data['first_name'])) {
            $userData['first_name'] = $data['first_name'];
        }

        if (isset($data['password'])) {
            if ($data['password'] != '') {
                $userData['password'] = $data['password'];
            }
        }
        if (isset($data['is_update_basic'])) {
            $userData['is_ban'] = isset($data['is_ban']) ? 1 : 0;
        }

        \Sentinel::update($user, $userData);

        $detail = $member->detail;
        $wallet = $member->wallet;
        $shares = $member->shares;
        $original = $member->original_amount;

        foreach ($member->getAttributes() as $k => $value) {
            if (isset($data[$k])) $member->{$k} = $data[$k];
        }

        foreach ($detail->getAttributes() as $k => $value) {
            if ($k != 'created_at') {
                if (isset($data[$k])) $detail->{$k} = $data[$k];
            }
        }

        // foreach ($wallet->getAttributes() as $k => $value) {
        //     if ($k != 'created_at' || 
        //         $k != 'lock_cash' || 
        //         $k != 'lock_register' || 
        //         $k != 'lock_promotion') {
        //         if (isset($data[$k])) $wallet->{$k} = $data[$k];
        //     }
        // }

        // if (isset($data['is_update_wallet'])) {
        //     $wallet->lock_cash = isset($data['lock_cash']) ? 1 : 0;
        //     $wallet->lock_register = isset($data['lock_register']) ? 1 : 0;
        //     $wallet->lock_promotion = isset($data['lock_promotion']) ? 1 : 0;
        // }

        // foreach ($shares->getAttributes()as $k => $value) {
        //     if ($k != 'created_at') {
        //         if (isset($data[$k])) $shares->{$k} = $data[$k];
        //     }
        // }

        $member->save();
        $detail->save();
        // $wallet->save();
        // $shares->save();

        // if (isset($data['original_amount'])) {
        //     if ($original != $data['original_amount']) {
        //         $this->updateSalesAmount($member, $original, $data['original_amount']);
        //     }
        // }

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Member account updated.'
        ]);
    }

    /**
     * [updateSalesAmount description]
     * @param  [type] $member [description]
     * @param  [type] $amount [description]
     * @return [type]         [description]
     */
    public function updateSalesAmount ($member, $amountFrom, $amountTo) {
        $befores = '';
        $beforeChildren = '';
        $isSub = false;
        $amount = $amountTo - $amountFrom;
        if ($amount == 0) return false;
        if ($amount < 0) {
            $amount = abs($amount);
            $isSub = true;
        }
        if ($parent = $member->parent()) {
            $alwaysRemove = explode(',', $member->position == 'right' ? $parent->left_children : $parent->right_children);
        }

        while ($root = $member->root()) {
            if ($member->position == 'left') {
                $memberIds = $root->left_children;
                $position = 'left_children';
                $totalField = 'left_total';
            } else {
                $memberIds = $root->right_children;
                $position = 'right_children';
                $totalField = 'right_total';
            }

            $memberIds = rtrim($root->id . ',' . $memberIds, ',');
            $memberIds = explode(',', $memberIds);

            if ($befores != '') {
                $memberIds = array_diff($memberIds, explode(',', $befores));
            }

            if (isset($alwaysRemove)) {
                $memberIds = array_diff($memberIds, $alwaysRemove);
            }

            if ($beforeChildren != '') {
                $memberIds = array_diff($memberIds, explode(',', $beforeChildren));
            }

            $memberIds = implode(',', $memberIds);

            if ($root->position == 'top') {
                \DB::update('UPDATE tb_member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id IN (' . $memberIds . ') AND position="top" AND level < ' . $member->level);
            } else {
                \DB::update('UPDATE tb_member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id=' . $root->id . ' AND level < ' . $member->level);
            }

            $beforeChildren .= ',' . ($root->position == 'left' ? $root->left_children : $root->right_children);
            $beforeChildren = ltrim($beforeChildren, ',');
            $befores .= ',' . $memberIds;
            $member = $root;
        }

        return true;
    }

    /**
     * Find member info when register
     * @return html
     */
    public function getMemberRegisterModal () {
        $model = false;
        if (trim(\Input::get('u')) != '') {
            $model = $this->MemberRepository->findByUsername(trim(\Input::get('u')));
        }
        return view('front.member.modalRegister')->with('model', $model);
    }


    public function getResetPassword(Request $request){
        return view('back.resetpassword');
    }

    public function getUploads(Request $request){
        $uploads = UploadProof::all();
        return view('back.withdraw.statements',compact('uploads'));
    }

    public function getApprovedUploads(Request $request){
        $uploads = UploadProof::where('status','Approved')->get();
       
        return view('back.withdraw.approved',compact('uploads'));
    }

    public function getSearchedUploads(Request $request){
        $uploads = "";
        $member = Member::where('username',$request->search)->first();
        if(!empty($member)){
        $uploads = UploadProof::where('member_id',$member->id)->get();
        }
        return view('back.withdraw.statements',compact('uploads'));
    }


    public function passwordUpdateAccount (Request $request) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();

        $userData = [];
        $userdetails = \Sentinel::authenticate([
            'username'  =>  $user->username,
            'password'  =>  $data['old-password'],
        ]);

        if (isset($data['password']) && $userdetails) {
            // echo "Yes Password".' , ';
            if ($data['password'] != '') {
                $userData['password'] = $data['password'];
                if (count($userData) > 0) {
                    \Sentinel::update($user, $userData);
                    // echo "Password Updated";
                }
            }
        }

        \Cache::forget('member.'. $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Password updated successfully'
        ]);
    }

    function updateProofStatus(Request $request){
        $message = "Something Went Wrong";
        $status = "Pending";
        $id = UploadProof::find($request->id);
        $epoint = Epoint::find($id->member_id);

        if($id){
            $id->status = $request->status;
            $id->update();
            if($epoint){
               
                $epoint->epoint = $epoint->epoint + $id->amount;
                $epoint->update();
            }
            else{

                $epoint = new Epoint;
                $epoint->epoint = $id->amount;
                $epoint->member_id = $id->member_id;
                $epoint->save();

            }
            $message = "Status Changed Successfully ";
            $status = "Approved";
        }    
        // die;
    return \Response::json([
            'type'  =>  'success',
            'message'   =>  $message,
            'status'    => $status
        ]);

    }

    public function updateUserDetail(Request $request,$id){
        $member = MemberDetail::where('member_id',$id)->first();
        $member->gender = $request->gender;
        $member->mobile_phone = $request->mobile_phone;
        $member->date_of_birth = $request->date_of_birth;
        $member->beneficiary_name = $request->beneficiary_name;
        $member->relation_with_beneficiary = $request->relation_with_beneficiary;
        $member->address = $request->address;
        $member->state = $request->state;
        $member->adhaar = $request->adhaar;
        $member->pan = $request->pan;
        
        $member->update();


        $mbr = Member::where('id',$member->member_id)->first();
        $mbr->username = $request->username;
		$mbr->name = $request->first_name;
        $mbr->update();
    
        $user = User::where('id',$mbr->user_id)->first();
        $name = explode(' ',$request->first_name);

        if($user){
        $user->first_name = $name[0];
        if(!empty($name[1])){
            $user->last_name = $name[1];
        }
        
        $user->phone = $request->mobile_phone;
        $user->username = $request->username;
        $user->update();
        echo $user->username;
        }
        
        return redirect()->back()->with('message','Data Successfully Updated');
    }

    public function updatePassword(Request $request, $id){
        $message = "Something Went Wrong";
        
        if($request->new_password != $request->confirm_password){
            echo $message = "Password Did't Match";
        }
        else{
            $user = User::where('id',$id)->first();
            $user->password = Hash::make($request->new_password);
            $user->update();
           echo  $message = 'Password Updated Successfully';
        }
        return redirect()->back()->with('message','Password Changed Updated');
    }

    public function updateUserBankDetail(Request $request,$id){
        $member = MemberDetail::where('member_id',$id)->first();
       
        $member->bank_address = $request->bank_address;
        $member->bank_name = $request->bank_name;
        $member->bank_account_number = $request->bank_account_number;
        $member->bank_account_holder = $request->bank_account_holder;
        $member->bank_branch = $request->bank_branch;
        $member->ifsc = $request->ifsc;

        $member->update();

        return redirect()->back()->with('message','Bank Details Updated Successfully.');
    }

    public function deleteMember($id){

        $message = "Something Went Wrong";
        $check = Member::where('id',$id)->first();
        
        $user = User::where('id',$check->user_id)->first();
         
        if(!empty($user)){
             if($user->is_ban == 1){
             $user->is_ban = 0;
             $user->update();
             $message = "Member Activated";
             }
             else{
                 $user->is_ban = 1;
                 $user->update();
             $message = "Member Blocked";
             }
         }
        
        
        // echo $check;
        // $member_id = $check->user_id;
        // Member::where('id',$id)->delete();        

        // $detail = MemberDetail::where('member_id',$member_id)->first();        
        // if($detail)       
        // $detail->delete();

        // $user = User::where('id',$member_id)->first();
        // if($user)
        // $user->delete();
        
        
        return redirect()->back()->with(['message'=>$message]);

    }
}
