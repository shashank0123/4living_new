<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\UploadProof;
use App\Models\Franchise;
use App\Models\FranchiseeDocument;
use App\User;
use DB;
use Hash;


class FranchiseController extends Controller
{
    //
	 public function __construct() {
        $this->middleware('admin', ['except' => [
            'getLogin',
            'postLogin',
            'getLogout'
        ]]);
    }


	
	public function getAllForms() {
		$forms = FranchiseeDocument::join('packages','packages.id','franchisee_documents.package_id')->select('franchisee_documents.*','package->package_amount')->get();

		return view('back.franchise.form_list',compact('forms'));
	}

	





	public function getAllFranchise() {
		$franchise = DB::table('franchises')
            ->leftJoin('franchise_details', 'franchises.id', '=', 'franchise_details.franchise_id')
			 ->orderBy('franchises.created_at', 'DESC')
			 ->paginate(10);
            
			
        //$franchise = Franchise::orderBy('created_at','DESC')->paginate(10);
        return view('back.franchise.list',compact('franchise'));
    }
	
	public function getFranchiseRegister(){
		return view('back.franchise.add-franchise');
    
	}
	
	public function storeFranchise(Request $request){
		

		$data = \Input::all();
		// var_dump($data);
		// die;

		$username = strtoupper(substr($data['name'],0,1)).rand(999999, 1000000000);

		$this->validate($request, [
        'name' => 'required',
        'address1' => 'required',
		'city' => 'required',
        'state' => 'required',
		'pincode' => 'required|integer',
		// 'phone' => 'required|integer',
        'username' => 'required',
		'email' => 'required|email|unique:franchises',
		'password' => 'required',
		
		]);   


		$user = \Sentinel::registerAndActivate([
                'email'   => $data['email'],
                'username'  =>  $username,
                'password'  =>  $data['password'],
                'phone'  =>  $data['phone'],
                'permissions' =>  [
                    'franchisee' => true,
                ]
            ]);

		$id = User::where('username',$username)->first();

		$password = Hash::make($request->password);
		
    	$franchiseStore = new Franchise;
    	$franchiseStore->name = $request->name;
    	$franchiseStore->user_id = $id->id;
    	$franchiseStore->address1 = $request->address1;
		$franchiseStore->address2 = $request->address2;
		$franchiseStore->city = $request->city;
		$franchiseStore->state = $request->state;
		$franchiseStore->pincode = $request->pincode;
		$franchiseStore->phone = $request->phone;
		$franchiseStore->alternet_contact = $request->alternet_contact;
		$franchiseStore->username = $request->username;
		$franchiseStore->email = $request->email;
		$franchiseStore->password = $password;
		$franchiseStore->aadhar_number = $request->aadhar_number;
		$franchiseStore->pass = $request->password;
		$fcode = 'FKU'.rand(1000000,9999999);
		//strlen($fcode);
		$franchiseStore->franchise_code = $fcode;
		
		
    	$franchiseStore->save();
		return redirect()->action('Admin\FranchiseController@getAllFranchise')->with('message','Data Inserted Successfully');
    	
    }
	
	public function editFranchise($id)
    {
        $franchisedit = Franchise::find($id);
	
        return view('back.franchise.edit-franchise',compact('franchisedit'));
    }
	
	public function updateFranchise(Request $request, $id)
    {
		$this->validate($request, [

                'name' => 'required',
				'address1' => 'required',
				'city' => 'required',
				'state' => 'required',
				'pincode' => 'required|integer',
				'phone' => 'required|integer',
               
        ]);
					 
        $franchiseUpdate = Franchise::find($id);
		$franchiseUpdate->name = $request->name;
    	$franchiseUpdate->address1 = $request->address1;
		$franchiseUpdate->address2 = $request->address2;
		$franchiseUpdate->city = $request->city;
		$franchiseUpdate->state = $request->state;
		$franchiseUpdate->pincode = $request->pincode;
		$franchiseUpdate->phone = $request->phone;
		$franchiseUpdate->alternet_contact = $request->alternet_contact;
		$franchiseUpdate->aadhar_number = $request->aadhar_number;
		
	   	$franchiseUpdate->update();
		return redirect()->back()->with('message','Data Successfully Updated');
    }
	
	public function getFranchiseDetails($id){
		$franchisDetailsedit = FranchiseDetails::where('franchise_id', $id)->first();
		
		if(!empty($franchisDetailsedit)){
			$details = $franchisDetailsedit;
		}else{
			//convert array to obj
			
			$array = array('franchise_id'=>$id);
			$details = (object) $array;
	
		}
		
        return view('back.franchise.edit-franchise-details',compact('details'));
	}
	
	public function updateFranchiseDetails(Request $request, $id)
    {
		
		$franchisDetailsedit = FranchiseDetails::where('franchise_id', $id)->first();
		if(!empty($franchisDetailsedit)){
			
			$affectedRows = FranchiseDetails::where('franchise_id', $id)->update(['gst_no' => $request->gst_no, 'pan_no'=>$request->pan_no, 'bank_name'=>$request->bank_name, 'account_holder_name'=>$request->account_holder_name, 'account_number'=>$request->account_number, 'bank_address'=>$request->bank_address, 'bank_branch'=>$request->bank_branch, 'bank_ifsc_code'=>$request->bank_ifsc_code, 'status'=>'1', 'updated_at'=>date('Y-m-d H:i:s')]);
		}else{
			
			$this->validate($request, [

                'gst_no' => 'required|unique:franchise_details',
				'pan_no' => 'required|unique:franchise_details',
				'bank_name' => 'required',
				'account_holder_name' => 'required',
				'account_number' => 'required|integer|unique:franchise_details',
				'bank_address' => 'required',
				'bank_branch' => 'required',
				'bank_ifsc_code' => 'required',
				
               
        ]);
			
			$affectedRows = FranchiseDetails::insert(['franchise_id'=>$id, 'gst_no' => $request->gst_no,'pan_no'=>$request->pan_no, 'bank_name'=>$request->bank_name, 'account_holder_name'=>$request->account_holder_name, 'account_number'=>$request->account_number, 'bank_address'=>$request->bank_address, 'bank_branch'=>$request->bank_branch, 'bank_ifsc_code'=>$request->bank_ifsc_code, 'status'=>'1', 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')]);
			
			$franchiseData = Franchise::find($id);
			$fcode = 'F'.strtoupper(substr($franchiseData->name, 0, 2)).$franchiseData->id;
			 
			 $total = 10 - strlen($fcode);
			if($total == 6){
				$randcode = rand(100000, 999999);
			}elseif($total == 5){
				$randcode = rand(10000, 99999);
			}elseif($total == 4){
				$randcode = rand(1000, 9999);
			}elseif($total == 3){
				$randcode = rand(100, 999);
			}elseif($total == 2){
				$randcode = rand(10, 99);
			}
			$franchisecode = $fcode.$randcode;
			
			$updateFranchise = Franchise::where('id', $id)->update(['franchise_code' => $franchisecode]);
			
	
		}			 
       
        return redirect()->back()->with('message','Data Successfully Updated');
    }
	
	public function getResetPassword ($id) {
        
		 $franchisPassword = Franchise::find($id);
        return view('back.franchise.resetPassword',compact('franchisPassword'));
    }
	
	public function updatePassword(Request $request, $id){
        $message = "Something Went Wrong";
        if($request->new_password != $request->confirm_password){
             $message = "Password Did't Match";
			
        }
        else{
        	$password = Hash::make($request->new_password);
            $franchise = Franchise::where('id',$id)->first();
            $franchise->password = $password ;
            $franchise->pass = $password ;
            $franchise->update();
            $message = 'Password Updated Successfully';
		  
        }
        return redirect()->back()->with('message',$message);
    }
	
	  // Remove Product from the db 
public function destroyFranchise($id)
{
    $Franchise = Franchise::find($id);
        // echo $Product;
        // exit();
		if($Franchise){
			$Franchise->delete();
			$franchisDetailsedit = FranchiseDetails::where('franchise_id', $id)->first();
			if(!empty($franchisDetailsedit)){
				$franchisDetailsedit = FranchiseDetails::where('franchise_id', $id)->delete();
			}
		}
    
    return redirect()->back()->with('message','Data Successfully Deleted');
}
	
	
	
		
	
	
}
