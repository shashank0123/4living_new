<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Associate;
use Input;

class AssociateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $associates = Associate::orderBy('display_priority',"ASC")->get();
        return view('back.associates.list',compact('associates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.associates.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $newData = new Associate;

if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/images/partner', time().$file->getClientOriginalName());
           $newData->image=time().$file->getClientOriginalName();           
        }

        $newData->company_name = $data['company_name'] ?? '';
        $newData->website_link = $data['website_link'] ?? '';
        $newData->company_email = $data['company_email'] ?? '';
        $newData->address = $data['address'] ?? '';
        $newData->city = $data['city'] ?? '';
        $newData->state = $data['state'] ?? '';
        $newData->pincode = $data['pincode'] ?? '';
        $newData->display_priority = $data['display_priority'] ?? '';
        
        $newData->save();
        return redirect()->back()->with('message','Data successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $associate = Associate::where('id',$id)->first();

        return view('back.associates.edit',compact('associate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        // var_dump($data); die;

        $updateData = Associate::find($id);

if (Input::hasfile('image')) {
           $file=Input::file('image');
           $file->move(public_path(). '/images/partner', time().$file->getClientOriginalName());
           $updateData->image=time().$file->getClientOriginalName();           
        }

        $updateData->company_name = $data['company_name'] ?? '';
        $updateData->website_link = $data['website_link'] ?? '';
        $updateData->company_email = $data['company_email'] ?? '';
        $updateData->address = $data['address'] ?? '';
        $updateData->city = $data['city'] ?? '';
        $updateData->state = $data['state'] ?? '';
        $updateData->pincode = $data['pincode'] ?? '';
        $updateData->display_priority = $data['display_priority'] ?? '';
        
        $updateData->save();
        return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Associate::where('id',$id)->delete();

        return redirect()->back()->with('message','Data successfully deleted');
    }
}
