<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\MainCategory;
use App\Product;
use App\Models\Package;

class ProductController extends Controller
{
        // Display a listing of the Product on the Navbar.

    public function getAllProducts(){
        $products = array();
        $packages = Package::where('package_amount','>=','10000')->get();
    
        foreach($packages as $package){
            $product = Product::where('category_id',$package->id)->orderBy('category_id','ASC')->get();

            if(!empty($product)){
                foreach($product as $pro){
                    $pro->package_amount = $package->package_amount ?? '0';
                    $products[] = $pro;
                }
            }
        }

        return view('back.franchise.products_list',compact('packages','products'));
    }

    public function getProduct()
    {
        $product = Product::leftjoin('main_categories','main_categories.id','=','products.category_id')->select('products.*','main_categories.Mcategory_name')->orderBy('created_at', 'desc')->get();
        // echo $product;
        // exit();
        return view('back.ecommerce.product',compact('product'));
    }





    public function createProduct(){
        return view('back.franchise.add_product');
    }

public function storeProduct(Request $request){
    $product = new Product;

    if (Input::hasfile('image1')) {
         $file=Input::file('image1');
         $file->move(public_path(). '/images/products/', $file->getClientOriginalName());
         $product->image1 = $file->getClientOriginalName();           
         $product->image2 = $file->getClientOriginalName();           
         $product->image3 = $file->getClientOriginalName();           
     }


     $product->name                  = $request->name ?? '';
     $product->short_descriptions    = $request->name ?? '';
     $product->long_descriptions     = $request->long_descriptions ?? '' ;
     $product->mrp                   = '0';
     $product->sell_price            = '0';
     $product->trending              = 'yes';
     $product->page_title            = $request->name ?? '' ;        
     $product->page_keywords         = $request->name ?? '' ;
     // $product->product_size         = $request->product_size;
     // $product->product_color         = $request->product_color;
     $product->page_description      = $request->name ?? '' ;        
     $product->category_id           = $request->package_id;        
     $product->pv           = '0';        
     $product->bv           = '0';        
     $product->availability           = '1';   
     $product->status                = 'Active';

     $product->save();


        return redirect()->back()->with('message','Product added successfully');
    }

     public function editFranProduct($id){
        $product = Product::where('id',$id)->first();
        return view('back.franchise.edit_product',compact('product'));
    }


    public function updateFranProduct(Request $request , $id){
    $product = Product::where('id',$id)->first();

    if (Input::hasfile('image1')) {
         $file=Input::file('image1');
         $file->move(public_path(). '/images/products/', $file->getClientOriginalName());
         $product->image1 = $file->getClientOriginalName();           
         $product->image2 = $file->getClientOriginalName();           
         $product->image3 = $file->getClientOriginalName();           
     }


     $product->name                  = $request->name ?? '';
     $product->long_descriptions     = $request->long_descriptions ?? '' ;
    

     $product->update();


        return redirect()->back()->with('message','Product updated successfully');
    }

    public function deleteFranProduct($id){
        Product::where('id',$id)->delete();
        return redirect()->back()->with('message','Product deleted successfully');
    }

    





    // Show the form for creating a new Product Record
    public function viewProduct()
    {
        $product = Product::all();
        // echo $Product;
        // exit();
        return view('back.ecommerce.add-product',compact('product'));   
    }

    // Store a newly created Product in storage.
    public function addProduct(Request $request)
    {
        $product = new Product;

        // for image1
        if (Input::hasfile('image1')) {
         $file=Input::file('image1');
         $file->move(public_path(). '/assetsss/images/AdminProduct/', $file->getClientOriginalName());
         $product->image1 = $file->getClientOriginalName();           
         $product->image2 = $file->getClientOriginalName();           
         $product->image3 = $file->getClientOriginalName();           
     }

        // for image2
     // if (Input::hasfile('image2')) {
     //     $file=Input::file('image2');
     //     $file->move(public_path(). '/assetsss/images/AdminProduct/', $file->getClientOriginalName());
     //     $product->image2=$file->getClientOriginalName();           
     // }

     //    // for image2
     // if (Input::hasfile('image3')) {
     //     $file=Input::file('image3');
     //     $file->move(public_path(). '/assetsss/images/AdminProduct/', $file->getClientOriginalName());
     //     $product->image3=$file->getClientOriginalName();           
     // }

     

     $product->name                  = $request->name;
     $product->short_descriptions    = $request->name;
     $product->long_descriptions     = $request->name;
     $product->mrp                   = $request->sell_price;
     $product->sell_price            = $request->sell_price;
     $product->trending              = 'yes';
     $product->page_title            = $request->page_title;        
     $product->page_keywords         = $request->page_keywords;
     // $product->product_size         = $request->product_size;
     // $product->product_color         = $request->product_color;
     $product->page_description      = $request->page_description;        
     $product->category_id           = '0';        
     $product->pv            = '0';        
     $product->bv           = '0';        
     $product->availability           = $request->availability;   
     $product->status                = $request->status;

     $product->save();

     return redirect()->back()->with('message','Data Successfully Inserted');
 }

    // Display the specified Product.   
 public function showProduct($id)
 {
    $product = Product::find($id);
        // echo $product;
        // exit(); 
    return view('back.ecommerce.edit-product',compact('product'));
}

    // Show the form for editing the specified Product.    
public function editProduct($id,Request $request)
{
    $product = Product::find($id);

        // for image1
    if (Input::hasfile('image1')) {
     $file=Input::file('image1');
     $file->move(public_path(). '/assetsss/images/AdminProduct/', $file->getClientOriginalName());
     $product->image1=$file->getClientOriginalName();           
     $product->image2=$file->getClientOriginalName();           
     $product->image3=$file->getClientOriginalName();           
 }

 $product->name                  = $request->name;
 // $product->short_descriptions    = $request->short_descriptions;
 // $product->long_descriptions     = $request->long_descriptions;
 // $product->mrp                   = $request->mrp;
 $product->sell_price            = $request->sell_price;
 // $product->trending              = $request->trending;
 $product->page_title            = $request->page_title;        
 $product->page_keywords         = $request->page_keywords;
 // $product->pv           = $request->pv;        
     // $product->bv           = $request->bv;
 // $product->product_color         = $request->product_color;
 // $product->product_size         = $request->product_size;
 $product->page_description      = $request->page_description;        
 // $product->category_id           = $request->category_id;   
 $product->availability           = $request->availability;     
 $product->status                = $request->status;


 $product->update();

 return redirect()->back()->with('message','Data Successfully Updated');

}

  // Remove Product from the db 
public function destroyProduct($id)
{
    $Product = Product::find($id);
        // echo $Product;
        // exit();
    $Product->delete();
    return redirect()->back()->with('message','Data Successfully Deleted');
}


public function searchProduct(Request $request)
{
    $keyword = $request->product_keyword;
    $product = Product::where('name','LIKE','%'.$keyword.'%')->orWhere('short_descriptions','LIKE','%'.$keyword.'%')->orWhere('long_descriptions','LIKE','%'.$keyword.'%')->orWhere('page_description','LIKE','%'.$keyword.'%')->orWhere('page_keywords','LIKE','%'.$keyword.'%')->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('products.*','main_categories.Mcategory_name')->orderBy('created_at', 'desc')->get();
    return view('back.ecommerce.searched-product',compact('product'));
}

}
