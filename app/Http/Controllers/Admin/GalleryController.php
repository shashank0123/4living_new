<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\imagegallery;
use App\videogallery;
use Illuminate\Validation\Rule;


class GalleryController extends Controller
{
	
    public function viewImageGallerylList(){
    	$imagegallery = imagegallery::orderBy('created_at','DESC')->get();
    	return view('back.ecommerce.image-gallery',compact('imagegallery'));
    }
	
    public function createImageGallery()
    {
        return view('back.ecommerce.add-image-gallery');
    }
	
	public function storeImageGallery(Request $request){
		
		$this->validate($request, [

                'filename' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
		
				
		if($request->hasfile('filename'))
         {
			
            foreach($request->file('filename') as $image)
            {
                $name=time().$image->getClientOriginalName();
                $image->move(public_path().'/assetsss/images/gallery/', $name);  
                $data[] = $name;  
            }
         }
		 
		
    	$imggallery = new imagegallery;
    	$imggallery->name = $request->ename;
    	$imggallery->designation = $request->eaddress;
		$imggallery->filename=json_encode($data);
    	$imggallery->status = $request->status;
    	$imggallery->save();
    	return redirect()->back()->with('message','Data Inserted Successfully');
    }

    public function editImageGallery($id)
    {
        $imagegallery = imagegallery::find($id);
        return view('back.ecommerce.edit-image-gallery',compact('imagegallery'));
    }
	
    public function updateImageGallery(Request $request, $id)
    {
		$this->validate($request, [

                'filename' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
		
				
		if($request->hasfile('filename'))
         {
			
            foreach($request->file('filename') as $image)
            {
                $name=time().$image->getClientOriginalName();
                $image->move(public_path().'/assetsss/images/gallery/', $name);  
                $data[] = $name;  
            }
         }
		 
        $imagegalleryUpdate = imagegallery::find($id);
		//get previous image from db;
		
		$previousImg = json_decode($imagegalleryUpdate->filename);	
		$newfilename = array_merge($previousImg,$data); 
		
		$imagegalleryUpdate->name = $request->ename;
    	$imagegalleryUpdate->designation = $request->eaddress;
		$imagegalleryUpdate->filename=json_encode($newfilename);
        $imagegalleryUpdate->status = $request->status;
        $imagegalleryUpdate->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }

    public function destroyImageGallery($id)
    {
        $imagegallery = imagegallery::find($id);
        $imagegallery->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }
	
	public function deleteImgGallery($id, $imgid){
		$imagegallery = imagegallery::find($id);
		$allimg = json_decode($imagegallery->filename);
		$newarr = [];
		foreach ($allimg as $key => $value){
			$path = '';
			if ($value == $imgid) {
				unset($allimg[$key]);
				$path = public_path().'/assetsss/images/gallery/'.$imgid;
				unlink($path);
			}else{
			$newarr[] = $value;
			}
		}

			
		$newfilename = json_encode($newarr);
		$imagegallery = imagegallery::find($id);        
        $imagegallery->filename = $newfilename;
		$imagegallery->update();
		
		
		return redirect()->back()->with('message','Image Successfully Deleted');

	}
	
   // Video Gallery code start here
   
    public function viewVideoGallerylList(){
    	$videogallery = videogallery::orderBy('created_at','DESC')->get();
    	return view('back.ecommerce.video-gallery',compact('videogallery'));
    }
	
    public function createVideoGallery()
    {
        return view('back.ecommerce.add-video-gallery');
    }
	
	public function storeVideoGallery(Request $request){
		
		$this->validate($request,[
		'title' => 'required|max:50',
		
        'url'=> 'required|max:255',
         
      ]);
		
    	$videoStore = new videogallery;
    	$videoStore->title = $request->title;
    	$videoStore->url = $request->url;
		$videoStore->status = $request->status;
    	$videoStore->save();
		return redirect()->action('Admin\GalleryController@viewVideoGallerylList')->with('message','Data Inserted Successfully');
    	
    }
	
	public function editVideoGallery($id)
    {
        $videoedit = videogallery::find($id);
        return view('back.ecommerce.edit-video-gallery',compact('videoedit'));
    }
	
	public function updateVideoGallery(Request $request, $id)
    {
		$this->validate($request, [

                'title' => 'required',
               
        ]);
					 
        $videoUpdate = videogallery::find($id);
		$videoUpdate->title = $request->title;
    	$videoUpdate->url = $request->url;
		$videoUpdate->status = $request->status;
        $videoUpdate->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }
	  public function destroyVideoGallery($id)
    {
        $videogallery = videogallery::find($id);
        $videogallery->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }
}
