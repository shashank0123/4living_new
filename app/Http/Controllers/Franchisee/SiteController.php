<?php

namespace App\Http\Controllers\Franchisee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Cart;
use App\Address;
use App\Models\Franchise;
use App\Models\FranchiseeDocument;
use App\Models\FranchiseDetails;
use App\User;
use App\FranchiseeProduct;
use App\Product;
use Hash;
use Input;
use StdClass,Mail;

class SiteController extends Controller
{
	public function __construct() {       
        $this->middleware('franchise');
    }

   public function getLogin () {
        return view('franchisee.login');
    }

     public function getRegister () {
        return view('franchisee.register');
    }

    public function getHome () {
        $member =  session()->get('franchisee');
        $franchisee = Franchise::where('user_id',$member->id)->first();
     
      return view('franchisee.home',compact('member','franchisee'));
    }

public function postRegister(Request $request){
    $data = $request->all();
    
    $info = new StdClass;

    $newData = new FranchiseeDocument;

    $newData->package_id = $data['package_id'];

     if (Input::hasfile('reg_form')) {
         $file=Input::file('reg_form');
         $file->move(public_path(). '/images/documents/', $file->getClientOriginalName());
         $newData->reg_form = $file->getClientOriginalName();        
         $info->form = '/images/documents/'.$newData->reg_form;
     }

     if (Input::hasfile('profile_pic')) {
         $file=Input::file('profile_pic');
         $file->move(public_path(). '/images/documents/', $file->getClientOriginalName());
         $newData->profile_pic = $file->getClientOriginalName();       
         $info->profile_pic = '/images/documents/'.$newData->profile_pic;
     }

     if (Input::hasfile('pan_card')) {
         $file=Input::file('pan_card');
         $file->move(public_path(). '/images/documents/', $file->getClientOriginalName());
         $newData->pan_card = $file->getClientOriginalName();     
         $info->pan_card = '/images/documents/'.$newData->pan_card;
     }

     if (Input::hasfile('adhaar_front')) {
         $file=Input::file('adhaar_front');
         $file->move(public_path(). '/images/documents/', $file->getClientOriginalName());
         $newData->adhaar_front = $file->getClientOriginalName();        
         $info->adhaar_front = '/images/documents/'.$newData->adhaar_front;
     }

     if (Input::hasfile('adhaar_back')) {
         $file=Input::file('adhaar_back');
         $file->move(public_path(). '/images/documents/', $file->getClientOriginalName());
         $newData->adhaar_back = $file->getClientOriginalName();     
         $info->adhaar_back = '/images/documents/'.$newData->adhaar_back;
     }

     $newData->save();

    $admin = array();
    
    $admin['admin'] = 'info@4living.in';
    $admin['site'] = '4LivinG';


     Mail::send('mails.franchisee', ['info' => $info],
                  function ($m) use ($admin) {
                    //   $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
                      $m->from( 'no-reply@4living.in', '4Living' );

                      $m->to($admin['admin'], $admin['site'])->subject('Franchisee Form Submitted.');
                  });

     return redirect()->back()->with('message','Documents submitted successfully.');

     
}


    public function getAllProducts () {
        $data = session()->get('franchisee');
        $id = $data->id;
        $data = FranchiseeProduct::where('user_id',$id)
                    ->get();                   

        return view('franchisee.products.allproducts',compact('data'));
    }

    public function getPurchaseRequestForIBD  () {
        $user = session()->get('franchisee');
        $cart = Cart::where('user_id',$user->id)
                ->where('status','Pending')
                ->orderBy('created_at','DESC')
                ->first();
                
                if(!empty($cart))
                $show = json_decode($cart->cart);

        $data = Product::where('status','Active')->get();
        return view('franchisee.products.purchase-request',compact('data','show'));
    }

    public function getProfile () {
        $user = session()->get('franchisee');

        $member = Franchise::leftjoin('franchise_details','franchise_details.franchise_id','franchises.id')
                ->where('user_id',$user->id)->first();
              
        return view('franchisee.settings.account',compact('member'));
    }

    public function postUpdateAccount ( Request $request) {
        $user = session()->get('franchisee');

        $id=$user->id;

        $data = \Input::all();

        $data = $data['data'];

        $franchisee = Franchise::where('user_id',$id)->first();

      

        if($franchisee){
           $franchisee->name = $data['name'];
        
        $franchisee->address1 = $data['address1'];
        $franchisee->address2 = $data['address2'];
        $franchisee->city = $data['city'];
        $franchisee->state = $data['state'];
        $franchisee->pincode = $data['pincode'];
        $franchisee->phone = $data['phone'];
        $franchisee->alternet_contact = $data['alternet_phone'];
        $franchisee->gender = $data['gender'];        
        $franchisee->aadhar_number = $data['aadhar_number'];       
        $franchisee->update();
        
    }
      
        
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.accountUpdateSuccess')
        ]);
    }
    
    public function postUpdateBankAccount ( Request $request) {
        $user = session()->get('franchisee');
        $id=$user->id;
        $data = \Input::all();
        $data = $data['data'];

        $franchisee = Franchise::where('user_id',$id)->first();
        $f_id = $franchisee->id;
        $franchise_details = FranchiseDetails::where('franchise_id',$f_id)->first();        
        if($franchise_details){     
           $franchise_details = FranchiseDetails::where('franchise_id',$f_id)
                           ->update(array(
                            'bank_name' => $data['bank_name'],
                            'account_number' => $data['account_number'],
                            'account_holder_name' => $data['account_holder_name'],
                            'bank_address' => $data['bank_address'],
                            'bank_ifsc_code' => $data['ifsc'],
                            'gst_no' => $data['gst_no'],
                            'bank_branch' => $data['bank_branch'],
                        ));  
        
    }

    else{

        $franchise_detail = new FranchiseDetails;
        $franchise_detail->franchise_id = $f_id; 
        $franchise_detail->bank_name = $data['bank_name'];        
        $franchise_detail->account_number = $data['account_number'];
        $franchise_detail->account_holder_name = $data['account_holder_name'];
        $franchise_detail->bank_address = $data['bank_address'];
        $franchise_detail->pan_no = $data['pan'];
        $franchise_detail->bank_ifsc_code = $data['ifsc'];
        $franchise_detail->gst_no = $data['gst_no'];
        $franchise_detail->bank_branch = $data['bank_branch'];
        $franchise_detail->save();   
    }      
        
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.accountUpdateSuccess')
        ]);
    }


public function passwordUpdateAccount (Request $request) {
        $user = session()->get('franchisee');        
        $data = \Input::all();
        $data = $data['data'];

        if($data['password'] == $data['confirm-password']){
            $userDetail = User::where('username',$user->username)
                        ->update(array(
                            'password' => Hash::make($data['password'])
                        ));
            }

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.accountUpdateSuccess')
        ]);
    }
public function addProductToCart (Request $request) {
    $count = new StdClass;
    $message = 'Product added successfully.';
           $cart = new Cart;
        $product_id = $request->id;
        if (session()->get('cart') != null){
            $cart = session()->get('cart');
            $count = session()->get('count');           
        }
        else {
            $cart = array();
          
        }

        if (isset($cart[$product_id]->quantity)){
            $cart[$product_id]->quantity = $request->quantity;
        }
        else{
            $item = new StdClass;
            $item->product_id = $product_id;
            $item->quantity = $request->quantity;
            $cart[$product_id] = $item;
            $count++;
        }

        $product = Product::where('id',$request->id)->first();
        $amount = $product->sell_price * $request->quantity;

        $request->session()->put('cart',$cart);
       
        if(!$cart)
        {
            $message = "Something Went Worng";        
        }
        return response()->json([
            'type'  =>  'success',
            'message'   =>  $message,
            'amount' => $amount
        ]);
}


public function addEntryToCart(Request $request){
        $user = session()->get('franchisee');       
        $message = 'Something Went Wrong';
    if (session()->get('cart') != null){
        $data = session()->get('cart');
        $datas = [];
        $i=0;
        foreach($data as $d){
            if($d->quantity >0){
                $datas[$i++] = $d;
            }
        }
        $session = session()->getId();
        $check = Cart::where('session_id',$session)->where('status','Pending')->orderBy('created_at','DESC')->first();
            $data = json_encode($datas);

        if(!empty($check)){
                $check->cart =  $data;
                $check->update();
        }
        else{
            $cart = new Cart;
           
            $cart->user_id = $user->id;
            $cart->cart = $data ;  
            $cart->date = date('Y-m-d H:m:s');
            $cart->session_id = session()->getId();       
            $cart->status  = 'Pending';     
            $cart->user_type  = 'Franchisee';     
            $cart->save(); 
        }
            
            $message = 'Added Successfully'; 
        }
        return response()->json([
            'type'  =>  'success',
            'message'   =>  $message            
        ]);

} 

public function getCheckout(){
        $user = session()->get('franchisee');       
    $cart = Cart::where('user_id',$user->id)
                ->where('status','Pending')
                ->orderBy('created_at','DESC')
                ->first();
                $cart_id = $cart->id;

                $data = json_decode($cart->cart);

                return view('franchisee.products.checkout',compact('data','cart_id'));
}

public function getCheckoutForm(){
     $user = session()->get('franchisee');  
     $member = Franchise::leftjoin('franchise_details','franchise_details.franchise_id','franchises.id')->where('franchises.user_id',$user->id)->first();

     $bill = 0;   
    $cart = Cart::where('user_id',$user->id)
                ->where('status','Pending')
                ->orderBy('created_at','DESC')
                ->first();
                $cart_id = $cart->id;

                $data = json_decode($cart->cart);
                foreach($data as $d){
                    $product = Product::where('id',$d->product_id)->first();
                    $bill = $bill+$product->sell_price*$d->quantity;
                }

    return view('franchisee.products.bill',compact('bill','member'));
} 


public function thankyou(Request $request){
     $user = session()->get('franchisee');  
        $message = 'Something Went Wrong';
        $order = new Order;
        $address = new Address(); 

        $checkSessionID = Cart::where('session_id',session()->getId())->where('user_id',$user->id)->orderBy('created_at','DESC')->first();        
        
            $address->user_id = $user->id;
            $address->address = $request->address;
            $address->country_id = '1';
            $address->state_id = '1';
            $address->city_id = '1';
            $address->pin_code = '1';
            $address->phone = $request->phone;
            $address->save();    

        $address_id = Address::where('user_id',$user->id)->orderBy('created_at','DESC')->first();
        $order->user_id = $user->id;
        $order->address_id =  $address_id->id;
        $order->price =  $request->bill;
        $order->order_status =  'Pending';
        $order->user_type =  'Franchisee';
        $order->payment_method =  $request->payment_mode;
        $order->date = date('Y-m-d H:m:s');
        $order->save();

        $order_id = Order::where('user_id',$user->id)->orderBy('created_at','DESC')->first();
        $i=0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $order_item = new OrderItem;
                $order_item->order_id = $order_id->id;
                $order_item->item_id = $cart->product_id;
                $price = Product::where('id',$cart->product_id)->first();
                $order_item->quantity   = $cart->quantity;
                $order_item->price = $cart->quantity * $price->sell_price;
                $order_item->save();
            }   

            $message = 'Done';
        }

        session()->forget('cart');
        return response()->json(['message'=>$message]);
}


public function getProductPurchaseHistory(){
     $user = session()->get('franchisee');

     $orders = Order::leftJoin('order_items','order_items.order_id','orders.id')  
                ->leftjoin('carts','carts.id','order_items.cart_id')
                ->where('carts.user_id',$user->id)
                ->get();

         
                return view('franchisee.products.purchase-history',compact('orders'));
} 

public function getSoldProducts(Request $request){
    return view('franchisee.products.sold-products');
}
}

