<?php

namespace App\Http\Controllers\Franchisee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement;

class AnnouncementController extends Controller
{
     /**
     * The AnnouncementRepository instance.
     *
     * @var \App\Repositories\AnnouncementRepository
     */
  
    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     * @return void
     */
    public function __construct() {
       $this->middleware('franchise');
    }

    /**
     * Datatable List
     * @return [type] [description]
     */
    

    /**
     * Announcement List
     * @return html
     */
    public function getAll () {
        $data = Announcement::all();
        $member = session()->get('auth'); 
        return view('franchisee.announcement.list',compact('data'))->with('member',$member);
    }

    /**
     * Announcement Read Page
     * @param  integer $id
     * @return html
     */
    public function read ($lang, $id) {
        	
        return view('franchisee.announcement.read')->with('model', $model);
    }
}
