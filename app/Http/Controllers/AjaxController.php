<?php

namespace App\Http\Controllers;
use App\Contact;
use App\Models\Member;
use App\Models\MemberDetail;
use App\Models\CustomerAddress;
use App\Models\CustomerOrder;
use App\Models\CustomerOrderProduct;
use App\User;
use App\Leads;
use Illuminate\Http\Request;
use StdClass,Mail;

class AjaxController extends Controller
{
     public function addToCart(Request $request ){
        $data = $request->all();

         $cart = new StdClass;
        $product_id = $request->product_id;
        if (session()->get('cart') != null){
            $cart = session()->get('cart');
        }
        else 
            $cart = array();
        if (isset($cart[$product_id]->quantity)){
            $cart[$product_id]->quantity = $request->quantity ;
        }
        else{
            $item = new StdClass;
            $item->product_id = $product_id;
            $item->product_name = $request->product;
            $item->quantity = $request->quantity;
            $item->image = $request->image;
            $item->price = $request->price;
            $item->page_url = $request->page_url;
            $cart[$product_id] = $item;
        }

        $request->session()->put('cart',$cart);

        if(!$cart)
        {
            echo "Something Wen Worng";        
        }

        $countCart = count(session()->get('cart'));
        return response()->json(['status' => 200, 'message' => 'data retrieved', 'countCart' => $countCart]); 

     }

     public function getMinicart(){
        $cartProducts = session()->get('cart') ?? '';
        // var_dump($cartProducts);
         // die;

        return view('minicart',compact('cartProducts'));
     }

     public function deleteCart(Request $request){
        $flag = 1 ;
        $bill = 0;
        $cart = session()->get('cart');
        $cartItem = 0;
        $cart[$request->id] = null;
        $cart = array_filter($cart);
        $request->session()->put('cart',$cart);

        foreach(session()->get('cart') as $pro){
            $cartItem++;
            $total = $pro->price*$pro->quantity;
            $bill = $bill + $total;
        }
        if($flag == 1)

            return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully','subtotal' => $bill, 'cartItem' => $cartItem]);
        else
            return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
     }

public function getCheckout(){
    $cartProducts = session()->get('cart');
        return view('checkout',compact('cartProducts'));
}




public function postCheckout(Request $request){
    $data = $request->all();

    // var_dump($data);
    // die;
    $billAdd = "";
    $shipAdd = "";

    $billAddress = new CustomerAddress;

    $billAddress->name = $data['name'] ?? '';
    $billAddress->email = $data['email'] ?? '';
    $billAddress->phone = $data['phone'] ?? '';
    $billAddress->address = $data['address'] ?? '';
    $billAddress->country = $data['country'] ?? '';
    $billAddress->state = $data['state'] ?? '';
    $billAddress->city = $data['city'] ?? '';
    $billAddress->pin_code = $data['pin_code'] ?? '';

    $billAddress->save();

    $billAdd = CustomerAddress::get()->last();

    if(!empty($data['shipping_addresss'])){
        $shipAddress = new CustomerAddress;

        $shipAddress->name = $data['name1'] ?? '';
    $shipAddress->email = $data['email1'] ?? '';
    $shipAddress->phone = $data['phone1'] ?? '';
    $shipAddress->address = $data['address1'] ?? '';
    $shipAddress->country = $data['country1'] ?? '';
    $shipAddress->state = $data['state1'] ?? '';
    $shipAddress->city = $data['city1'] ?? '';
    $shipAddress->pin_code = $data['pin_code1'] ?? '';

    $shipAddress->save();
    $shipAddress = CustomerAddress::get()->last();
    }
    else{
        $shipAdd = $billAdd;
    }

    $newOrder = new CustomerOrder;

    $newOrder->shipping_address = $shipAdd->id ?? '0';
    $newOrder->billing_address = $billAdd->id ?? '0';
    $newOrder->total = $data['total_bill'] ?? '0' ;
    $newOrder->booking_date = date('Y-m-d');
    $newOrder->payment_mode = $data['payment_method'];

    $newOrder->save();

    $lastOrder = CustomerOrder::get()->last();

    // var_dump(session()->get('cart')); die;

    foreach(session()->get('cart') as $cart){
        $newProduct = new CustomerOrderProduct;

        $newProduct->order_id = $lastOrder->id;
        $newProduct->product_id = $cart->product_id;
        $newProduct->product_name = $cart->product_name ?? '';
        $newProduct->unit_price = $cart->price;
        $newProduct->quantity = $cart->quantity;
        $newProduct->image = $cart->image;
        $newProduct->path = $cart->page_url;

        $newProduct->save();
    }

    $products = CustomerOrderProduct::where('order_id',$lastOrder->id)->get();

if($data['payment_method'] == 'Payumoney'){
    return view('payment.product_payu',compact('data','products','lastOrder'));
}
else{




    $userInfo = array();

 $userInfo['admin'] = 'info@4living.in';
      $userInfo['site'] = '4living';
      $userInfo['email'] = $data['email'];
      $userInfo['name'] = $data['name'];
      $userInfo['phone'] = $data['phone'];
      $action = 'admin';

// var_dump($user); die;








                  Mail::send('mails.orderInvoice', ['user' => $userInfo, 'lastOrder' => $lastOrder, 'products' => $products, 'action' => $action],
                  function ($m) use ($userInfo) {
                    //   $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
                      $m->from( 'no-reply@4living.in', '4Living' );

                      $m->to($userInfo['admin'], $userInfo['site'])->subject('Order Booked');
                  });
                 
                  $action = 'User';
                  Mail::send('mails.orderInvoice', ['user' => $userInfo, 'lastOrder' => $lastOrder, 'products' => $products, 'action' => $action],
                  function ($m) use ($userInfo) {
                    //   $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
                    $m->from( 'no-reply@4living.in', '4Living' );

                      $m->to($userInfo['email'], $userInfo['name'])->subject('Order Booked');
                  });
                  
                  session()->forget('cart');


    return view('order-success',compact('lastOrder','products'));
}

}

public function checkoutForm1(Request $request,$id){
    $success = "";
    if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
        if(strcasecmp($contentType, 'application/json') == 0){
            $data = json_decode(file_get_contents('php://input'));
            $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
            $json=array();          
            $json['success'] = $hash;
            echo json_encode($json);        
        }
        exit(0);
    }

}

public function checkoutSuccess(Request $request){


         $post_data = $_POST;  
        //   var_dump($post_data); die;





$email = $post_data['email'];
$name = $post_data['firstname'];
$phone = $post_data['phone'];
$orderId = $post_data['productinfo'];
    $status = $post_data['txnStatus'];
    $txnid = $post_data['txnid'];
// var_dump($products); die;


if($status == 'SUCCESS' || $status =='success'){


$getOrder = CustomerOrder::where('id',$orderId)->update([
    'transaction_id' => $txnid,
    'order_status' => $status
]);

$lastOrder = CustomerOrder::where('id',$orderId)->first();

$products = CustomerOrderProduct::where('order_id',$orderId)->get();



 $userInfo = array();

 $userInfo['admin'] = 'info@4living.in';
      $userInfo['site'] = '4living';
      $userInfo['email'] = $email;
      $userInfo['name'] = $name;
      $userInfo['phone'] = $phone;
      $action = 'admin';






 Mail::send('mails.orderInvoice', ['user' => $userInfo, 'lastOrder' => $lastOrder, 'products' => $products, 'action' => $action],
                  function ($m) use ($userInfo) {
                    //   $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
                      $m->from( 'no-reply@4living.in', '4Living' );

                      $m->to($userInfo['admin'], $userInfo['site'])->subject('Order Booked');
                  });
                 
                  $action = 'User';
                  Mail::send('mails.orderInvoice', ['user' => $userInfo, 'lastOrder' => $lastOrder, 'products' => $products, 'action' => $action],
                  function ($m) use ($userInfo) {
                    //   $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
                    $m->from( 'no-reply@4living.in', '4Living' );

                      $m->to($userInfo['email'], $userInfo['name'])->subject('Order Booked');
                  });
                  
                  session()->forget('cart');






return view('order-success',compact('lastOrder','products'));


}

else{
    return response()->json('Something went Wrong');
}
}









     public function sendContactForm(Request $request ){
                $message = 'Something Went Wrong';
                $query = new Contact;
                $query->name = $request->name;
                $query->email = $request->email;    
                $query->subject = $request->subject;
                $query->message_content = $request->message;
                $query->response_status = $request->name;

                $query->save();
                if($query){
                    $message = "Thank you . We will reply you Soon.";
                }
                return response()->json(["message" => $message]);
            }
            
     public function getIndex(){

        $checkPlease = Leads::where('verify_otp',NULL)->get();

        $id="";
        $i=1;
    
        if(!empty($checkPlease)){
            foreach($checkPlease as $c){
                // echo $c;
                $findu = User::where('username',$c->username)->first();
                if(!empty($findu)){
                    $findu->delete();
                }
                                
                // echo $c;
                $findm = Member::where('username',$c->username)->first();
                if(!empty($findm)){
                  $delm = MemberDetail::where('member_id',$findm->id)->first();
                if(!empty($delm)){
                $delm->delete();
                    
                }
                $m = Member::where('username',$c->username)->delete();
                }
                
                $c->delete();
                $i++;
            }
        }

        return  view('index');
     }

     public function get4living(){
        return  view('4living');
     }

     public function getAbout(){
        return  view('about-us');
     }

     public function getDirectorsDesk(){
        return  view('directors-desk');
     }
     
     public function getDoctorPanel(){
        return  view('panel-of-doctors');
     }

     public function getDoctorConsult(){
        return  view('consult-doctors');
     }

     public function getContactUs(){
        return  view('contact-us');
     }

     public function getDrPrem(){
        return  view('dr-prem-prakash');
     }

     public function getTerms(){
        return  view('terms');
     }

     public function getPrivacyPolicy(){
        return  view('privacy');
     }

     public function getHumanBody(){
        return  view('humanbody');
     }

     public function getProductAyurveda(){
        return  view('products.ayurveda');
     }

     public function getProductNutritional(){
        return  view('products.nutrionals');
     }

     public function getProductSkinCare(){
        return  view('products.skin-care');
     }

     public function getProductTulsi(){
        return  view('products.tulsi');
     }

     public function getProductImsys(){
        return  view('products.imsys');
     }

     public function getProductArtho(){
        return  view('products.artho-sys');
     }

     public function getMadhumeghkalp(){
        return  view('products.madhumeghkalp');
     }

     public function getProductThyro(){
        return  view('products.thyro-sys');
     }

     public function getDoctorTeam(){
        return  view('doctors-team');
     }

     public function getAboutAyurveda(){
        return  view('about-ayurveda');
     }

     public function getRegister(){
        return  view('register');
     }
     
     public function getCapcha(Request $request){
         if(isset($_SESSION['digit'])){
         var_dump($_SESSION('digit'));
         }
         die;
     }

     public function getLogin(){
        return redirect()->route('login', ['lang' => \App::getLocale()]);
     }

     public function getResetPassword(){
        return  view('front.forgetPassword');
        }

     public function getClearCache(){
      $exitCode = \Artisan::call('config:clear');
  $exitCode = \Artisan::call('cache:clear');
  $exitCode = \Artisan::call('config:cache');
  $exitCode = \Artisan::call('route:cache');
    return 'DONE'; //Return anything
}
     
}
