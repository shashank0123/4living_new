<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\staticPages;
use App\SEO;
use File, Mail;

class IndexController extends Controller
{
    public function indexpage(Request $request)
    {
        $cities = City::all();
        $tarrif = Tarrif::where('booking_type', 'hourly')->distinct('min_hr', 'min_km')->get();
        $products = Category::orderBy('position', 'asc')->get();
        $servicecat = Service_category::orderBy('position', 'asc')->get();
        $servicessub = Service_sub_category::orderBy('position', 'asc')->get();
        $productssub = SubCategory::orderBy('position', 'asc')->get();
        $sliders = Sliderimages::get();
        return view('front.index', compact('cities', 'tarrif', 'products', 'servicecat', 'servicessub', 'productssub', 'sliders'));
    }

    // public function carpage(Request $request)
    // {
    //     $cities = City::all();
    //     $tarrif = Tarrif::where('booking_type', 'hourly')->distinct('min_hr', 'min_km')->get();
    //     return view('newhome', compact('cities', 'tarrif'));
    // }

    public function aboutpage(Request $request)
    {
        return view('aboutus', compact('cities', 'tarrif', 'products', 'servicecat'));
    }

    public function carpage(Request $request)
    {
        $cities = City::all();
        $tarrif = Tarrif::where('booking_type', 'hourly')->distinct('min_hr', 'min_km')->get();
        return view('carbooking', compact('cities', 'tarrif'));
    }

    public function contactus(Request $request)
    {
    	return view('contactus');
    }

    public function searchframe(Request $request)
    {
        $cities = City::all();
        $tarrif = Tarrif::where('booking_type', 'hourly')->distinct('min_hr', 'min_km')->get();
        return view('searchform', compact('cities', 'tarrif'));
    }

    public function staticpage($pagename)
    {
        $content = File::get(base_path('resources/views/static/'.$pagename.'.blade.php'));
        $seo = SEO::where('page_name', $pagename)->first();
        $page = staticPages::where('slug', $pagename)->first();
        if ($seo){
            session()->put('keywords', $seo->keywords);
            session()->put('meta', $seo->meta_desc);
            session()->put('title', $seo->title);
            session()->put('tags', $seo->tags);
        }
        else {
            session()->put('title', $page->title);
        }



    	return view('staticpagesview', compact('content', 'seo'));
    }
    public function submit(Request $request)
    {
        $requirements = new Requirement;
        $requirements->name = $request->name;
        $requirements->email = $request->email;
        $requirements->mobile = $request->mobile;
        $requirements->slug = $request->slug;
        $requirements->requirements = $request->Requirements;
        $requirements->save();
        $page = staticPages::where('slug', $request->slug)->first();

        $representative = RequirementRepresentative::where('page_id', $page->id)->first();
        if ($representative){
            $user = User::where('id', $representative->user_id)->first();
            $email = $user->email;
        }
        else{
            $email = 'shashankshekhar5823@gmail.com';
        }

        Mail::send('mails.requirements', compact('requirements'), function ($message) use ($email) {
            $message->from('webmaster@bizaad.com', 'Bizaad');
            $message->sender('webmaster@bizaad.com', 'Bizaad');
        
            $message->to($email);
        
            $message->bcc('shashankshekhar5823@gmail.com', 'shashank');
        
        
            $message->subject('New Requirement');
        
            $message->priority(3);
        
        });
        return redirect('/');
    }
}
