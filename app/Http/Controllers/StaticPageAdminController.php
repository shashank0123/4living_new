<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\staticPages;
use App\SEO;
use File;
class StaticPageAdminController extends Controller
{
    public function index()
    {
       $statics = staticPages::all();
        return view('admin.staticpages', compact('statics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_pages');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $static = new staticPages;
        
        $static->title            = $request->title;
        $static->slug                 = $request->slug;
        $static->save();
        $page_content = $request->content;





        $save = File::put(base_path('resources/views/static/'.$request->slug.'.blade.php'), $page_content);



        $seo                     = new SEO;
        $seo->page_name          = $request->slug;
        $seo->title              = $request->title;
        $seo->tags               = $request->tags;
        $seo->meta_desc          = $request->meta_desc;
        $seo->save();
        
        return redirect()->Route('staticpages.index')->with('message', "Data Successfully Inserted");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staticpage = staticPages::join('s_e_o_s', 's_e_o_s.page_name', '=', 'static_pages.slug')->select('static_pages.*', 's_e_o_s.tags', 's_e_o_s.title', 's_e_o_s.meta_desc')->where('static_pages.id', $id)->first();
        $content = File::get(base_path('resources/views/static/'.$staticpage->slug.'.blade.php'));
        return view('admin.add_pages', compact('staticpage', 'content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'title' => 'required',          
            'content' => 'required'         
        ]);
        $static = staticPages::find($id);     
        $static->title            = $request->title;
        $static->update();
        $page_content = $request->content;
        $save = File::put(base_path('resources/views/static/'.$request->slug.'.blade.php'), $page_content);
        $seo                     = SEO::where('page_name', $request->slug)->first();
        $seo->title              = $request->title;
        $seo->tags               = $request->tags;
        $seo->meta_desc          = $request->meta_desc;
        $seo->update();


        return redirect()->Route('staticpages.index')->with('message', "Data Successfully Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staticpages = staticPages::find($id);
        $seo = SEO::where('page_name', $staticpages->slug)->first();
        $seo->delete();
        $staticpages->delete();
        return redirect()->Route('staticpages.index')->with('message', "Data Successfully Deleted");
    }
}
