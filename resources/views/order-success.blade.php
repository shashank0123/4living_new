
@extends('layouts.ecommerce2')

@section('content')


<style type="text/css">
	#order-success .row{
		display: block!important;
	}
</style>

<!-- header end here -->
<div class=" mt-30 mb-30">
 <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
</div>

<br><br>
<div class="container" style="text-align: center;" id="order-success">
	<div class="row" style="text-align: center; margin-top: 45px;">
			<h1>Order Booked Successfully</h1><br>
		</div>

	<div class="row" style="text-align: center;">
			<h4> <b>Order ID :</b> {{ $lastOrder->id ?? '' }} &nbsp;&nbsp;&nbsp;&nbsp; <b>Booking Date :</b> {{ $lastOrder->booking_date ?? '' }} &nbsp;&nbsp;&nbsp;&nbsp; <b>Total amount :</b> Rs. {{ $lastOrder->total ?? '' }}</h4>
		</div>
		<div class="row">
			<br><br>
			
			<table class="table">
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>Quantity</th>
					<th>Sub Total</th>
				</thead>
				<tbody>
					@foreach($products as $product)
<tr>
					<td>
						<img src="{{ asset($product->image ?? '') }}" style="height: 75px;">
					</td>
					<td><a href="{{ url($product->path ?? '') }}">{{ $product->product_name ?? ''}}</a></td>
					<td>{{ $product->quantity ?? ''}}</td>
					<td>Rs. {{ $product->unit_price*$product->quantity }}</td>
				</tr>

					@endforeach
				</tbody>
			</table>


		</div>

	<div class="row" style="text-align: center;">
			<br><br>
			<a href="{{ url('/') }}" class="btn btn-success">Continue</a>
	</div>
			<br><br>
			<br><br>
</div>






@endsection