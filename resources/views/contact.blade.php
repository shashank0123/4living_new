@extends('layouts/ecommerce2')

@section('content')
<style>
     .form-control { padding: 10px; font-size: 15px !important; padding: 20px !important }
</style>
    <!-- page title begin-->
    <div class="page-title contact-page">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <h2>CONTACT US</h2>
                    <p>Contact us to know more & for any query.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- page title end -->

    <!-- contact begin-->
    <div class="address-area">
        <div class="container">
            <div class="tsk-contact-info">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="contact-info-item" style="height: 320px">
                            <div class="icon-bar">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div class="info-details">
                                <h5>Our Address</h5>
                                <p>Rz-10, Mahendra Park, Pankha Road, Uttam Nagar, Near Billu Material Supply, New Delhi, West Delhi, India - 110059</p>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="contact-info-item" style="height: 320px">
                            <div class="icon-bar">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="info-details">
                                <h5>Email Address</h5>
                                <a href="mailto:help2humancf@gmail.com">help2humancf@gmail.com</a>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="contact-info-item" style="height: 320px">
                            <div class="icon-bar">
                                <i class="fas fa-mobile-alt"></i>
                            </div>
                            <div class="info-details">
                                <h5>Phone Number</h5>
                                <a href="tel:+919315728925">+919315728925</a> / <a href="tel:+917838753662">+917838753662</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="google-map" id="map"></div> --}}
    <div class="contact " style="margin-top: 0px">
        <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contact-form-outer">
                                <div class="row justify-content-center">
                                        <div class="col-xl-8 col-lg-8">
                                            <div class="section-title text-center">
                                                <h2>Contact Us For <span>Support</span></h2>
                                                <p>Put your investing ideas into action with full range of investments.
                                                    Enjoy real benefits and rewards on your accrue investing.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-xl-12 col-lg-12">
                                                <form id="query-form">
                                                    <div class="row">
                                                        <div class="col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="InputName">Name<span class="requred">*</span></label>
                                                                <input type="text" name="name" class="form-control" id="InputName" placeholder="Enter Your Name"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="InputMail">E-mail<span class="requred">*</span></label>
                                                                <input type="email"name="email"  class="form-control" id="InputMail" placeholder="Enter Your E-mail Address"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="InputPhone">Phone<span class="requred">*</span></label>
                                                                <input type="text" name="phone" class="form-control" id="InputPhone" placeholder="Enter Your Phone Number"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="InputSubject">Subject<span class="requred">*</span></label>
                                                                <input type="text" name="subject" class="form-control" id="InputSubject" placeholder="Enter Your Subject"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">Message<span class="requred">*</span></label>
                                                                <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Enter Your Meassage"
                                                                    required></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-12 col-lg-12" style="text-align: center;">
                                                            <button type="button" class="btn btn-primary" style="padding:5px 10px; font-size: 18px" onclick="sendForm()">Send Now</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                    </div>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
    <!-- contact end -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css"></script>
    <script>
        function sendForm(){
            var request = $.ajax({
      type: "POST",
      url: "/contact-form",
      data: $('#query-form').serialize(),

      success:function(data){
           Swal(data.message);
      },

      failure:function(data){
        Swal(data.message);
      }

     
    });
        }
    </script>>
@endsection