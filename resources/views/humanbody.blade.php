@extends('layouts.ecommerce2')
@section('content')
            <div class="mobile-off-canvas-active">
                <a class="mobile-aside-close"><i class="la la-close"></i></a>
                <div class="header-mobile-aside-wrap">
                    <div class="mobile-search">
                        <form class="search-form" action="#">
                            <input type="text" placeholder="Search entire store…">
                            <button class="button-search"><i class="la la-search"></i></button>
                        </form>
                    </div>
                    <div class="mobile-menu-wrap">
                        <!-- mobile menu start -->
                        <div class="mobile-navigation">
                            <!-- mobile menu navigation start -->
                            <nav>
                                <ul class="mobile-menu">
                                    <li><a href="index.html">HOME</a></li>
                                    <li><a href="living.html">LIVE 4LivinG </a>
                                    <li class="menu-item-has-children ">
                                        <a href="#">KNOW YOUR BODY</a>
                                        <ul class="dropdown">
                                            <li><a href="humanbody.html">HUMAN BODY INTRO</a></li>
                                            <!--  <li class="menu-item-has-children">
                                                <a href="#">Page List</a>
                                                <ul class="dropdown">
                                                   <li><a href=" ">Page1</a></li>
                                                   <li><a href=" ">list 2</a></li>
                                                   <li><a href=" ">list no</a></li>
                                                </ul>
                                                </li> -->
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a>COMPANY</a>
                                        <ul class="dropdown">
                                            <li><a href="about-us.html">ABOUT US </a></li>
                                            <li><a href="directordesk.html">DIRECTOR'S DESK </a></li>
                                            <li><a href="paneldoctor.html">PANEL OF DOCTOR'S </a></li>
                                            <li><a href="consultdoctor.html">CONSULT WITH DOCTOR'S </a></li>
                                            <li><a href="contact.html">CONTACT US </a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children" >
                                        <a href="product.html">PRODUCT</a>
                                        <ul class="dropdown">
                                            <li><a href="tulsi.html">TULSI</a></li>
                                            <li><a href="imsys.html"> IMSYS</a></li>
                                            <li><a href="arthosys.html">ARTHO SYS</a></li>
                                            <li><a href="madhumeghkalp.html">MADHUMEGHKALP</a></li>
                                            <li><a href="thyrosys.html">THYRO SYS</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="login-register.html">LOG IN</a></li>
                                </ul>
                            </nav>
                            <!-- mobile menu navigation end -->
                        </div>
                        <!-- mobile menu end -->
                    </div>
                    <div class="mobile-curr-lang-wrap">
                        <div class="single-mobile-curr-lang">
                            <a class="mobile-language-active" href="#">Language <i class="la la-angle-down"></i></a>
                            <div class="lang-curr-dropdown lang-dropdown-active">
                                <ul>
                                    <li><a href="#">English (US)</a></li>
                                    <li><a href="#">English (UK)</a></li>
                                    <li><a href="#">Spanish</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="single-mobile-curr-lang">
                            <a class="mobile-account-active" href="#">My Account <i class="la la-angle-down"></i></a>
                            <div class="lang-curr-dropdown account-dropdown-active">
                                <ul>
                                    <li><a href="#">Login</a></li>
                                    <li><a href="#">Creat Account</a></li>
                                    <li><a href="#">My Account</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-social-wrap">
                        <a class="facebook" href="#"><i class="ti-facebook"></i></a>
                        <a class="twitter" href="#"><i class="ti-twitter-alt"></i></a>
                        <a class="pinterest" href="#"><i class="ti-pinterest"></i></a>
                        <a class="instagram" href="#"><i class="ti-instagram"></i></a>
                        <a class="google" href="#"><i class="ti-google"></i></a>
                    </div>
                </div>
            </div>
            <!-- header end here -->
            <div class=" mt-30 mb-30">
                <img class="img-responsive"  src="{{ URL::asset('asset/images/bg/breadcrumb.jpg')}}">
            </div>
            <!--   -->
            <div class="container-fluid mt-40 mb-50 ">
                <h2 class="sechead">Human Body Introduction </h2>
                <hr class="hrstyle">
                <div class="row lrmargin pagemarginsec" >
                    <div>
                        <p>The human body, with all its powers of endurance, its life-preserving systems for combating disease and taking in nourishment, and its ability through the senses to interpret what is happening in the world outside it, is like some marvelously complex machine. But unlike a machine, it also has the capacity for pleasure and sensitivity to pain. And no machine, however futuristic, could match the body's ability to grow and to repair broken bones and damaged tissues, or it's even more remarkable ability to maintain or multiply the human population by generating new life. All these powers and capacities, all the strengths and intricacies of the body, could be reduced to a few handful of chemical elements such as oxygen, hydrogen, proteins, minerals, fats, trace elements and water which are all contained in the body's cells - microscopic structures, only a few hundredths of millimeter in diameter, but with the ability to absorb nourishment, grow, excrete wastes and increase in numbers by dividing in two.</p>
                        <h4 class="phead">Systems Of The Body</h4>
                        <hr class="hrstyle2">
                        <p>Various collections of cells make up body tissues such as skin, muscle, and bone; and tissues are grouped to form organs, such as the heart, lungs, and stomach. A set of organs make up a system, and the ultimate physical aim of the systems, working together, is to convert food into energy to keep the body working. Foodstuffs are eaten; prepared in the mouth into a form suitable for digestion; broken down in the digestive system into smaller units by the action of chemical substances called enzymes, and absorbed into the body; where they are partly used as fuel. For the burning of fuel in the body, as for any form of combustion, a supply of oxygen is needed. This is taken in from the lungs - a major function of the respiratory system - and distributed to the tissues by the blood, carried in the blood vessels and pumped by the heart. The blood vessels and the heart make up the circulatory system, which also conveys foodstuffs and waste products around the body. The body needs to get rid of waste products formed by the release of energy, the process known as excretion. Carbon dioxide and some water are excreted from the lungs in the air breathed out; a little water and salts are lost from the skin in sweat, and water and salts, together with complex waste products such as urea, and uric acid, are excreted from the kidneys. These form part of the urinary system. This also includes connecting tubes, known as the Ureters (from the kidneys to the bladder) and the urethra (from the bladder to outside the body). The bladder itself is merely a reservoir of urine. Fibrous wastes and indigestible food residues pass out of the body in the feces. Some of the energy produced by the body keeps the various systems working and the rest is used for movement. This is carried out by the locomotor system, consisting of muscles, which act on the bony skeleton. The bones have an important role, not only in providing a framework for the whole body but also in protecting the vital organs, such as the lungs and the brain, from injury. Acting on information provided by the sense organs, such as the eyes and ears, the brain and the rest of the nervous system can control a variety of bodily processes, either directly or by causing various glands to release hormones- chemical messengers, which in turn act on the tissues. The release of the many types of hormones into the bloodstream is controlled by the endocrine system, a series of glands in different parts of the body that regulate growth and the ability to reproduce. The body also needs to be maintained. Treatment with medicines or surgery may be required to repair the damage by injury or disease, but often the body can cope with the problem by itself. The body's repair system consists of the normal continual process of replacing worn-out and damaged tissues. The ability to repair itself without outside help and the ability to grow, which is particularly obvious during childhood are two of the physical properties distinguishing the living organisms from non-living organisms. A third is a body?s ability to perpetuate the species by reproduction - the role of the reproductive system. This involves the creation of new life by the joining of two sex cells - sperm and an egg from the parents, and the subsequent development while protected inside the mother's body.</p>
                        <h4 class="phead">Musculoskeletal System</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> At birth, we have over 300 bones. As we grow up, some of these bones fuses, as a result, an adult has only 206 bones.</li>
                            <li>  The Human hand has 27 bones.</li>
                            <li> The human body has 230 movable and semi- movable joints.</li>
                            <li> Evolve as a world-class company offering many fold business solutions. Ensures a private equity partnership to raise funds.</li>
                            <li> The Human skull is made up of 29 different bones.</li>
                            <li> The strongest muscles of the human body are masseters, these are present on either side of the mouth and help with chewing and grinding food in our mouth.</li>
                            <li> The Thighbone is so strong that it withstands the axial load of about 1600-1800 kilos.</li>
                            <li> Most of the bones in the human body constitute about ¾ of water.</li>
                            <li> The whole leg consists of 31 bones.</li>                   
                            <li> Almost every seven years, the human body replaces the equivalent of an entirely new skeleton.</li>
                            <li> Laughing and coughing creates more pressure on the spine than walking or standing.</li>
                            <li> The shoulder blade is connected to the body using 15 different muscles and it is not attached to a single bone.</li>
                            <li> An average of 17 muscle contracts for a smile.</li>
                            <li> The human body consists of over 600 muscles.</li>
                            <li> The middle part of the back is the least sensitive part of our body.</li>
                            <li> The tongue is the most versatile muscle in the human body.</li>
                            <li> The longest muscle in the human body is the sartorius, which is present in the hip region and it is commonly known as "Tailor's muscle".</li>
                            <li> The smallest muscle in the human body is the stapedius, which is present deep inside the ear.
                            <li> The muscles of our body constitute 40% of our body weight.</li>
                            <li>  Our muscles often work in pairs so that they can pull in different or opposite directions.</li>
                            <li>  An average person laughs about 15 times a day.</li>
                        </ol>
                        <h4 class="phead">Skeletal System</h4>
                        <hr class="hrstyle2">
                        <p><b>Bones</b> More than 206 bones support the body and protect organs such as the brain, heart, and lungs. The bones of the skeleton act as a frame to which muscles are attached. These skeletal muscles allow the body to move; they are attached to bones by bands of tough elastic tissue, called tendons, and it is utilizing tendons that they exert their pull. Another important task of bones is to produce blood cells. Finally, the bones provide a store of chemicals such as calcium salts, which are released into the bloodstream, as they are needed.</p>
                        <h4 class="phead">Types of bones</h4>
                        <hr class="hrstyle2">
                        <p>Man has evolved with bones specialized into four main types, each with a different role:-
                        <ol>
                            <li><b> The Long Bones:</b> eg. in the limbs, they are thin, hollow and light, they play as an essential role in all types of movements.</li>
                            <li><b>  Flat Circular Bones:</b> eg. bones that form the spine or vertebral column.</li>
                            <li><b> Long Circular Bones:</b> eg. Ribs, are strong but elastic giving the chest the flexibility and springiness it needs for breathing.</li>
                            <li><b> Flat Irregular Bones:</b> eg. shoulder, blades, hips and skull these are strong but light and protect delicate organs, such as the brain.</li>
                        </ol>
                        <hr>
                        <h4 class="phead">The framework of the body</h4>
                        <hr class="hrstyle2">
                        <p>The body owes its shape & support to the skeleton a frame consisting of hundreds of joint bones.</p>
                        <br>
                        <p><b>Head:</b> The bones of the skull surround & protect the brain; the lower jaw is hinged to the skull.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/head.jpg')}}">
                        <br><br>
                        <p><b>Chest: </b>The bony cage of the ribs, connected to the spine at the back and the breastbone at the front, surround and protect the organs in the chest.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/chest.jpg')}}"><br><br>
                        <p><b>Arm:</b> The bones of the arms are joint to sockets in the shoulder blades.</p>
                        <br><br>
                        <p><b>Spinal Column:</b>  The 7 vertebrae in the neck and the 20 in the back make the spinal column.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/spinal.jpg')}}"><br><br>
                        <p><b>Pelvis:</b>  The bones of the pelvis surround the lower abdominal organs, support the spine and provide attachment for the legs.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/pelvis.jpg')}}"><br><br>
                        <p><b>Hand:</b>  Eight bones make up the wrist, five the palm and 14 hinged bones form the fingers and thumb.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/hand.jpg')}}"><br><br>
                        <p><b>Leg:</b>  Three major leg bones are suspended by ball and socket joints from the pelvis</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/leg.jpg')}}"><br><br>
                        <p><b>Foot:</b>  The bones of the foot form arches, so that the weight is carried on the heel and toes.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/foot.jpg')}}"><br><br>
                        <h4 class="phead">Muscles</h4>
                        <hr class="hrstyle2">
                        <p>There are about 650 muscles in the body, and they are divided into three different types, skeletal, visceral and cardiac The skeletal muscles move the arms, legs, and spine The visceral muscles control movements in the walls of blood vessels, the stomach, and intestines. The cardiac muscles produce the pumping action of the heart. All the muscles are present in the body at birth, and everyone has the same number of muscles, consisting of the same number of fibers</p>
                        <hr>
                        <h4 class="phead">Nervous System</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li>A newborn baby's brain grows almost 2 times during  its first year.</li>
                            <li>TThe left side of the human brain controls the right side of the body and the right side of the brain controls the left side of the body.</li>
                            <li>A Newborn baby loses about half of their nerve cells before they are born.</li>
                            <li>As we get older, the brain loses almost one gram per year.</li>
                            <li>There are about 13, 500,00 neurons in the human spinal cord.</li>
                            <li>The total surface area of the human brain is about 25, 000 square cm.</li>
                            <li>The base of the spinal cord has a cluster of most sensitive nerves.</li>
                            <li>An average adult male brain weighs about 1375 grams.</li>
                            <li>An average adult female brain is about 1275 grams.</li>
                            <li>Only four percent of the brain's cells work while the remaining cells are kept in reserve.
                            <li> The Brain utilizes 20 % of our body's energy ie, it uses 20% of one's blood and oxygen and makes up only 2 % of our body weight.</li>
                            <li> The Human brain stops growing by 18 years of age.</li>
                            <li> The human brain is very soft like butter.</li>
                            <li> Sixty percent of the human body's nerve ends in the forehead and the hands.</li>
                            <li> The brain continues to send out electric wave signals until approximately 37 hours after death.</li>
                            <li> It is estimated that there are over 1, 000,000,000,000,000 connections in the human brain.</li>
                            <li> The human brain constitutes 60 % of white matter and 40 % of grey matter.</li>
                            <li> The average length of the human brain is about 167 mm and its average height is 93mm.</li>
                            <li>On average, 100, 000 to 1000, 000 chemical reactions take place in our brain.</li>
                            <li> The Nervous system transmits messages to the brain at the speed of 180 miles per hour.</li>
                            <li> The spinal cord, which controls over 10 billion nerve cells, is less than two feet in length and its diameter is the same as that of the index finger.</li>
                            <li> Reading aloud to children helps to stimulate brain development.</li>
                            <li> The right side of the human brain is responsible for self-recognition.</li>
                            <li> Men listen with the left side of the brain and women use both sides of the brain.</li>
                            <li> The human brain is made up of a staggering 15 billion cells with about a thousand billion connections between these cells. It, however, weighs less than 2 pounds.</li>
                        </ol>
                        <h4 class="phead">Anatomy of Nervous System</h4>
                        <hr class="hrstyle2">
                        <p>The Central nervous system consists of the brain, the spinal cord, and the body’s nerve network. This complex system is based on one kind of cell the neurons. The brain, the mass of tissue inside the head, has the greatest number of these cells, most of which are in its outer part, the cerebrum. Below the cerebrum are the cerebellum and the brain stem, which is linked to the spinal cord.
                        </p>
                        <p><b>The Brain:</b>  The brain has been compared to a giant telephone exchange or a computer. It functions as both, handling incoming and outgoing calls, and making decisions, as diverse as whether to laugh or cry and whether the temperature of the body should be higher or lower, based on information fed into it.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/brain.jpg')}}"><br><br>
                        <p><b>Cerebrum: </b>  The brain’s most obvious external features are two soft hemispheres, which make up the cerebrum. These hemispheres make up 70% of the whole brain and nervous system. They are “mirror images” of each other, and each is chiefly concerned with the movements and sensations of only one side of the body. Sensations on the right side of the body and the control of the muscles on that side are functions of the left hemisphere, and vice versa. It consists of 2 layers: (1) outer cortex or grey matter - which is the decision-maker of the brain, (2) Inner layer of white matter - made up of nerve fibers.</p>
                        <p><b>Cerebellum: </b> The cerebellum functions below the level of consciousness. It is concerned with balance and is the center for the co-ordination of complex muscular movements.</p>
                        <p><b>Brain Stem: </b> Links the spinal cord to the brain. They lie below the cerebral hemispheres.</p>
                        <p><b>The Spinal Cord: </b> The spinal cord is the body’s main nerve trunk-a cylinder of nerve tissue 18 inches long about as thick as a man’s little finger. It runs down the back from the medulla oblongata, at the base of the brain. It is enclosed in a set of two membranes, similar to those surrounding the brain. Between the layers of membranes, Cerebro-spinal fluid acts as a cushion, to protect the cord from damage.
                        </p>
                        <p><b>Nerve Fibres: </b> The spinal cord is a column of nervous tissue, which is spread throughout the body; they carry impulses to and from the brain. Nerve fibers from the brain and spinal cord are bundled together to form 12 pairs of cranial nerves, connected to the brain and 31 pairs of spinal nerves.
                        </p>
                        <h4 class="phead">Spinal Nerves</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> Cervical Nerves - (8 pairs) serve mainly the arms.</li>
                            <li>  Thoracic Nerves - (12 pairs) lead to the sternum, internal organs and muscles of the chest.</li>
                            <li> Lumbar Nerves - (5 pairs) serve the abdominal wall and legs.</li>
                            <li> Sacral & Coccygeal Nerves - (6 pairs) lead mainly to the legs.</li>
                        </ol>
                        <h4 class="phead">Carnival Nerves</h4>
                        <hr class="hrstyle2">
                        
                        <p>The brain has links with the sense organs and the muscles of the head utilizing 12 pairs of cranial nerves</p>
                        <ol>
                            <li><b>Olfactory:</b> a sense of sight balance</li>
                            <li> <b>Optic:</b> a sense of sight balance</li>
                            <li> <b>Oculomotor:</b> Focusing, regulating the size of the pupil, balance</li>
                            <li> <b>Trochlear:</b> Movement of the eyeball</li>
                            <li> <b>Trigeminal:</b> Chewing, the sensation from the Facebook</li>
                            <li> <b>Abducent:</b> Movement of the eye, sense of taste</li>
                            <li> <b>Vestibulocochlear:</b> Maintenance of balance, sense of hearing</li>
                            <li><b> Glossopharyngeal:</b> Secretion of saliva, sense of taste, movement of pharynx</li>
                            <li><b> Vagus:</b> Movement and secretion</li>
                            <li><b> Accessory:</b> Movement of the head, shoulders, pharynx, and larynx</li>
                            <li><b> Hypoglossal:</b> Movement of the tongue</li>
                        </ol>
                        <h4 class="phead">Autonomic Nervous System:</h4>
                        <hr class="hrstyle2">
                        <p>The autonomic system controls glands, such as the salivary glands, and the internal organs-the bladder, heart, intestines, liver, lungs and sexual organs. Nearly all the actions of the autonomic system are outside voluntary control eg. You cannot normally “will” your heart to beat faster; but if you are given a fight, your pulse involuntarily speeds up. The autonomic division of the nervous system consists of two opposing parts:</p>
                        <br>
                        <ol type="A">
                            <li>The Sympathetic Nerves</li>
                            <li>The Parasympathetic Nerves which operates below the level of consciousness</li>
                        </ol>
                        <br>
                        <h4 class="phead">The Sympathetic Nerves:</h4>
                        <hr class="hrstyle2">
                        <p> Through the sympathetic nerves, the brain mobilizes the body for action to meet possible danger
                        </p>
                        <ol>
                            <li> IRIS - Changes size, when someone is frightened or angry, the brain stimulates the sympathetic nerves to their part of the eye, causing the pupils to open wide.</li>
                            <li>  SALIVARY GLANDS - produces less saliva, so that the mouth goes dry.</li>
                            <li>  LUNGS & WINDPIPE - are affected under stress; breathing becomes faster so that the body gets more oxygen.</li>
                            <li>  HEART - pumps faster, during times of fear & anger. Normally you are unaware of the beating of the heart, but its increased activity in times of excitement raises the blood pressure, pumping more blood to supply energy for muscles.</li>
                            <li>  ADRENAL GLANDS - at the top of the kidneys secrete the hormone adrenaline, which prepares the body to fight or run away</li>
                            <li>  LIVER - releases glucose under emotional stress, providing extra energy for muscles.</li>
                            <li>  STOMACH & INTESTINES - have their blood diverted to the heart, CNS and muscles so that they can operate under stress. The wave-like movements of the intestinal walls stop, and the various sphincters close</li>
                        </ol>
                        <h4 class="phead">Parasympathetic Nerves:</h4>
                        <hr class="hrstyle2">
                        <p>are concerned with restoring the body to peaceful activity after an emergency</p>
                        <ol>
                            <li>  <b>Heart -</b> slows down & the blood pressure falls after the danger is over.</li>
                            <li>  <b>Bladder -</b> can be contracted and its sphincter may open, causing urination.</li>
                        </ol>
                        <h4 class="phead">Circulatory System</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> The heart muscles will stop working only when we die.</li>
                            <li> Every second, 15 million blood cells are destroyed in the human body.</li>
                            <li> Platelets, which form a part of the blood cell component are produced at the rate of 200 billion per day.</li>
                            <li> An adult human body contains five to six liters of blood and an infant has about one liter of blood.</li>
                            <li> Except for the heart and lungs, all the other parts of the body receive their blood supply from the largest artery of the body, the aorta.</li>
                            <li> The Pulmonary vein is the only vein in the human body that carries oxygenated blood while all the other veins of the body carry de-oxygenated blood.</li>
                            <li> Human blood is colorless. It is the hemoglobin; a pigment present in the red blood cells that is responsible for the red color of the blood.</li>
                            <li> Heartbeat is nothing but the sound produced by the closure of valves of the heart when the blood is pushed through its chamber.</li>
                            <li> A women's heartbeat is faster than that of a man's.</li>
                            <li> The human heart continues to beat even after it is taken out of the body or cut into pieces.</li>
                        </ol>
                        <h4 class="phead">Anatomy of Circulatory System</h4>
                        <hr class="hrstyle2">
                        <p>The human circulatory system consists of blood which is kept in motion or circulation by the pump called the heart and the pipes called blood vessels. The heart is a nonstop pump that pushes the blood through the arteries and supplies the body cells oxygen and glucose along with other essential nutrients. The waste from the cells including carbon-di-oxide is brought back through the veins and pumped by the heart into the lungs for purification. In human beings and other multicellular animals, the transport of oxygen and nutrients for the cells of the body takes place by a fluid medium called the blood. The blood constantly moves around the body by the circulatory system.</p>
                        <h4 class="phead">Blood</h4>
                        <hr class="hrstyle2">
                        <p>Blood is an important fluid connective tissue and composed of the following components</p>
                        <ol>
                            <li>  Formed Elements (R.B.C, W.B.C, PLATELETS)</li>
                            <li>  Plasma</li>
                        </ol>
                        <br><br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/blood.jpg')}}"><br><br>
                        <h4 class="phead">Formed Elements:</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> <b> R.B.C (Red Blood Cells or Erythrocytes)</b></li>
                            <ul>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Total Number = 5 million cells/ cubic mm of blood</li>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Shape = dumbbell-shaped</li>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Nuclei = absent</li>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Main Constituent= hemoglobin (a red pigment which is made up of protein and iron)
                                </li>
                            </ul>
                            <li><b>  W.B.C. (White Blood Cells or Leucocytes)</b></li>
                            <ul>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Total Number = 7000-10,000 cells / cubic mm of blood</li>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Nuclei = present</li>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Function = WBC consumes bacteria, viruses, and debris that enter the body and form special proteins, called antibiotics that protect against infection.</li>
                            </ul>
                            <li><b> PLATELETS: (or Thrombocytes)</b></li>
                            <ul>
                                <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Total Number = 400,000 / cubic mm of blood.</li>
                                <li> <span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Function = important role in the formation of a solid plug called clot at the site of injury to a blood vessel, to prevent further loss of blood.</li>
                            </ul>
                        </ol>
                        <h4 class="phead">Plasma:</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li>  It is the liquid part of blood into which float different types of blood cells; i.e. RBC, WBC & platelets</li>
                            <li>  It contains several salts, glucose, amino acids, proteins, hormones, and also digested and excretory products of food.</li>
                            <li> The serum is blood plasma from which the blood-clotting protein called fibrinogen is removed.</li>
                        </ol>
                        <h4 class="phead">How the Circulatory System Functions Heart:</h4>
                        <hr class="hrstyle2">
                        <p>The heart is the muscular pump like organ that circulates blood through the body. The muscles of the heart contract periodically and cause the heart to pump blood. The heart contracts about 72 times a minute when an adult person is at rest, but this rate increases to 100 or more during activity or excitement. The total volume of blood in the system is about 5 to 6 liters. The heart pumps approximately 5 liters of blood out every minute.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/heart.jpg')}}"><br>
                        <h4 class="phead">Blood Vessels:</h4>
                        <hr class="hrstyle2">
                        <p>The 2 types of blood vessels are arteries, veins, and capillaries and they are all connected to form one continuous closed system.</p>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/vessals.jpg')}}">
                        <h4 class="phead">Arteries:</h4>
                        <hr class="hrstyle2">
                        <p>
                            They are the widest blood vessels having thick and elastic walls; arteries branch out into thinner tubes called arterioles, which again branch into thinner capillaries.
                        </p>
                        <h4 class="phead">Capillaries:</h4>
                        <hr class="hrstyle2">
                        <p>Capillaries are tiny blood vessels with walls that are just one cell thick. These walls are permeable to water and CO2, which are exchanged with tissues surrounding the capillaries. Capillaries ultimately joint to form venules and at last veins return blood to the heart. Thus, arteries take blood from the heart and supply it to various tissues via the capillaries and veins return blood from the tissue to the heart. For maintaining such a unidirectional flow of blood, large veins have valves in them. The pressure of blood flow opens them in the direction of flow and closes them otherwise. Arterial blood is rich in oxygen and dissolved food, while venous blood carries CO2 and waste material. However, the pulmonary artery and pulmonary vein form two important exceptions to it. Pulmonary artery supplies lungs CO2 - rich blood and pulmonary vein collects oxygen-rich blood from the lungs and sends it to heart.</p>
                        <h4 class="phead">Urinary System:</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> The Human bladder can stretch to hold about 400ml of urine.</li>
                            <li> All the blood in our body passes 400 times through each kidney every day.</li>
                        </ol>
                        <p>Excretion can be defined as the removal of toxic waste products of metabolism from the body. These wastes can be either solid, liquid or in the gaseous state. The liquid wastes are ammonia and urea, which exist in the blood along with the nutrients and other useful substances. So there is a need for complex organs that may separate or filter out the dissolved excretory wastes from the blood while retaining the nutrients in the latter. Two kidneys in human beings are such organs that perform this task. There is a distinct advantage of the two kidneys in our bodies. If one kidney fails, the other can still deal with functions of excretion and regulation.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/pelvis.jpg')}}"><br><br>
                        <p>The kidneys are solid; bean-shaped, reddish brown-paired structure, which lies in the abdominal cavity one on either side of the vertebral column. They are about 11 to 12 cms in length, 5 to 6 com in breadth and about 2 cms in thickness and weigh about 120 to 150 gms each. The kidney is approximately the size of your fist. They are well protected and lie in the retroperitoneal section of the abdomen which means that they lie behind the stomach and other digestive organs and nearer to the back of the body.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/kidney.jpg')}}"><br><br>
                        <p>A cross-section of the kidney shows a darker outer zone called the cortex and a lighter inner zone called the medulla. A funnel-shaped hollow pelvis meets the ureter and the junction is called the pelvic-ureteric junction. This junction can have a functional blockage giving rise to obstruction to flow of the kidney. Each kidney is made up of about 1 million tubules that are the basic functioning unit of the kidney that can make urine and these are known as nephrons. About 180 liters of blood, which run through these nephrons daily produce just one to two liters of urine by the process of filtration, reabsorption, and secretion by the nephrons. The urine enters the pelvis of the kidney where it collects and continues down the ureters to the bladder. In the urinary bladder, urine is temporarily stored and is finally eliminated from the body. The bladder has an average capacity of about 400 milliliters. The urinary bladder is a muscular reservoir for the urine and can expand without exerting any pressure within the bladder. Its function is to store and evacuate urine. The urine is released periodically to the outside via the urethra.
                        </p>
                        <h4 class="phead">Skin and Hair</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> 80 hairs are likely to fall every day.</li>
                            <li> The human skin contains 45 miles of nerves.</li>
                            <li> In one square inch of skin, there are 4 yards of nerve fibers.</li>
                            <li> An average, the human scalp has 100,000 hairs.</li>
                            <li> Fingernails grow faster than toenails.</li>
                            <li> Nails of toes and fingers take about 6 months to grow from base to tip.</li>
                            <li> In one square inch of skin, there are 2 million cells.</li>
                            <li> There are 100 receptors in each of our fingertips.</li>
                            <li>The total weight of skin in an average human adult is 61 pounds.</li>
                            <li> The human skin contains 280,000 heat receptors.</li>
                        </ol>
                        <h4 class="phead">Anatomy of Skin & Hair</h4>
                        <hr class="hrstyle2">
                        <p>The skin, which forms the outer covering of the body, has a surface area of about 1.5 - 2m2 in adults and it contains glands, hair, and nails.</p>
                        <br><br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/Hair.jpg')}}"><br><br>
                        <h4 class="phead">Structure of Skin</h4>
                        <hr class="hrstyle2">
                        <p>Skin is made up of two layers:</p>
                        <ol>
                            <li> The Epidermis (outer covering)</li>
                            <li> The Dermis (true skin)</li>
                        </ol>
                        <h4 class="phead">The Epidermis:</h4>
                        <hr class="hrstyle2">
                        <p>
                            The outermost part of the epidermis consists of flat, dead cells that are constantly being shed.
                            The underlying part of the epidermis is made up of rapidly dividing cells. These are continually pushing upwards and replacing the dead cells above them.
                            Specialized epidermis cells that extend downwards into the dermis produce hair and nails, which are also composed of dead cells.
                        </p>
                        <h4 class="phead">The Dermis:</h4>
                        <hr class="hrstyle2">
                        <p>The dermis is made up of tiny blood vessels and nerve endings that are densely woven into the flexible connective tissue. Sweat glands and oil glands are embedded in it.</p>
                        <h4 class="phead">General Features Of Skin</h4>
                        <hr class="hrstyle2">
                        <ol>
                        <li>  It is a barrier against germs, and a tough resilient cushion that protects the tissues underneath, and helps to regulate the body temperature.</li>
                        <li>  When it is hot, glands in the skin secrete sweat, the evaporation of which causes cooling. Or when it is cold, constriction of the blood vessels in the skin cuts down the flow of blood near the body’s surface and so reduces heat loss.</li>
                        <li>  Just below the surface of the skin are millions of tiny nerve endings. These are the touch receptors that tell us about the world through five different kinds of sensations pain, cold, heat, pressure, and contact.</li>
                        <li>  The skin supplies much of the body’s vitamin D requirement by producing substances that changes into vitamin D when it is exposed to the Ultraviolet radiation in sunlight.</li>
                        <li>   Skin is thinnest on the lips and thickest on the scalp, palms of the hands and soles of the feet (Continual pressure or friction can cause the skin to thicken.</li>
                        <li>   Hair follicles are found on nearly the whole body, being abundant on the scalp but absent in the skin of the soles and palms.</li>
                        <li>  Sweat glands, oil glands, and nerve endings are also unevenly distributed. There is a concentration of sweat glands in the armpits; nerve endings are most abundant in the lips and fingertips.</li>
                        <li>   The total weight of skin in an average human adult is 61 pounds.</li>
                        <li>   Skin owes its color partly to the blood, redness of which shows through translucent tissues, and partly to various pigments in the epidermis.</li>
                        <h4 class="phead">Skin Infections</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> <b>  Impetigo:</b>Usually caused by group A hemolytic streptococci or coagulase-positive Staphylococcus aureus. Appear as redness, thin yellowish crusts, and even bullae, which may be localized or widespread on the skin and develop over days. Itching, pain, and tenderness may occur. Moderately contagious. Treatment is with mupirocin 2% ointment BID or systemic antibiotics, daily bathing with antibacterial soap, and attention to personal hygiene. Need to monitor for the development of post-strepto-coccal glomerulonephritis.</li>
                            <li><b>  Ecthyma: </b>Considered a deeper extension of impetigo with the same cause, except it may also be caused by Pseudomonas organisms. It is characterized by a hemorrhagic crust with erythema or induration that develops over weeks. Treatment includes systemic antibiotics as well as débridement of the epidermis, which becomes necrotic. Scars may occur after healing. Cellulitis. Usually caused by group A beta-hemolytic streptococci, it is a suppurative inflammation of the dermis and subcutaneous tissue. Usually follows trauma or underlying dermatosis, and there is moderate local erythema, tenderness, warmth, and tenseness. The area can become indurated, and frequently streaks of lymphangitis can be seen with the involvement of the regional lymph nodes. Systemic symptoms are common, and bacteremia and septicemia may follow. Treatment is with systemic antibiotics and the application of local heat, elevation, and immobilization. For necrotizing fasciitis and synergistic gangrene, early wide surgical excision and débridement is necessary for addition to IV antibiotics.</li>
                            <li> <b> Folliculitis:</b> (including sycosis barbae [barber’s itch], pseudofolliculitis, and hot-tub folliculitis). A common problem with predisposing factors such as maceration, friction, and the use of irritant chemicals. Usually caused by S. aureus but occasionally Klebsiella, Pseudomonas (hot-tub folliculitis), Enterobacter or Candida albicans are the causative agents. Appears as a pustule with a central hair (follicle) with or without any surrounding erythema. Scarring may occur with the destruction of the hair follicle with severe infections. Tenderness, itching, and pain may occur. Treatment includes avoidance of inciting agents, antiseptic soap washes, and, in severe cases, topical or systemic antibiotics such as dicloxacillin or erythromycin 500 mg QID x 7 to 10 days and mupirocin 2% ointment topically. Complications can include cellulitis, furunculosis, and alopecia.</li>
                            <li><b>  Furuncle (boil):</b> An acute, localized perifollicular abscess of the skin and subcutaneous tissue caused by coagulase-positive S. aureus resulting in a red, hot, very tender inflammatory nodule that exudes pus from one opening. A carbuncle is an aggregate of connected furuncles and characteristically is painful and has several pustular openings. This can be an acute or chronic problem with lesions commonly on areas of friction such as buttocks, axillae, breasts, and the nape of the neck. Treatment involves systemic antibiotics local heat, and rest. Incision and drainage are generally required. Prevention is often difficult. Improved personal hygiene, use of antibacterial soaps, frequent hand washing, daily bathing, and change of clothing are important. Elimination of carrier states in the nose and perineum by the use of topical and systemic antibiotics is often possible.</li>
                        </ol>
                        <h4 class="phead">Viral Infections</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li><b>  Warts</b> (caused by the human papilloma-viruses): The lesions are most common on the hands, feet, and face. They are infectious and auto inoculable. Common in children, the elderly, or patients with immunologic deficiencies or atopic dermatitis. Treatment is with mild destructive chemicals (salicylic and lactic acid preparations), liquid nitrogen therapy, or electrodesiccation. Recurrences are common (25%). Cicatrix caused by treatment may be painful and is often confused with the persistence of the wart, especially on the sole of the foot.
                            </li>
                            <li>
                                <b> Herpes simplex </b>viruses types I and II are DNA viruses: The early lesions are multiple, 1 to 2 mm in diameter, yellowish, clear vesicles on an erythematous base. The vesicles can ulcerate and become quite painful. Classic type I herpes occurs around the mouth, and type II occurs on the genitalia, but either type I or type II can occur anywhere on the skin. Diagnosis can be made from the clinical appearance, the serologic reaction in acute and convalescent sera for primary infections. Tzanck smear (Wright’s stain of material obtained from the base of the lesion showing multinucleated giant cells), biopsy, or viral culture. A prodrome of pain or discomfort or tingling is often reported a week to 10 days before the lesions are seen. Treatment is symptomatic with cool compresses, analgesics, and topical drying agents for the oozing, weeping stages. Acyclovir has only a modest effect on recurrent genital herpes and does not seem to influence subsequent episodes; it is thus not recommended for therapy for recurrent attacks in the immunologically competent host. It may be indicated in persons who experience frequent, severe recurrences with complications. Some clinical infection syndromes are listed below:
                                <ul>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Gingivostomatitis: This occurs periodically in children and young adults.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Keratoconjunctivitis Ophthalmology consultation is warranted: Usually heals without scarring.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Vulvovaginitis.Herpes gladiatorum. This occurs on the head, neck, or shoulder. Common in wrestlers.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Eczema Herpeticum: This occurs in those with underlying skin disorders, most commonly in atopic dermatitis. It occurs more frequently in children than in adults. Consists of disseminated umbilicated vesicles confined to eczematous skin, which evolve into punched-out erosions that may become confluent.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Hepatorenal Necrosis and Encephalitis.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Herpetic whitlow (herpetic paronychia): Occurs on the distal portion of fingers.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Cold sores.</li>
                                </ul>
                            </li>
                            <li> <b>Herpes zoster (shingles):</b> Reactivation of latent virus present in the sensory ganglia. The classic description is that of grouped vesicles on an erythematous base in one dermatome. Thoracic nerve dermatomes are most commonly involved followed by the major branches of the trigeminal nerve. Symptoms are pain, dysesthesia, and pruritus. Healing requires 2 to 2 weeks, and the afflicted persons are infectious until the lesions have crusted over. Persons of any age can be affected, but the disease is more common and more severe in the elderly. Diagnosis is by clinical presentation, though Tzanck smear, biopsy, and viral culture may be performed. Treatment is oral acyclovir, 800 mg 5 times per day, which is effective if treatment is initiated within 2 days of the onset of the rash. Acyclovir is very effective for pain relief. Alternatively, famciclovir 500 mg PO TID for 7 days can be used and may be more effective at preventing postherpetic neuralgia. Capsaicin creams can be used for pain relief after the lesions have healed. Amitriptyline 25 to 150 mg QHS may be useful in the treatment of postherpetic neuralgia. For recurrent herpes zoster, particularly if more than one dermatome is involved, consider a work-up for malignancy or other causes of immunosuppression.</li>
                            <li> <b>Molluscum contagiosum:</b> Caused by a DNA virus. Appear as pearly papules up to 5 mm in diameter having a central dimple. Multiple lesions are usually present. The central core (molluscum body) can be expressed with a blade. The lesions are infectious, and autoinoculation is common. Children are most commonly affected. Spontaneous resolution may occur, but there is often an eczematous reaction before its resolution. Treatment can be limited to simple superficial curettage without anesthesia. The removal of the molluscum body, application of 50% trichloroacetic acid, or liquid nitrogen cryotherapy is equally efficacious.</li>
                            </li>
                        </ol>
                        <h4 class="phead">Fungal Infections</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li><b>  Candidiasis:</b> Caused by Candida albicans. Seen as thrush diaper dermatitis, perineal infections, and intertriginous dermatitis. Diagnosis is by clinical exam, and microscopic examination of skin scraping in 10% KOH reveals yeast forms and budding hyphae. Treatment of choice miconazole 2% cream BID to affected areas for superficial fungal infections. Chronic mucocutaneous involvement can be treated with ketoconazole 200 to 600 mg PO daily (alternatives itraconazole or fluconazole). The multifocal invasive disease requires intravenous amphotericin B. Persons who present with recurrent infections should have an investigation of other causes such as diabetes mellitus, hypoparathyroidism, Addison’s disease, malignancies, HIV. The use of steroids and antibiotics is also a predisposing factor.</li>
                            <li>
                                <p><b> Dermatophytoses (tinea):</b> The fungi belonging to the genera Trichophyton, Microsporum, and Epidermophyton infect the stratum corneum of the epidermis, hair. and nails. Commonly referred to by the locus of infection, that is, tinea unguis (nails), tinea pedis (foot, athlete’s foot), tinea cruris (perineum, jock itch), tinea corporis (body, ringworm), and tinea capitis (scalp and hair). Lesions can appear as grayish, scaling patches that can be quite pruritic and may lead to autoinoculation of scalp alopecia. Skin scraping in 10% KOH will demonstrate fungal hyphae. Infected hairs, when examined under Wood’s light, will fluoresce a green-yellow color. Treatment is as follows:</p>
                                <ul>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Tinea corporis (body, ringworm), tinea cruris (perineum, jock itch), tinea pedis (foot, athlete’s foot). Topical tolnaftate or clotrimazole TID until clear and then 1 to 2 weeks longer.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Tinea capitis (scalp and hair). Micronizedgriseofulvin is usually used for up to 4 to 8 weeks. Adjunctive therapy includes selenium sulfide shampoo q2-2 days.</li>
                                    <li> <span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Tinea unguis, or onychomycosis (nails). Griseofulvin 500 mg BID for a period of 4 to 6 months or itraconazole 200 mg BID for 4 months (1 week on; 2 weeks off); latter regimen is very expensive. An alternative is terbinafine 250 mg PO QD for 12 weeks or BID for 1 week of the month for 2 or 4 months.</li>
                                </ul>
                            </li>
                            <li><b> Folliculitis</b> (including sycosis barbae [barber’s itch], pseudofolliculitis, and hot-tub folliculitis): A common problem with predisposing factors such as maceration, friction, and the use of irritant chemicals. Usually caused by S. aureus but occasionally Klebsiella, Pseudomonas (hot-tub folliculitis), Enterobacter or Candida albicans are the causative agents. Appears as a pustule with a central hair (follicle) with or without any surrounding erythema. Scarring may occur with the destruction of the hair follicle with severe infections. Tenderness, itching, and pain may occur. Treatment includes avoidance of inciting agents, antiseptic soap washes, and, in severe cases, topical or systemic antibiotics such as dicloxacillin or erythromycin 500 mg QID x 7 to 10 days and mupirocin 2% ointment topically. Complications can include cellulitis, furunculosis, and alopecia.</li>
                            <li><b>  Pityriasis (tinea) Versicolor:</b> Appears as slightly pigmented superficial tan scaling plaques of various sizes, primarily on the neck, trunk, and proximal area of the arms. With sun exposure, the infected regions do not tan and appear hypopigmented. Usually caused by Malassezia furfur (Pityrosporum orbicular). Diagnosis is by clinical exam and KOH preparations of a skin scraping. Treatment can be with topical miconazole 2% cream twice daily or washing with zinc or selenium shampoos daily for 2 to 2 weeks. Although not FDA approved, ketoconazole 400 mg in a single dose orally is 97% effective in adults. Have patients exercise to sweat and not shower for 2 to 4 hours.</li>
                        </ol>
                        <h4 class="phead">Pediculosis</h4>
                        <hr class="hrstyle2">
                        <p>Two species of lice affect humans: Pediculus humanus (capitis or corporis) and Phthirus pubis. Sensitization to louse saliva and antigens results in clinical manifestations.</p>
                        <ol>
                            <li> <b> Pediculosis capitis (infestation by head lice):</b> Seen primarily in preschoolers and early elementary school ages but it occurs in all ages and socioeconomic classes (however, the low incidence in blacks). Spread by direct contact or on fomites (helmets, combs, etc.). Diagnosis: pruritus; erythematous papules usually on occiput, postauricular region, and nape of the neck; lice; and nits (eggs firmly attached to hair shaft ~1 cm from the scalp. Differential diagnosis: seborrhea, psoriasis, tinea capitis, impetigo.</li>
                            <li><b> Pediculosis corporis (infestation by body lice):</b> Live in clothing or bedding and not on people. Seen primarily in lower socioeconomic class. The louse bites at night and leaves pruritic vesicles or papules (especially in axillae, groin, and truncal areas). Diagnosis by examination of clothes to find nits or lice.</li>
                            <li> <b> Pediculosis pubis (infestation by pubic lice):</b> Transmitted by intimate contact. Diagnosis: pubic or anogenital pruritus, and lice or nits found especially in the pubic hair but also in trunk, beard, eyelashes, or axillae. Often associated with additional STDs.</li>
                            <li>
                                <b> Treatment for all the above by a pediculicide:</b>
                                <ul>
                                    <li> <span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>Treat all sexual partners and household members simultaneously.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Wash all bedding, clothes, towels, and hats in hot water and a hot dryer.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> If eyelashes or eyebrows are infested, avoid a pediculicide in those areas. Instead, apply petrolatum 5 times a day until clear. Remove nits with forceps.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> Pruritus may last for several weeks after successful treatment.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> For head lice or crabs. Permethrin (1%) cream rinse is the drug of choice applied to the scalp after shampooing, left on for 10 minutes, and then rinsed off. Alternative: lindane 1% shampoo. Wash all linens in hot water. Avoid in infants, since it may be neurotoxic.</li>
                                    <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> For body lice. Use pyrethrum with piperonyl butoxide lotion over the whole body and in the bath; wash off after 10 minutes. Re-treat in 7 to 10 days.</li>
                                </ul>
                            </li>
                        </ol>
                        <h4 class="phead">Scabies</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li><b>      Overview:</b> Caused by a mite, Sarcoptes scabiei, that burrows in the skin. Most common in children but found in all ages. Usually transmitted by person-to-person transmission but may be picked up from bedding, clothes, etc.</li>
                            <li><b>      Diagnosis:</b> Characterized by linear burrows (pathognomonic), papules, or nodules with or preceded by intense pruritus, especially at night. Secondary findings include excoriations, crusting, eczematous plaques, and impetigo. In older children and adults, areas of involvement are webs of fingers, axillae, flexures of arms and wrists, beltline, and areas around the umbilicus, nipples, genitals, and lower buttocks. In infants, palms, soles, head, and neck are involved. Confirm with a scraping of the lesion with a scalpel blade placed on a slide with mineral oil and look for mites, ova, or fecal pellets.</li>
                            <li><b>      Treatment:</b> Permethrin cream 5% is the drug of choice applied head to toe at bedtime and washed off in the morning (8 to 14 hours). The alternative is lindane lotion 1% applied to cool, dry skin at bedtime, washed off in the morning, and repeated in 1 week. Neither should be used in infants <2 months of age or in pregnant or nursing women; instead, use precipitated sulfur (6%) in petroleum applied for three consecutive nights. Family members should be treated even if symptomatic. Bed linens and clothing should be washed in hot water (>120° F) or stored in tightly sealed bags for 1 week. Recently, Meinking et al. reported the use of a single dose of ivermectin 100 mg as an effective treatment for scabies, but this is not an approved indication.</li>
                        </ol>
                        <h4 class="phead">Endocrine System</h4>
                        <hr class="hrstyle2">
                        <p>The human body contains 30 amazing hormones, which regulate activities like sleep, body temperature, hunger, and managing stress in times of crisis and so on.</p>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/system.jpg')}}"><br>
                        <p>Similar to plants, certain special chemicals called hormones regulate the various activities of our body. Hormones are chemical messengers, which help in controlling and coordinating the activities of the body. They are produced by endocrine glands. The hormone, in other words, acts as a switch or a trigger for a certain action to take place.</p>
                        <br>
                        <p>The endocrine glands are under the control of the nervous system.</p>
                        <p>The endocrine system of human beings consists of the following glands:</p>
                        <ol>
                            <li>  Hypothalamus And Pituitary Gland</li>
                            <li>  The Pineal Gland</li>
                            <li>  The Thyroid Gland</li>
                            <li> The Parathyroid Glands</li>
                            <li> The Adrenal Glands</li>
                            <li> Islets Of Langerhans (Pancreas)</li>
                            <li> Ovaries (Female)</li>
                            <li> Testes (Male)</li>
                        </ol>
                        <h4 class="phead">Respiratory System</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li>  We breathe 13 pints of air every minute.</li>
                            <li>  Each lung contains 300-350 million respiratory units called alveoli making it a total of 700 million in both lungs.</li>
                            <li>  More than half a liter of water per day is lost through breathing.</li>
                            <li>  People under 30 years of age take in double the amount of oxygen in comparison to an 80 year old.</li>
                            <li>  Yawning brings more oxygen to the lungs.</li>
                        </ol>
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/respiratory.jpg')}}"><br><br>
                        <p>Human beings like other land animals breathe through their nostrils in noses and with the help of lungs. A pair of lungs are located in the airtight thoracic cavity that is bounded by a convex muscular and elastic sheet called diaphragm. Functionally, the lungs are elastic bags resembling rubber balloons. They lack any muscle, which may allow them to expand or contract by themselves. In normal breathing, through the nose, air travels through the nasal passages that are lined with ciliated mucous epithelium. Here, it is cleaned and warmed. Sensory cells detect odors. As the air continues through the pharynx or throat, it crosses the path of food. This is why we can breathe through the mouth. Then, air passes the epiglottis, enters the larynx or voice box, and goes down the trachea or windpipe. A bronchus runs to each lung, divides in a tree-like manner to give smaller bronchioles and finally deposits the air in the microscopic thin-walled air sacs or alveoli (singular alveolus). A group of alveoli appears like a cluster of grapes and gives the lungs, a sponge-like structure. There are about 150 million alveoli in each lung and altogether they cover a very large surface area (approximately 70 square meters).
                        </p>
                        <br><br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/human.jpg')}}"><br><br>
                        <p>The alveoli are lined by a layer of moist flat epithelial cells and surrounded by networks of blood capillaries. The blood, which flows to the lungs by the pulmonary artery, contains little oxygen and much carbon dioxide. On the other hand, the air in the alveoli has a high concentration of oxygen and relatively less carbon dioxide. Thus a 2-way diffusion takes place through the cells of the capillaries. Oxygen enters the blood and CO2 leaves it. Since enormous breathing surface of the lungs is exposed to the external environment the exchange of gases is computed within a few seconds. Living organisms need food.
                        </p>
                        <h4 class="phead">Digestive System</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li>  For every 2 weeks, the human stomach produces a new layer of the mucous lining, otherwise, the stomach will digest itself.</li>
                            <li>  The human liver performs 500 different functions.</li>
                            <li>  The liver is the largest and heaviest internal organ of the body and weighs about 1.6 kilos.</li>
                            <li>  The Liver is the only organ of the body, which has the capacity to regenerate itself completely even after being removed almost completely.</li>
                            <li>  Liver cells take several years to replace themselves.</li>
                            <li>  A healthy liver processes 720 liters of blood per day.</li>
                            <li>  The human stomach contains about 35 million small digestive glands.</li>
                            <li>  The human stomach produces about 2.5 liters of gastric juice every day.</li>
                            <li>  In an average person, it takes 8 seconds for food to travel down the food pipe, 3-5 hours in the small intestine and 3-4 days in the large intestine.</li>
                            <li> The human body takes 6 hours to digest a high-fat meal and takes 2 hours for a carbohydrate meal.</li>
                        </ol>
                        <h4 class="phead">Living organisms need food</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> In order to keep alive and to carry on their various life activities such as ingestion, digestion, absorption, respiration, movement, circulation, co-ordination, secretion, excretion and reproduction.</li>
                            <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>For building and maintaining their cellular and metabolic machinery (growth maintenance and repair of the organism).</li>
                            <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span>For regulating metabolic processes.</li>
                            <li><span class="arrowicon"><img src="{{ URL::asset('asset/images/logo/bullet.jpg')}}"></span> For building up the resistance against disease.</li>
                        </ol>
                        <p><b>Food thus can be defined as an essential substance that when absorbed into the body tissues yields materials for the production of energy, the growth, and regulation of life processes, without harming the organism.</b></p>
                        <ol>
                            <li>  The particles or pieces of food, small or big are taken into the body. This is called eating or ingestion.</li>
                            <li> The ingested food is then digested, where the complex and large food particles are broken down into simpler, smaller and soluble molecules.</li>
                            <li>  Then, the simpler substances obtained from digestion are then absorbed into the cells of the body.</li>
                            <li>  Then the undigested waste material is removed and thrown out of the body by excretion. The process of digestion includes a mechanical and chemical breakdown of ingested food.</li>
                        </ol>
                        <p>The chunks of food chewed by us are broken down into small pieces and are acted upon a variety of enzymes secreted into the mouth. Thus, inside the mouth, saliva moistens the masticated food and causes chemical digestion (of starch by the amylase enzymes into smaller molecules). The masticated food and partially digested food then passes the esophagus or the food pipe into the stomach. Here, it is acted upon by gastric juice of the stomach, which contains hydrochloric acid, pepsin, and other enzymes. These enzymes break down the proteins of the food into smaller molecules, which pass onto the small intestine.</p>
                        <br><br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/digestive.jpg')}}"><br><br>
                        <p>In the first part of the small intestine, called the duodenum, food (now called chyme) is acted upon the by bile juice from the liver and pancreatic juice from the pancreas. The walls of a part of small intestine called ileum also pour some enzymes for food digestion.</p>
                        <br><br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/Digestive2.jpg')}}"><br><br>
                        <p>All the food, which is digested by the mouth, stomach, duodenum, and ileum, is ultimately absorbed by the villi, which are numerous minute finger-like projections into the cavity of the small intestine. The absorbed food is then sent through the blood to different parts of the body. The absorbed food materials are utilized by the body in various ways, by a process called assimilation. The undigested food is sent to the large intestine and removed through the rectum and anus in the form of stool or feces. This process is called excretion.

</p>
                        <br>
                        <h4 class="phead">Reproductive System</h4>
                        <hr class="hrstyle2">
                        <ol>
                            <li> In the womb, the baby's body is covered by a thin layer of hair but as soon as the baby is born it disappears.</li>
                            <li> The largest cell in the female human body is the ovum or egg present in the ovaries.</li>
                            <li> About 500 million sperm mature every day in a normal male adult.</li>
                            <li> The ovaries of a newborn girl contain about 600, 000 immature eggs.</li>
                            <li> The average life span of a sperm is about 36 hours.</li>
                            <li> The life span of an ova is about 12 - 24 hours.</li>
                            <li> The female human body is capable of giving birth to 35 children in one lifetime.</li>
                        </ol>
                       <div class="table_div2 img-res">
            <p class="Paragraph ">
            </p><table class="" style="width:99%; padding: 10px;" border="1px">
                <tbody><tr>
                   <td width="50%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;<b>Male Reproductive System</b></td>
                   <td width="50%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;<b>Female Reproductive System</b></td>
                </tr>
                <tr>
                  <td width="50%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><img src="{{ URL::asset('asset/images/partofbody/male.jpg')}}" class="img-responsive"></td>
                   <td width="50%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><img src="{{ URL::asset('asset/images/partofbody/female.jpg')}}" class="img-responsive"></td>
                </tr>
            </tbody></table>
            <p></p>
      </div>



<!-- <div class="table_div2" class="table-responsive"  style="overflow-x:hidden;">
            <p class="Paragraph">
            </p><table class="table table-responsive" style="width:100%; overflow-x: auto" border="1px"  >
                <tbody><tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><b>Sr.No.</b></td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><b>Endocrine Glands</b></td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><b>Location</b></td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><b>Hormone</b></td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;"><b>Action</b></td>
                </tr>
               <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">1.</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Pituitary (Master Gland)</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Base of fore brain, pea shaped</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Growth Hormone</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Regulates the growth of bone and tissue.</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Anti-diuretic</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Controls amount of water</td>
                </tr>
               <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">hormone</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">reabsorbed by the kidney</td>
                </tr>
                  <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Adreno corticotrophic hormone</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Defending the body against physiological stress eg. exposure to cold Follicle stimulating hormone Stimulates ovary to produce female hormone</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">&nbsp;</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Thyroid stimulating hormone</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Stimulates thyroid to produce its hormone</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">2.</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Thyroid</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Neck of lower extremity of larynx, butterfly shaped</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Thyroxine</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Regulates rate of growth and metabolism. Too little over weight and sluggishness. Too much- thinning and over activity.</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">3.</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Adrenal</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">A Pair of cap shaped organs above each kidney</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Cortisone</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Aids in conversion of proteins to sugar, cortex of this gland produces the hormone.</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">4.</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Pancreas</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">It’s a double gland</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Insulin</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Regulates Sugar metabolism. Too little insulin leads to high sugar level in blood and weakness (a condition called diabetes).</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">5.</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Ovary</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Lie on the lateral walls of the pelvis</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Estrogen</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Development of Secondary sexual characters. Eg. Development of breasts in female.</td>
                </tr>
                <tr>
                   <td width="5%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">6.</td>
                   <td width="15%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Testis</td>
                   <td width="25%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">In the scrotum</td>
                   <td width="20%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Testosterone</td>
                   <td width="35%" class="Paragraph" style="padding-left: 10px; padding-right: 10px;">Development of many masculine features such as growth of moustaches and beard.</td>
                </tr>
          </tbody></table>
            <p></p>
     
      </div>
 -->








                        <p>Reproduction is one of the most important and fundamental properties of living organisms by which every kind of living organism multiplies to form new individuals of its own kind. In this process, one generation of living organisms give rise to the next generation. This process is not essential to the life of any individual but is a function essential for the life of the species. The continuity of life has been possible from the time of its origin, millions of years ago up to the present day, only due to the phenomenon of reproduction-in fact, the perpetuation of self and of the species is one of the most remarkable features of living organisms. A species is a group of related animals, plants or microbes that can interbreed to produce a fertile offspring. The preservation of species has been made possible because the parents produce offspring like themselves. It is also the means of increasing the population of a species. It plays a major in evolution by transmitting advantageous changes/variations from organisms of one generation to those of the next. In human beings, the process is one of sexual reproduction, which involves both male and female sexes. The new individual or offspring develop from the fusion cell called zygote, which is formed, by the fusion or union of two specialized cells called germ cells, sex cells or gametes. Generally, one of the gametes is active, smaller in size and without any reserve food. This is the male gamete and it is called the sperm.
                        </p>
                        <p>The male reproductive system of a man consists of a pair of testes, ducts, accessory glands, and a penis. The functions of the male reproductive system are:</p>
                        <ol>
                            <li> Production of sperms.</li>
                            <li> Transmission of sperms to the female.</li>
                        </ol>
                        <p>The female reproductive system consists of two ovaries (produce ova and female hormones), Fallopian tubes, uterus (muscular organ which supports the fetus during it’s 40 week gestation period before birth), vagina and external genitalia.</p>
                        <br>
                        <p>The functions of the female reproductive system are: -</p>
                        <ol>
                            <li>  Production of ovum eggs.</li>
                            <li>  Receiving the sperms.</li>
                            <li>  Providing a suitable environment for fusion (fertilization) of the egg and the sperm.</li>
                            <li>  Childbirth.</li>
                            <li>  Nourishment of the baby with breast milk until it can take a mixed diet.</li>
                        </ol>
                        <h4 class="phead">E.N.T System</h4>
                        <hr class="hrstyle2">
                        <br>
                        <ol>
                            <li>  Humans have the ability to distinguish 4,000 to 10, 000 different smells.</li>
                            <li>  Children have a better sense of smell than adults.</li>
                            <li>  A human ear contains about 24,000 fibers in it.</li>
                            <li>  Women have a better sense of smell than men.</li>
                            <li>  The wax present inside the ear is made up of oil and sweat.</li>
                            <li>  Human speech is produced by the interaction of 72 muscles.</li>
                            <li>  Human speech and a Bird's song have a similar characterizations</li>
                        </ol>
                        <h4 class="phead">Eyes</h4>
                        <hr class="hrstyle2">
                        <br>
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/Eye.jpg')}}"><br>
                        <ol>
                            <li>The muscles of the eye move more than 100,000 times a day.</li>
                            <li> Most people blink about 25 times per minute.</li>
                            <li> An average human eye blinks about 6,205, 000 times.</li>
                            <li> The human eyeball is 24.5 mm long.</li>
                            <li>  Every hour, the human eye can process 36,000 bits of information.</li>
                            <li>  The eyelashes shed by a human in his entire life is of 30 m length.</li>
                            <li> Blinking of one eye causes movement of 200 muscles.</li>
                            <li>There are 1,200, 000 optic fibers in the human eye.</li>
                            <li> The lens of the human eye is composed of 65 % of water and 3% of protein.</li>
                            <li> We shut our eyes for 0.3 seconds when weblink.</li>
                            <li> Colorblind people find it hard to distinguish colors like green and red.</li>
                            <li> At birth, everyone is color blind.</li>
                            <li> The human eye has 110-130 million receptors to perceive light.</li>
                            <li> The human eye contains five to seven million receptors for color perception.</li>
                            <li> The human eye cannot perceive a motionless image.</li>
                            <li>The eyes have the fastest reacting muscle in the whole body. It contracts in 1/100th of a second.</li>
                            <li> Iris is the part of the eye that determines the color of the eye.</li>
                            <li> The human eye contains structures called Rods and cones. Rods register the shapes of images and respond to low levels of light and cones respond to bright light and registers the color of images.
                            </li>
                            <li> Newborns will cry out without tears for the first three to six weeks.</li>
                        </ol>
                        <h4 class="phead">Dental</h4>
                        <hr class="hrstyle2">
                        <img class="img-res" src="{{ URL::asset('asset/images/partofbody/Dental.jpg')}}"><br>
                        <ol>
                            <li> The tooth is the only part of the human body that cannot heal and repair on its own.</li>
                            <li>  One in every 2000 babies is born with a tooth.</li>
                            <li>  Plaque begins to form every 6 hours after brushing our teeth.</li>
                        </ol>
                        <h4 class="phead">Immune System</h4>
                        <hr class="hrstyle2">
                        <p>Human diseases can be split into two categories: -</p>
                        <ol type="A">
                            <li>   <b>Those that happen before or during birth</b></li>
                            <li> <b>And, those that happen afterward.</b></li>
                        </ol>
                        <p>Damage to the body caused by injury or disease is repaired naturally by the growth and spread of new tissue to replace damaged ones. The precise way in which the replace process works varies according to the type of tissue and the size and nature of the damage. The first step in healing is the formation of a blood clot, sealing the cut and stopping the bleeding. A scar of dried blood forms on the surface of the skin and protects the new tissue forming beneath it. Fibrous tissue builds up in the wound, new cells and blood vessels grow in the gap, and a week later there is no visible indication of the injury. However this is not the case in all injuries, in more complicated cases the wound is replaced by fibrous scar tissue. Eg. Lungs, Kidneys, etc. HOW THE BODY FIGHTS Infection Viruses, bacteria, fungi, worms and other parasites, which enter the body through the skin, nose, mouth or other openings of the body, cause infectious diseases. The body has various barriers against such invasions and they are as follows:-
                        </p>
                        <ol>
                            <li>  Adenoids & Tonsils: Are made of lymphoid tissue, located at the entrances to the throat, they act as barriers to bacteria & viruses.</li>
                            <li>Lachrymal Glands: Located above the outer corner of each eye, secrete tears, which wash dust and dirt from the eyes. Tears also contain Lysozyme, a substance that combats bacteria.</li>
                            <li> Salivary Glands: Are found in the cheeks and under the tongue. The salvia they secrete contains substances, which help to resist infection.</li>
                            <li> Mucous Membrane: Is a soft moist tissue lining the body openings and passages, such as the nose & the throat. It secretes mucus, a sticky fluid, which provides protection against some bacteria. In the nasal cavity, the mucous membrane also traps germs and dust.</li>
                            <li> Lymph Nodes are small lumpy structures scattered along the course of the lymphatic system. The neck, armpits, and groins have the greatest numbers where they act as traps for bacteria & other invaders. The nodes also produce white blood cells, which attack foreign matter.</li>
                            <li>  Spleen: Which has lymphoid tissue, helps to form white blood cells and removes used ones.</li>
                            <li>  Stomach: Has digestive juices including hydrochloric acid which helps to sterilize the food intake.</li>
                            <li> Liver: Produces two substances, fibrinogen, and prothrombin, which make the blood clot and so help in wound healing. This large gland also destroys many parasites and poisons which are carried to it in the bloodstream.</li>
                            <li>  Intestines: Contains bacteria that usually remain there and are harmless. Some intestinal bacteria, such as bacillus, cause disease if they get into other areas of the body.</li>
                            <li> Lymphatic System: Is a second circulation, which is intertwined with the network of blood vessels, the lymph stream contains white blood cells, which helps to stop the spread of infection by producing antibodies.</li>
                            <li> Skin: Provides the body’s main protection against disease. Because it is dry and slightly acid, and its surface is constantly being replaced, invaders rarely colonize the skin, except in the case of injury. Beneficial bacteria live on the skin, but they can be a cause of illness if they penetrate below the surface.</li>
                        </ol>
                    </div>
                </div>
      </div>
           @endsection