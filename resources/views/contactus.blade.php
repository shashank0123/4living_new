@extends('layouts.company')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
        <div class="contact-area pt-85 pb-90">
            <div class="container">
                <div class="contact-info-wrap mb-50">
                    <h3>contact info</h3>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-contact-info text-center mb-30">
                                <i class="ti-location-pin"></i>
                                <h4>our address</h4>
                                <p>Hanuman chatti, Gaytri Nagar, Salasar(Rajasthan)<BR> pincode – 331506  </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-contact-info extra-contact-info text-center mb-30">
                                <ul>
                                    <li><i class="ti-mobile"></i> 8950603008 </li>
                                    <li><i class="ti-email"></i> <a href="mailto:info@4living.in">info@4living.in</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-contact-info text-center mb-30">
                                <i class=" ti-alarm-clock"></i>
                                <h4>openning hour</h4>
                                <p>Monday - Friday. 9:00am - 5:00pm </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="get-in-touch-wrap">
                    <h3>Get In Touch</h3>
                    <div class="contact-from contact-shadow">
                        <form id="contact-form" action="assets/mail.php" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input name="name" type="text" placeholder="Name">
                                </div>
                                <div class="col-lg-6">
                                    <input name="email" type="email" placeholder="Email">
                                </div>
                                <div class="col-lg-12">
                                    <input name="subject" type="text" placeholder="Subject">
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="message" placeholder="Your Message"></textarea>
                                </div>
                                <div class="col-lg-12">
                                    <button class="submit" type="submit">Send Message</button>
                                </div>
                            </div>
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
                <div class="contact-map pt-90">
                    <!-- <div id="map"></div> -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.3978085308586!2d77.07341131455931!3d28.617837141497837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04c8de8d7595%3A0xd70f6083653bb727!2sFriday+Market!5e0!3m2!1sen!2sin!4v1558335636767!5m2!1sen!2sin" width="100%" height="auto" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection