<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<title>Welcome To {{ config('app.name') }}</title>
<style>
.box
{
        height: 63px;
    border: 1px solid #000;
    background: black;
    color: #fff;
    line-height: 32px;
    margin-left: 15%;
    margin-right: 15%;
    padding-left: 11%;
    border-radius: 10px
}
.small-box
{
    height: 29px;
    border: 1px solid #000;
    background: black;
    color: #fff;
    line-height: 28px;
    margin-left: 42%;
    margin-right: 15%;
    padding-left: 5%;
    border-radius: 10px;
    width: 44%;
}
.snapBox
{
        height: 100px;
    border: 1px solid #000;
    width: 45%;
}
</style>
</head>

<body>
<!-- <div width="512" style="text-align:center;">
    <a href="#">Can't see our images? Click here.</a>
</div> -->
<table align="center" border="0" cellspacing="0" cellpadding="0" width="512" style="border:1px solid #e8e8e8;">
    <tbody>
        <tr><td colspan="3" height="10"></td></tr>
        <tr>
            <td width="10"></td>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" class="table">
                    <tbody>
                        <tr>
                          <td colspan="3" height="70" align="center" style="background-color:#eeeeee">
                            <!-- <a href="{{ url('/') }}"> 4living</a> -->
                            <a href="{{ url('/') }}"> <img src="{{ asset('asset/images/logo/logo-2.png') }}" width="100" height="65"></a>
                          </td>
                        </tr>
                        <tr><td colspan="3" height="10"></td></tr>
                        <tr>
                            <td width="10"></td>
                            <td>
                                <h5>New franchisee form submitted.</h5>
                                
                                <br>
                            </td>
                            <td width="10"></td>
                        </tr>
                        <tr>
                            <td width="10"></td>
                            <td>
                                <br>
                            </td>
                            <td width="10"></td>
                        </tr>
                        
                              @if(!empty($info->form))
                              <tr>
                            <td width="10"></td>
                            <td>
                                <p>Registration Form</p>
                                <a href='{{ url($info->form) }}' download><u>Click Here</u></a> to download form.
                                
                                
                                 </td>
                            <td width="10"></td>
                        </tr>
                              @endif
                              
                              
                              
                              
                               @if(!empty($info->profile_pic))
                              <tr>
                            <td width="10"></td>
                            <td>
                                <p>Profile Photo</p>
                                <img src='{{ asset($info->profile_pic) }}' style="height: 75px;">
                                
                                
                                 </td>
                            <td width="10"></td>
                        </tr>
                              @endif
                              
                              
                              
                               @if(!empty($info->pan_card))
                              <tr>
                            <td width="10"></td>
                            <td>
                                <p>Pan Card </p>
                                <img src='{{ asset($info->pan_card) }}' style="height: 75px;">
                                
                                
                                 </td>
                            <td width="10"></td>
                        </tr>
                              @endif
                              
                              
                              
                              
                               @if(!empty($info->adhaar_front))
                              <tr>
                            <td width="10"></td>
                            <td>
                                <p>Adhaar Card (Front)</p>
                                <img src='{{ asset($info->adhaar_front) }}' style="height: 75px;">
                                
                                
                                 </td>
                            <td width="10"></td>
                        </tr>
                              @endif
                              
                              
                              
                               @if(!empty($info->adhaar_back))
                              <tr>
                            <td width="10"></td>
                            <td>
                                <p>Adhaar Card (Back)</p>
                                <img src='{{ asset($info->adhaar_back) }}' style="height: 75px;">
                                
                                
                                 </td>
                            <td width="10"></td>
                        </tr>
                              @endif
                              
                              
                              
                              
                              
                              
                              
                              
                           
                        <tr><td colspan="3" height="10"></td></tr>
                        <tr><td colspan="3" height="30" style="background-color:#e06f47;"></td></tr>
                    </tbody>
                </table>
            </td>
            <td width="10"></td>
        </tr>
        <tr><td colspan="3" height="10"></td></tr>
    </tbody>
</table>
</body>
</html>
