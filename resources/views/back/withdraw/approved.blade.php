<?php
use App\Models\Member;
?>



@extends('back.app')

@section('title')
Add WD Statement | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">All Statements</li>
</ul>
@stop
<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }

  #myImg:hover {opacity: 0.1;}

  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }

  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }

  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }

  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }

  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }

  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }

  /* The Close Button */
  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }

  #show-message{ display: none; }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }

</style>

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      <section class="tables-data" style="padding:10px 50px">
        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">              
              <h1><i class="md md-group-add"></i>Funds Request Statements</h1>         
              <p>Funds Request Statements</p>         
            </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/admin/funds-statement/funds">                  
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 50%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
              <a href="/admin/funds-statement/all" class="btn btn-primary">Pending Statements</a>
              
            </div>
          </div>
        </div>        
      </section>

      <section class="tables-data" style="padding: 5px 40px">
        
        <p class="alert-success" id="show-message" style="padding: 20px"></p>
        <div class="card">
          <div>
            <div  id="table-response">
              <table class="table table-full ">
                <thead>
                  <tr>
                    <th>User Name</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>UTR Photo</th>
                    <th>Status</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php $count=0; ?>
                  @if(!empty($uploads))
                  @foreach($uploads as $upload)
                  <?php $count++; ?>
                  <?php
                  $user = Member::where('id',$upload->member_id)->first();
                  ?>
                  @if($upload->status == "Approved")
                  <tr id="approved{{$upload->id}}">
                    <td>{{$user->username}}</td>
                    <td>Rs. {{$upload->amount}}</td>
                    <td>{{$upload->date}}</td>
                    <td><img id="myImg{{$upload->id}}" src="{{url('/assetsss/proof')}}/{{$upload->utr_photo}}" alt="Utr Photo" style="width: 100px;height: 100px"></td>
                    <td>
                      <select class="form-control" name="status" id="status{{$upload->id}}" disabled>
                        <option value="Pending" <?php if($upload->status == "Pending"){echo 'selected';} ?>>Pending</option>
                        <option value="Approved" <?php if($upload->status == "Approved"){echo 'selected';} ?>>Approved</option>
                      </select>                        
                      
                    </tr>
                    @endif
                    @endforeach
                    @endif
                  </tbody>
                </table>
                @if($count == 0)
              <div style="text-align: center; padding:10% ">
                <h3>No Result Found</h3>
              </div>
              @endif
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

@if($count>0)
  <!-- The Modal -->
  <div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
  </div>

  @endif

  <script>
// Get the modal
@if($count>0)
@foreach($uploads as $upload)
var id=<?php echo $upload->id;?>;
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg"+id);
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
@endforeach
@endif


function updateStatus(id){

  var status = $('#status'+id).val();
  // alert(id+" / "+status);

  $.ajax({
    type: "POST",
    url: "/admin/update-statement-status",
    data:{ id: id, status : status },
    success:function(msg){

      // alert('Done');
      if(msg.status == 'Approved'){
        $('#approved'+id).hide();
      $('#show-message').show();
      $('#show-message').html(msg.message);
      
      }
      // if(msg.message == 'Username already exists. Try another')
      //   $('#check-availability').html(msg.message);
    },

    failure:function(msg) {
      alert('Fail');
    }
  });

}
</script>
@stop
