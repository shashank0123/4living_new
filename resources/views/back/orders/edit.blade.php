
@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

@include('back.include.header')
@include('back.include.sidebar')

<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
		<a href="{{ url('admin/franchise/products') }}"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
		<div style="width: 60%; text-align: center !important;" >
			<h3 style="margin-left: 20%">Order Detail</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			




	<div class="row">
		<div class="col-sm-12">
			<h4> <b>Order ID :</b> {{ $order->id ?? '' }} &nbsp;&nbsp;&nbsp;&nbsp; <b>Booking Date :</b> {{ $order->booking_date ?? '' }} &nbsp;&nbsp;&nbsp;&nbsp; <b>Total amount :</b> Rs. {{ $order->total ?? '' }}</h4>
			<br><br>

			<div class="col-sm-12">

			<div class="row">
				<div class="col-sm-4">
					<h5>Billing Details</h5>
					<p><b>Name : </b>{{ $billing->name ?? '' }}</p>
					<p><b>Email : </b>{{ $billing->email ?? '' }}</p>
					<p><b>Contact No. : </b>{{ $billing->phone ?? '' }}</p>
					<p><b>Address : </b>{{ $billing->address ?? '' }} , {{$billing->city ?? ''}} , {{$billing->state}} - ( {{$billing->pin_code ?? ''}} )</p>
				</div>
				<div class="col-sm-4">
					<h5>Shipping Details</h5>
					<p><b>Name : </b>{{ $shipping->name ?? '' }}</p>
					<p><b>Email : </b>{{ $shipping->email ?? '' }}</p>
					<p><b>Contact No. : </b>{{ $shipping->phone ?? '' }}</p>
					<p><b>Address : </b>{{ $shipping->address ?? '' }} , {{$shipping->city ?? ''}} , {{$shipping->state}} - ( {{$shipping->pin_code ?? ''}} )</p>
				</div>
				<div class="col-sm-4">
					<form action="{{ url('admin/order_status/'.$order->id) }}" method="POST">
					<label>Order Status</label>
					<select name="order_status" id="order_status" class="form-control">
						<option value="Pending" @if($order->order_status == 'Pending'){{'selected'}}@endif>Pending</option>
						<option value="Completed" @if($order->order_status == 'Completed'){{'selected'}}@endif>Completed</option>
						<option value="Cancelled" @if($order->order_status == 'Cancelled'){{'selected'}}@endif>Cancelled</option>
					</select>
					<br>
					<button type="submit" name="submit" class="btn btn-primary">Update Status</button>
				</form>
				<br><br>
				<?php
				$color_code = 'yellow';
					if($order->order_status == 'Pending')
					$color_code = 'yellow';
					if($order->order_status == 'Completed')
					$color_code = 'green';

					if($order->order_status == 'Cancelled')
					$color_code = 'red' ;
				
				  ?>
				<h5>Status :
					<span style="background-color: {{ $color_code  }} ; padding: 5px 10px;">

					{{ $order->order_status ?? '' }}</h5>
				</div>
			</div>
			</div>
			<br><br>
		</div>
		</div>
		<div class="row">
			<br><br>
			
			<table class="table">
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>Quantity</th>
					<th>Sub Total</th>
				</thead>
				<tbody>
					@foreach($products as $product)
<tr>
					<td>
						<img src="{{ asset($product->image ?? '') }}" style="height: 75px;">
					</td>
					<td><a href="{{ url($product->path ?? '') }}">{{ $product->product_name ?? ''}}</a></td>
					<td>{{ $product->quantity ?? ''}}</td>
					<td>Rs. {{ $product->unit_price*$product->quantity }}</td>
				</tr>

					@endforeach
				</tbody>
			</table>


		</div>




			</div>
		</div>

	<!-- END Page Content -->
	@endsection

