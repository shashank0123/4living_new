@extends('back.app')

@section('title')


@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      @if($errors->any())
      <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </div>
      @endif

      @if($message = Session::get('message'))
      <div class="alert alert-primary">
        <p>{{ $message }}</p>
      </div>
      @endif
      
      <section class="tables-data" style="padding: 30px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i>Orders List</h1>
        </div>
        
       
        <div style="overflow: auto;">
         <table  class="table table-full-small " style="text-align: center;">
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Booking On</th>
              <th>Customer Name</th>
              <th>Contact No.</th>
              <th>Amount</th>
              
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            @if(!empty($orders))
            
            @foreach($orders as $order)
            <tr>
             <td>{{ $order->id ?? '' }}</td>
             <td>{{ date('d M, Y') , strtotime($order->booking_date ?? '') }}</td>
             <td>{{ $order->name ?? '' }}</td>
             <td>{{ $order->phone ?? '' }}</td>
             <td>{{ $order->total ?? '' }}</td>
             
            <td>
              <a class="btn btn-primary" href="{{ url('/admin/order_detail', $order->id) }}"><i class="fa fa-edit"></i></a>
              <!-- <a class="btn btn-danger" href="{{ url('/admin/franchise/delete_product', $order->id) }}" onclick="return myFunction();"><i class="fa fa-trash"></i></a> -->
            </td>
          </tr>
          @endforeach
          @endif
          
          
        </tbody>
        
      </table>
    </div>
  </div>
  
</section>

</div>
</div>
</main>
<script>
  function myFunction() {
    if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
</script>

@stop
