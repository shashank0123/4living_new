<?php
use App\Models\Package;
use App\Models\MemberDetail;
use App\Invoice;
use App\Models\Payment;
$package = Package::where('id',$user->package_id)->first(); 




$invoice = Invoice::where('member_id',$user->id)->where('package_amount',$package->package_amount)->first();

?>
@extends('back.app')

@section('title')
Create Announcement | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="#">Front Page</a></li>
	<li><a href="{{ route('admin.home') }}">Dashboard</a></li>
	<li><a href="{{ route('admin.announcement.list') }}">Announcement List</a></li>
	<li class="active">Create Announcement</li>
</ul>
@stop
<style>
	.invoice { border: 1px solid #000 ; width: 98%; background-color: #eee;    }
	.head,.invoice-detail,.invoice-head,.receiver-head, .consignee-head { border-bottom: 1px solid #000;  }
	.address { text-align: right; }
	.address p { line-height: 1.35;color: #000; margin-top: 10px; margin-bottom: 0 }
</style>

@section('content')
{{-- <main> --}}
	@include('back.include.header')
	@include('back.include.sidebar')
	<br>
	<button onclick="window.print(); return false;" >Print Invoice</button>
	<br><br>
	<div class="container invoice" style="background: bisque;">

		{{-- Header --}}
		<div class="head">
			<div class="row">
				<div class="col-sm-6">
					<div class="logo" style="
					background-color: bisque;    padding: 1rem;
					">
					<img src="{{ asset('/images/h2hlogo.gif') }}" width="180" height="auto">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="address">
					<!--<p>Refurbind Tech Private Limited</p>-->

					<p >Rz-10 , Mahendra Park ,<br>
						Pankha Road , Uttam Nagar ,<br>
						Near Billu Material Supply ,<br> New Delhi , West Delhi ,<br> India - 110059</p>
						<p>GSTIN : 07AAECH9689K1ZR</p>

					</div>
				</div>
			</div>		
		</div>

		{{-- Invoice Number --}}
		<div class="invoice-head">
			<div class="row">
				<div class="col-sm-6">
					<div class="invoice-data">
						<div class="row">
							<div class="col-sm-6 col-xs-6">Invoice Number</div>
							<div class="col-sm-6 col-xs-6">: @if(!empty($invoice))HH1{{sprintf("%05d", $invoice->invoice_number)}} @endif</div>
							<div class="col-sm-6 col-xs-6">Invoice Date</div>
							<div class="col-sm-6 col-xs-6">: @if(!empty($invoice)){{$invoice->created_at}} @endif</div>
						
						<div class="col-md-6 col-sm-6 col-xs-6">PAN</div>
						<div class="col-md-6 col-sm-6 col-xs-6">: AAECH9689K</div>
						{{-- <div class="col-md-1 col-sm-1 col-xs-6">CIN</div>
						<div class="col-md-5 col-sm-5 col-xs-6">:  </div> --}}				
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="service-date">
					<div class="row">
						{{-- <div class="col-sm-6 col-xs-6">Service</div>
						<div class="col-sm-6 col-xs-6">: </div> --}}
						<div class="col-sm-6 col-xs-6">Order ID</div>
						<div class="col-sm-6 col-xs-6">: </div>
						<div class="col-sm-6 col-xs-6">Date & Time of Service</div>
						<div class="col-sm-6 col-xs-6">: @if(!empty($invoice)) {{$invoice->created_at}} @endif</div>
						{{-- <div class="col-sm-6 col-xs-6">Place of Supply</div>
						<div class="col-sm-6 col-xs-6">: </div> --}}
					</div>				
				</div>
			</div>
		</div>		
	</div>

	{{-- Invoice detail --}}
	<div class="invoice-detail">
		<div class="row">
			<div class="col-sm-6">
				<div class="receiver-head">Details of Receiver (Billed To)</div>
				<div class="receiver-desc">
					<div class="row">
						<div class="col-sm-4 col-xs-6">Name </div>
						<div class="col-sm-8 col-xs-6">: {{strtoupper($user->username)}}</div>
						<div class="col-sm-4 col-xs-6">Mobile</div>
						<div class="col-sm-8 col-xs-6">: 
							<?php
							$detail = MemberDetail::where('member_id',$user->id)->first();
							?>
							@if(!empty($detail->mobile_phone))
							{{$detail->mobile_phone}}
							@endif
						</div>
						<div class="col-sm-4 col-xs-6">Address</div>
						<div class="col-sm-8 col-xs-6">: {{$detail->address}}</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="consignee-head">Details of Consignee (Shipped To)</div>
				<div class="consignee-desc">
					<div class="row">
					<div class="col-sm-6 col-xs-6">Name </div>
					<div class="col-sm-6 col-xs-6">: {{strtoupper($user->username)}}</div>
					<div class="col-sm-6 col-xs-6">Address</div>
					<div class="col-sm-6 col-xs-6">: @if(!empty($detail->address))
						{{$detail->address}}
					@endif</div>
					<div class="col-sm-6 col-xs-6">GSTIN </div>
					<div class="col-sm-6 col-xs-6">: 07AAECH9689K1ZR</div>
				</div>
				</div>
			</div>
		</div>		
	</div>

	{{-- Invoice Table --}}
	<div class="invoice-table">
		<table style="width: 100%;">
			<thead>
				<tr>
					<th>Package</th>				
					<th>Date</th>				
					<th>Total</th>				
				</tr>
			</thead>

			<tbody>
				<td>
					Rs. {{$package->package_amount-($package->package_amount*5)/100}}
				</td>
				<td>
					Rs. {{($package->package_amount*5)/100}}
				</td>
				<td>
					{{$user->created_at}}
				</td>
				<td>					
					Rs. <?php echo sprintf('%0.2f', $package->package_amount); ?>
				</td>					
			</tbody>
		</table>	
	</div>
</div>


</div>



@endsection