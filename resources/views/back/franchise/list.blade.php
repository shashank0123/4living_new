<?php 
use App\Models\FranchiseDetail;
use App\Models\Franchise;
use App\User;

?>


@extends('back.app')

@section('title')


@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      @if($errors->any())
      <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </div>
      @endif

      @if($message = Session::get('message'))
      <div class="alert alert-primary">
        <p>{{ $message }}</p>
      </div>
      @endif
      
      <section class="tables-data" style="padding: 30px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Franchise List</h1>
        </div>
        
        <div style="text-align: right;">
          <a href="add-franchise"><button type="submit" class="btn btn-alt-primary">Add Franchise</button> </a>

        </div>
        
        <div style="overflow: auto;">
         <table  class="table table-full-small " style="text-align: center;">
          <thead>
            <tr>
              <th>Joined On</th>
              <th>Name</th>
              <th>Franchisee ID</th>
              <th>Email</th>
              <th>Phone</th>
              <!--<th>Sponsor ID</th> -->
              <!--<th>Franchise Code</th>  -->
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
            @foreach($franchise as $value)
            <tr>
             <?php $datime = date('d M, Y H:i:s', strtotime($value->created_at));
             $user = User::where('id',$value->user_id)->first();?>
             <td>{{$datime}}</td>
             <td>{{$value->name}}</td>
             <td>{{$user->username}}</td>
             <td>{{$value->email}}</td>
             <td>{{$value->phone}}</td>
             <td>{{$value->franchise_code}}</td>
             <td>
              <a href="/admin/franchise-bank-details/{{$value->id}}" class="btn btn-primary" style="margin-top: 5px">Bank Detail</a>
              <a href="/admin/franchise-password/{{$value->id}}" class="btn btn-primary" style="margin-top: 5px">Password</a>
            </td>
            
            <td>
              <a class="btn btn-primary" href="{{ url('/admin/edit-franchise', $value->id) }}"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger" href="{{ url('/admin/franchise-list', $value->id) }}" onclick="return myFunction();"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          @endforeach
          
          
        </tbody>
        
      </table>
    </div>
  </div>
  {{ $franchise->links("pagination::bootstrap-4") }}
</section>

</div>
</div>
</main>
<script>
  function myFunction() {
    if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
</script>

@stop
