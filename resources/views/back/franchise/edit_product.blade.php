<?php
use App\Models\Package;

$packages = Package::where('package_amount','>',10000)->get();

?>

@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

@include('back.include.header')
@include('back.include.sidebar')


<style type="text/css">
	.package_list li h3{
		font-weight: bold;
		padding: 1%;
		margin-top: 15px;
		font-size: 20px;

	}

	.member_text{
		/*margin: 1% ;*/
		width: 100%;
		padding: 10px;
		background-color: green;
		color: #fff;
	}

	.package_list{
		list-style-type: none;

	}

	.package_list li{
		background-color: #fff;
		height: 200px;
		float: left;
		padding: 7px;
		margin: 10px;
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		text-align: center;
	}

	.package_list li:hover{
		position: relative;
		zoom: 1.02;
	}

	input[type="radio"]{
		width: auto;
		height: auto;
	}

	.select_package{
		margin-top: 35px;
		text-align: center;
		padding: 10px;
		margin-left: 20px;
		margin-right: 20px;
		background-color: green;
		color: #fff;
		width: fit-content;
	}
</style>


<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
		<a href="{{ url('admin/franchise/products') }}"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
		<div style="width: 60%; text-align: center !important;" >
			<h3 style="margin-left: 20%">Add Product</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
			@csrf

				{{-- Row 1 --}}
				<div class="container">

					@if(!empty($packages))
					<div class="form-group">
						<div class="row">
							<ul class="package_list">
								@foreach($packages as $package)
								<li>
									<div class="member_text"> Franchise Plan </div>
									<h3> Rs. {{number_format((float)$package->package_amount, 0, '.', '')}} /- </h3>
									<div class="select_package">
										<input type="radio" name="package_id" id="package_id" value="{{$package->id ?? ''}}" required="required" @if($package->id == $product->category_id){{'checked'}}@endif>
										&nbsp;&nbsp;
										<span>Select Now</span></div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif

				</div>
				<div class="form-group row">
					<div class="col-md-6">
						
						<label for="keyword">Product Name <font color="red">*</font></label>
						<input type="text" class="form-control" id="name" name="name"  maxlength="50" value="{{ $product->name ?? '' }}" >
						
					</div>

					<div class="col-md-6">
						
						<label for="status">Image <font color="red">*</font></label>							
						<input type="file" class="form-control" id="image1" name="image1"  maxlength="200" >
						
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-12">
						
						<label for="status">Description</label>							
						<textarea type="text" class="form-control" id="long_descriptions" name="long_descriptions" maxlength="10000" rows="6">{{ $product->long_descriptions ?? '' }}</textarea>
						
					</div>

					
				</div>
				
				











				{{-- </div>
					<div class="form-group row">
						<div class="col-md-9">
							<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
							<button type="submit" class="btn btn-alt-primary">Submit</button>
						{{-- </div> --}}
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- END Page Content -->
	@endsection

