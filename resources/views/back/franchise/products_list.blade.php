<?php 
use App\Models\FranchiseDetail;
use App\Models\Franchise;
use App\User;

?>


@extends('back.app')

@section('title')


@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      @if($errors->any())
      <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </div>
      @endif

      @if($message = Session::get('message'))
      <div class="alert alert-primary">
        <p>{{ $message }}</p>
      </div>
      @endif
      
      <section class="tables-data" style="padding: 30px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Products Under Franchise's Plans</h1>
        </div>
        
        <div style="text-align: right;">
          <a href="{{ url('admin/franchise/add_product') }}"><button type="submit" class="btn btn-alt-primary">Add New Product</button> </a>

        </div>
        
        <div style="overflow: auto;">
         <table  class="table table-full-small " style="text-align: center;">
          <thead>
            <tr>
              <th>Created On</th>
              <th>Plan</th>
              <th>Name</th>
              <th>Image</th>
              
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            @if(!empty($products))
            
            @foreach($products as $product)
            <tr>
             <?php $datime = date('d M, Y H:i:s', strtotime($product->created_at));
             ?>
             <td>{{$datime}}</td>
             <td>Rs. {{number_format((float)$product->package_amount, 0, '.', '')}}</td>
             <td>{{$product->name}}</td>
             <td><img src="{{ asset('images/products/'.$product->image1 ??'') }}" style="height: 75px;"></td>
             
            <td>
              <a class="btn btn-primary" href="{{ url('/admin/franchise/edit_product', $product->id) }}"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger" href="{{ url('/admin/franchise/delete_product', $product->id) }}" onclick="return myFunction();"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          @endforeach
          @endif
          
          
        </tbody>
        
      </table>
    </div>
  </div>
  
</section>

</div>
</div>
</main>
<script>
  function myFunction() {
    if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
</script>

@stop
