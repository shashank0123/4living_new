@extends('back.app')

@section('content')

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
	<a href="/admin/franchise-list"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
	<div style="width: 60%; text-align: center !important;" >
	<h3 style="margin-left: 20%">Add Franchise</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			<form action="/admin/edit-franchise/{{$franchisedit->id}}" method="POST" >
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">
						
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="keyword">Name <font color="red">*</font></label>
							<input type="text" class="form-control" id="name" name="name"  maxlength="50" value="{{ $franchisedit->name }}">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Address 1 <font color="red">*</font></label>							
							<input type="text" class="form-control" id="address1" name="address1"  maxlength="200" value="{{ $franchisedit->address1 }}" >
						
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">Address 2</label>							
							<input type="text" class="form-control" id="address2" name="address2" maxlength="200" value="{{ $franchisedit->address1 }}">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">City <font color="red">*</font></label>							
							<input type="text" class="form-control" id="city" name="city"  maxlength="100" value="{{ $franchisedit->city }}">
						
					</div>
				</div>
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">state <font color="red">*</font></label>							
							<input type="text" class="form-control" id="state" name="state"  maxlength="200" value="{{ $franchisedit->state }}">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Pin Code <font color="red">*</font></label>							
							<input type="text" class="form-control" id="pincode" name="pincode"  maxlength="10" value="{{ $franchisedit->pincode }}">
						
					</div>
				</div>
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">Phone Number <font color="red">*</font></label>							
							<input type="text" class="form-control"  id="phone" name="phone"  maxlength="15" value="{{ $franchisedit->phone }}">
						
					</div>

					<div class="col-md-6">
						
							<label for="alternet_contact">Alternet Contact Number </label>							
							<input type="text" class="form-control" id="alternet_contact" name="alternet_contact" maxlength="15" value="{{ $franchisedit->alternet_contact }}">
						
					</div>
				</div>
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="username">Username <font color="red">*</font></label>							
							<input type="text" class="form-control" id="username" name="username"  maxlength="20" value="{{ $franchisedit->username }}" readonly>
						
					</div>

					<div class="col-md-6">
						
							<label for="email">Email Address <font color="red">*</font> </label>							
							<input type="text" class="form-control" id="email" name="email"  maxlength="50" value="{{ $franchisedit->email }}" readonly>
						
					</div>
				</div>
				
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="aadhar_number">Aadhar_number </label>							
							<input type="text" class="form-control" id="aadhar_number" name="aadhar_number" maxlength="20" value="{{ $franchisedit->aadhar_number }}">
						
					</div>

					
				</div>
				
				
				
				


				



	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

