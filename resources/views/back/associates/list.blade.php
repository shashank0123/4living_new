@extends('back.app')

@section('title')
  All Associates | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="active">Associates List</li>
  </ul>
@stop

@section('content')
  {{-- <main> --}}
      @include('back.include.header')
    @include('back.include.sidebar')
    {{-- <div class="main-container"> --}}
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="width: 96%; margin: 1% 2%">
        <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-new-releases"></i> Associates List</h1>
        </div>

        <div class="card">
          <div class="card-content">
            <div class="datatables">
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" >
                <thead>
                  <tr>
                    <th data-id="created_at">Created Date</th>
                    <th data-id="title_en">Company name</th>
                    <th data-id="title_en">Image</th>
                    <th data-id="title_en">Status</th>
                    <th data-id="action">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!empty($associates))
                  @foreach($associates as $associate)

                  <tr>
                    <td>{{ $associate->created_at ?? '' }}</td>
                    <td>{{ $associate->company_name ?? '' }}</td>
                    <td><img src="{{ asset('images/partner/'.$associate->image ?? 'default.jpg') }}" style="height: 50px;
                    "></td>
                    <td>{{ $associate->status ?? '' }}</td>
                    <td>
                      <a href="{{url('admin/edit_associate/'.$associate->id)}}" class="btn btn-primary" id="edit">Edit</a>
                      <a href="{{url('admin/delete_associate/'.$associate->id)}}" class="btn btn-danger" id="edit">Delete</a>
                    </td>
                  </tr>

                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      </div>
   
@stop
