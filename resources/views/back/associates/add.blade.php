
@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

@include('back.include.header')
@include('back.include.sidebar')





<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
	<a href="{{ url('admin/associates_list') }}"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
	<div style="width: 60%; text-align: center !important;" >
	<h3 style="margin-left: 20%">Edit associate</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			<form action="{{ url('admin/add_associate') }}" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">

					
						
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="keyword">Company Name <font color="red">*</font></label>
							<input type="text" class="form-control" id="company_name" name="company_name"  maxlength="50" >
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Image <font color="red">*</font></label>							
							<input type="file" class="form-control" id="image" name="image"  maxlength="200" >
						
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">Address</label>							
							<input type="text" class="form-control" id="address" name="address" maxlength="200">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">City <font color="red">*</font></label>							
							<input type="text" class="form-control" id="city" name="city"  maxlength="100">
						
					</div>
				</div>
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">State <font color="red">*</font></label>							
							<input type="text" class="form-control" id="state" name="state"  maxlength="200">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Pin Code <font color="red">*</font></label>							
							<input type="text" class="form-control" id="pincode" name="pincode"  maxlength="10">
						
					</div>
				</div>
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">Website Link <font color="red">*</font></label>							
							<input type="text" class="form-control"  id="website_link" name="website_link"  maxlength="15">
						
					</div>

					<div class="col-md-6">
						
							<label for="alternet_contact">Company Email</label>							
							<input type="email" class="form-control" id="company_email" name="company_email" maxlength="15">
						
					</div>
				</div>
				
				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="username">Display Priority <font color="red">*</font></label>							
							<input type="number" class="form-control" id="display_priority" name="display_priority"  maxlength="20">
						
					</div>

					<div class="col-md-6">
						

							<label for="username">Status <font color="red">*</font></label>			

							<select name="status" id="status" class="form-control"		>
								<option value="Active">Active</option>
								<option value="Inactive">Inactive</option>
							</select>		

					</div>
					
				</div>
				
				
				
				
				


				



	                    
	                    
                                            	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

