@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member Detail</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
      #detailed .row { padding: 10px 20px !important; font-size: 16px !important; }
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
     @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif

      <section style="padding:  1% 5%">
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.title1')</h1>
            <p class="lead">@lang('settings.subTitle1')</p>
          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well white">
                <form method="POST" action="/admin/personal-info/{{$member->id}}">
                  <fieldset>
                      
                      <div class="container" id="detailed" >
                         
                              <div class="row">
                                  <div class="col-sm-6">Member ID</div>
                                  <div class="col-sm-6">: {{$member->username}}</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Name</div>
                                  <div class="col-sm-6">: {{ucfirst($member->name)}}</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Email</div>
                                  <div class="col-sm-6">: @if(!empty($user)){{$user->email}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Mobile</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail)){{$member_detail->mobile_phone}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Sponsor By</div>
                                  <div class="col-sm-6">: {{$member->register_by}}</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Package</div>
                                  <div class="col-sm-6">: @if($member->package_id == 2){{'2100'}}@else{{'0'}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">DOB</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->date_of_birth)){{$member_detail->date_of_birth}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Address</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->address)){{$member_detail->address}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">State</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->state)){{$member_detail->state}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">District</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->district)){{$member_detail->district}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">City</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->city)){{$member_detail->city}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">PIN Code</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->pin_code)){{$member_detail->pin_code}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Adhaar</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->adhaar)){{$member_detail->adhaar}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">PAN </div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->pan)){{$member_detail->pan}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Beneficiary Name</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->beneficiary_name)){{$member_detail->beneficiary_name}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Relation with Beneficiary</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->relation_with_beneficiary)){{$member_detail->relation_with_beneficiary}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Bank Name</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->bank_name)){{$member_detail->bank_name}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Account Number</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->bank_account_number)){{$member_detail->bank_account_number}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Account Holder</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->bank_account_holder)){{$member_detail->bank_account_holder}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">Branch</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->bank_branch)){{$member_detail->bank_branch}}@endif</div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">IFSC</div>
                                  <div class="col-sm-6">: @if(!empty($member_detail->ifsc)){{$member_detail->ifsc}}@endif</div>
                              </div>
                              
                          </div>
                     
                   

                    @if(!empty($member_detail))
                    @if(!empty($state))
                   <div class="form-group">
                      <label class="control-label" for="inputState">State</label>
                      <select name="state" class="form-control" id="inputState" required="">
                        <option value="">Select State</option>
                        <option @if ($member_detail->state=='AP') {{ 'selected' }} @endif value="AP">Andhra Pradesh</option>
                        <option @if ($member_detail->state=='AR') {{ 'selected' }} @endif value="AR">Arunachal Pradesh</option>
                        <option @if ($member_detail->state=='AS') {{ 'selected' }} @endif value="AS">Assam</option>
                        <option @if ($member_detail->state=='BH') {{ 'selected' }} @endif value="BH">Bihar</option>
                        <option @if ($member_detail->state=='CT') {{ 'selected' }} @endif value="CT">Chhattisgarh</option>
                        <option @if ($member_detail->state=='DL') {{ 'selected' }} @endif value="DL">Delhi</option>
                        <option @if ($member_detail->state=='GA') {{ 'selected' }} @endif value="GA">Goa</option>
                        <option @if ($member_detail->state=='GJ') {{ 'selected' }} @endif value="GJ">Gujarat</option>
                        <option @if ($member_detail->state=='HR') {{ 'selected' }} @endif value="HR">Haryana</option>
                        <option @if ($member_detail->state=='HP') {{ 'selected' }} @endif value="HP">Himachal Pradesh</option>
                        <option @if ($member_detail->state=='JK') {{ 'selected' }} @endif value="JK">Jammu and Kashmir</option>
                        <option @if ($member_detail->state=='JH') {{ 'selected' }} @endif value="JH">Jharkhand</option>
                        <option @if ($member_detail->state=='KA') {{ 'selected' }} @endif value="KA">Karnataka</option>
                        <option @if ($member_detail->state=='KL') {{ 'selected' }} @endif value="KL">Kerala</option>
                        <option @if ($member_detail->state=='MP') {{ 'selected' }} @endif value="MP">Madhya Pradesh</option>
                        <option @if ($member_detail->state=='MH') {{ 'selected' }} @endif value="MH">Maharashtra</option>
                        <option @if ($member_detail->state=='MN') {{ 'selected' }} @endif value="MN">Manipur</option>
                        <option @if ($member_detail->state=='ML') {{ 'selected' }} @endif value="ML">Meghalaya</option>
                        <option @if ($member_detail->state=='MZ') {{ 'selected' }} @endif value="MZ">Mizoram</option>
                        <option @if ($member_detail->state=='NL') {{ 'selected' }} @endif value="NL">Nagaland</option>
                        <option @if ($member_detail->state=='OR') {{ 'selected' }} @endif value="OR">Orissa</option>
                        <option @if ($member_detail->state=='Pondicherry') {{ 'selected' }} @endif value="Pondicherry">Pondicherry</option>
                        <option @if ($member_detail->state=='PB') {{ 'selected' }} @endif value="PB">Punjab</option>
                        <option @if ($member_detail->state=='RJ') {{ 'selected' }} @endif value="RJ">Rajasthan</option>
                        <option @if ($member_detail->state=='SK') {{ 'selected' }} @endif value="SK">Sikkim</option>
                        <option @if ($member_detail->state=='TN') {{ 'selected' }} @endif value="TN">Tamil Nadu</option>
                        <option @if ($member_detail->state=='TR') {{ 'selected' }} @endif value="TR">Tripura</option>
                        <option @if ($member_detail->state=='UK') {{ 'selected' }} @endif value="UK">Uttaranchal</option>
                        <option @if ($member_detail->state=='UP') {{ 'selected' }} @endif value="UP">Uttar Pradesh</option>
                        <option @if ($member_detail->state=='WB') {{ 'selected' }} @endif value="WB">West Bengal</option>
                      </select>
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputState">State</label>
                      <select name="state" class="form-control" id="inputState" required="" value="">
                        <option  value="">Select State</option>
                        <option  value="AP">Andhra Pradesh</option>
                        <option  value="AR">Arunachal Pradesh</option>
                        <option  value="AS">Assam</option>
                        <option  value="BH">Bihar</option>
                        <option  value="CT">Chhattisgarh</option>
                        <option  value="DL">Delhi</option>
                        <option  value="GA">Goa</option>
                        <option  value="GJ">Gujarat</option>
                        <option  value="HR">Haryana</option>
                        <option  value="HP">Himachal Pradesh</option>
                        <option  value="JK">Jammu and Kashmir</option>
                        <option  value="JH">Jharkhand</option>
                        <option  value="KA">Karnataka</option>
                        <option  value="KL">Kerala</option>
                        <option  value="MP">Madhya Pradesh</option>
                        <option  value="MH">Maharashtra</option>
                        <option  value="MN">Manipur</option>
                        <option  value="ML">Meghalaya</option>
                        <option  value="MZ">Mizoram</option>
                        <option  value="NL">Nagaland</option>
                        <option  value="OR">Orissa</option>
                        <option  value="Pondicherry">Pondicherry</option>
                        <option  value="PB">Punjab</option>
                        <option  value="RJ">Rajasthan</option>
                        <option  value="SK">Sikkim</option>
                        <option  value="TN">Tamil Nadu</option>
                        <option  value="TR">Tripura</option>
                        <option  value="UK">Uttaranchal</option>
                        <option  value="UP">Uttar Pradesh</option>
                        <option  value="WB">West Bengal</option>
                      </select>
                    </div>
                    @endif
                    

                   
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>

<script>
  function resetForm(){
      $('input[type=text]').val("");
      $('input[type=number]').val("");
      $('textarea').val("");
      
    }
</script>

@stop
