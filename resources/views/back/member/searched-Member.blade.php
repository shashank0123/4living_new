<?php 
use App\Models\MemberDetail;
use App\Models\Member;
use App\Models\Package;
?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member List</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">

@if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif

      <section class="tables-data" style="padding: 5px 10px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Member List</h1>
        </div>
        <div style="text-align: right;">
            <form method="get" action="/admin/searched-members">
              <input type="search" class="form-group" name="search_username" style="padding: 5px" placeholder="Enter username here" />
              <button type="submit" class="btn btn-primary">Search</button>
            </form>

        </div>
        
        <div class="card">
          <div>
            <div class="container">

              <table  class="table table-full-small " style="text-align: center;">
                <thead>
                  <tr>
                    <th>Joined On</th>
                    <th>Username</th>
                    <th>Phone</th>
                    <th>Package</th>
                    <th>Direct ID</th>
                    <th>Registered By</th>
                    <th>UPDATE</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $count = 0; ?>
                  @if(!empty($members))
                  @foreach($members as $member)
                  <?php $count++; ?>
                  <?php $i = 1; ?>
                  <?php
                  $mbr = MemberDetail::where('member_id',$member->id)->first();

                  ?>
                  <tr>
                    <td>{{$member->created_at}}</td>
                    <td>@if(!empty($member->username)){{$member->username}}@endif</td>
                    <td>@if(!empty($mbr->mobile_phone)){{$mbr->mobile_phone}}@endif</td>
                    <td>
                      <?php
                      $package = Package::where('id',$member->package_id)->first();
                      ?>
                      @if(!empty($package)){{$package->package_amount}}@endif</td>
                    <td>
                      <?php
                      $direct = Member::where('id',$member->direct_id)->first();
                      ?>
                      @if(!empty($direct)){{$direct->username}} @else{{'-'}} @endif</td>
                    <td>
                      
                      {{$member->register_by}}</td>
                    <td>
                      <a href="/admin/personal-info/{{$member->id}}" class="btn btn-primary">Personal Info</a>
                      <br>
                      <a href="/admin/bank/{{$member->id}}" class="btn btn-primary" style="margin-top: 5px">Bank Detail</a>
                      <a href="/admin/password/{{$member->id}}" class="btn btn-primary" style="margin-top: 5px">Password</a>
                    </td>
                    <th>
                      <form method="POST" action="/admin/delete/{{$member->id}}"><button type="submit" class="btn btn-danger">Delete</button></form>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  
              
                </tbody>
              </table>

              @if($count == 0)
              <div style="text-align: center; margin-top: 2%">
                  <h1 style="text-align: center;">No Result Found</h1>
                </div>
                  @endif

            </div>

          </div>

        </div>
      </section>
    </div>
  </div>
</main>
@stop
