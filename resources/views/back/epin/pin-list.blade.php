<?php 
use App\Models\Epin;

?>


@extends('back.app')

@section('title')


@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
      
      <section class="tables-data" style="padding: 30px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Epin List</h1>
        </div>
		
        <div style="text-align: right;">
            <a href="add-epin"><button type="submit" class="btn btn-alt-primary">Add Epin</button> </a>

        </div>
        
        <div style="overflow: auto;">
			<table  class="table table-full-small " style="text-align: center;">
                <thead>
                  <tr>
					<th>Sr. No.</th>
                    <th>Created On</th>
                    <th>Epin</th>
					<th>Epin Type</th>
                    <th>Status</th>
		       
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $m=1; ?>
                  @foreach($epin as $value)
                 <tr>
				 <?php $datime = date('d M, Y H:i:s', strtotime($value->created_at));?>
					<td>{{$m}}</td>
					<td>{{$datime}}</td>
					<td>{{$value->epin}}</td>
					<td>{{$value->epin_type}}</td>
					<td>
					@if ($value->status=='Used')
						<span class="btn btn-circle btn btn-danger">{{ $value->status }}</span>
					@else
						<span class="btn btn-circle btn-success">{{ $value->status }}</span>
					@endif
					</td>
					
					
									
					<td>
    
      <a class="btn btn-danger" href="{{ url('/admin/pin-list', $value->id) }}" onclick="return myFunction();"><i class="fa fa-trash"></i></a>
    </td>
				</tr>
				<?php $m++; ?>
                  @endforeach
				  
							  
                </tbody>
					
              </table>
</div>
</div>
		{{ $epin->links("pagination::bootstrap-4") }}
      </section>
	  
    </div>
  </div>
</main>
<script>
  function myFunction() {
      if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
 </script>
 
@stop
