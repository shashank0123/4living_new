<?php
use App\Models\Member;
?>

@extends('back.app')

@section('title')
  All Transfer | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">Funds Transfer History</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">

        <section style="padding:1% 4%">

        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">
            <h1><i class="md md-account-balance"></i> Funds Transfer History</h1>
          <p class="lead">Funds Transfer History.</p>
          </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/admin/transfer-funds/search" style="margin-top: 20px">                 
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 40%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
            </div>
          </div>
        </div>
           
      </section>


        <section class="tables-data" style="padding: 1% 5%">
        
        <div class="card">
          <div class="card-content">
              <table class="table">
                <thead>
                  <tr>
                    <th>Created Date</th>
                    <th>From</th>
                    <th>To</th>
                    <th>funds</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php $count = 0; ?>
                  @if(!empty($epoints))
                  @foreach($epoints as $epoint)
                  <?php $count++; ?>
                  <tr>
                    <td>{{$epoint->created_at}}</td>
                    <?php
                    $sender = Member::where('id',$epoint->sender_id)->first();
                    $receiver = Member::where('id',$epoint->receiver_id)->first();
                    ?>
                    <td>@if(!empty($sender)){{$sender->username}}@else{{'admin'}}@endif</td>
                    <td>
                      @if(!empty($receiver)){{$receiver->username}}@else{{'admin'}}@endif</td>
                    <td>{{$epoint->no_of_epoint}}</td>
                    
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>

              @if($count == 0)
              <div style="text-align: center;margin-top: 2%;">
                <h3>No Result Found</h3>
              </div>
              @endif
            </div>
          </div>
        </div>
      </section>
      </div>
    </div>
  </main>
@stop
