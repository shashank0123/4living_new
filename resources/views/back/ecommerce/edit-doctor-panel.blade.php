@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/doctor-panel"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="/admin/edit-doctor-panel/{{$doctorspannel->id}}" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="status">Panel Type</label><br>
							<select name="panel_type" class="form-control">
								<option value="consultwithdoctors" {{ 
								$doctorspannel->panel_type == 'consultwithdoctors'? 'selected': null }}>Consult with Doctors</option>
								<option value="panelofdoctors" {{ $doctorspannel->panel_type =='panelofdoctors'? 'selected': null }}>Panel of Doctors</option>
							</select>
						</div>
					</div>				
				</div>     
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="question">Name</label><br>
							<input type="text" class="form-control" id="name" name="name" value="{{ $doctorspannel->name }}">
						</div>
					</div>					
				</div>


				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="answers">Specialization</label><br>
							<input type="text" class="form-control" id="designation" name="designation" value="{{ $doctorspannel->designation }}">
						</div>
					</div>

					
				</div>
				<div class="form-group row">
					<div class="col-md-12">
						  <div class="form-material floating">
							<label for="keyword">Doctor's Image</label><br>
							<input type="file" class="form-control" id="image_url" name="image_url" >
						</div>
						
						@if( $doctorspannel->image )
						<img src="/assetsss/images/doctorspanel/{{ $doctorspannel->image }}" class="logo-img" />
					@endif
					</div>
					</div>
					<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Doctor's Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="description" class="form-control" id="description" >{{$doctorspannel->description}}</textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="question">Show Rank</label><br>
							<input type="text" class="form-control" id="rank" name="rank" value="{{ $doctorspannel->rank }}">
						</div>
					</div>					
				</div>
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="status">Status</label><br>
							<select name="status" class="form-control">
								<option value="0" {{ $doctorspannel->status=='0'? 'selected': null }}>Deactive</option>
								<option value="1" {{ $doctorspannel->status=='1'? 'selected': null }}>Active</option>
							</select>
						</div>
					</div>				
				</div>                   
				<button type="submit" class="btn btn-alt-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

