<?php
use App\Models\Member;
use App\Banner;
use App\Models\Announcement;
use App\Models\MemberDetail;
$count = Member::count();
$level1 = Member::where('level',1)->count();
$level2 = Member::where('level','>',1)->count();
$announcements = Announcement::orderBy('created_at','DESC')->get();

$recent_joined = Member::where('package_amount','>',0)->orderBy('created_at','DESC')->limit(10)->get();
$banners = Banner::where('image_type','banner')->where('status','Active')->get();
$count_banner = 1;

$top_earners = Member::where('max_pairing_bonus','>',0)->orderBy('max_pairing_bonus','DESC')->limit(5)->get();
$monthly_earners = Member::where('max_pairing_bonus','>',0)->orderBy('max_pairing_bonus','DESC')->orderBy('created_at','DESC')->limit(5)->get();
?>

@extends('layouts.ecommerce2')
@section('content')
   <!-- header slider -->
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="asset/images/banner/banner6.jpg" alt="imsys" width="100%" height="500">
    </div>
    <div class="carousel-item">
      <img src="asset/images/banner/banner2.png" alt="Chicago" width="100%" height="500">
    </div>
    <div class="carousel-item">
      <img src="asset/images/banner/banner3.png" alt="tulsi" width="100%" height="500">
    </div>
  </div>
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
  <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
  <span class="carousel-control-next-icon"></span>
  </a>
</div>
<!-- end slider  -->
<div class="container-fluid">
  <div class="row lrmargin ">
    <div class="homesec2 pagemarginsec">
      <h2 class="sechead">Welcome to 4LivinG </h2>
      <hr class="hrstyle">
      
      
      
      
      <p>
4livinG offers Ayurvedic Formulation and Dietary Supplement that supports the immune system, health, body transformation, and general wellness. Achieve your healthy lifestyle goal with 4livinG product to help build your immune system, muscle mass and more. Purchase a product as a customer or build your own business as a 4LivinG Distributor.</p>

<p>4LivinG Organic has been providing high quality, affordable Ayurvedic herbs trusted by health-care professionals and health sears. 4LivinG based on 5 Principles of vision, integrity, opportunity, freedom, and success. The Company has set its mission to create healthy India by offering the best Herbs -Nutritional product for Mankind 4LivinG is a one -of -a- kind, life-changing unique company and behind this company are some amazing leaders pressing forward. Drawing on years of experience and innovative new ideas. </p>

<p style="text-align: right;">Thank for your love & belief in us.<br></p>       
       
      </P>
      <div class="readmorebtn" style="text-align: right">
        <a href="/about-us"><button type="button" class="btn btn-success">Read More...</button></a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid pagemarginsec">
 
  <div class="row lrmargin align-items-center ">
    <div class=" col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="homesec3_1">
        <p class="quotes "><i>"My Dream of the world free from The Suffering of degenerative Disease to Aware the people in Important of Ayurveda in our life."</i></p><br><br><br>
        <div class="quotes2">
          <p><b style="font-size:larger;">Dr. Prem Prakash</b> <br>BVSc. AH; hvs-1<br>Rtd:NDDY;CCA(Accu) <br>(Chairman of Board)<br>4 Living</p>
        </div>
        <br><br><br><br><br><br><br>
        <div class="mentorreadmore">
      
        </div>
      </div>
    </div>
    <div class=" col-lg-6 col-md-6 col-sm-6 col-12">
        <div class="text-center">
      <img  src="asset/images/premp.jpg" class="img-responsive" alt="Prem Prakash"> 
  </div>
    </div>
  </div>
</div>



<div class="container-fluid pagemarginsec">
  <h2 class="sechead">Core Value </h2>
  <hr class="hrstyle">
  <div class="row lrmargin ">
    <div class=" col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="homesec4">
       <img  class="img-responsive" src="asset/images/core/excellence.jpg">
       <h5>EXCELLENCE</h5>
       <p>How to create products to how we sell them, we do it all with excellence. We never compromise.</p>
      </div>
    </div>
    <div class=" col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="homesec4">
       <img class="img-responsive"  src="asset/images/core/community.jpg">
       <h5>COMMUNITY</h5>
       <p>
A healthy, happy life can't be enjoyed without family, friends, and the community as a whole. 4LivinG believes in supporting communities, both locally and globally.
</p>
      </div>
    </div>
    <div class=" col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="homesec4">
       <img class="img-responsive"  src="asset/images/core/integrity.jpg">
       <h5>INTEGRITY</h5>
       <p>
4LivinG stands by its products and its business, and we choose to do what's both ethical and beneficial for our customers, employees, and the community.</p>
      </div>
    </div>
    <div class=" col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="homesec4">
       <img class="img-responsive"  src="asset/images/core/health.jpg">
       <h5>HEALTH</h5>
       <p>
Without health, life can't be enjoyed. We want you to enjoy your life, doing whatever you love to do. Our products are made to help you.</p>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
   
</style>

  
 @endsection