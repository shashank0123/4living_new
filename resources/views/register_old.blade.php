<?php
  use App\Models\Package;

  $packages = Package::where('package_amount','>',0)->where('package_amount','<',10000)->get();

?>

@extends('front.app_living')


<style>

  .pink-text {  font-size: 24px;  }

  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .card.bordered .card-action {
   border-top: none !important; 
 }
 .form-control { color: #000 !important; }
 select option { color: #000; }
 select {text-transform: uppercase !important;}
 label { text-transform: uppercase !important; }
 .card.bordered .card-header {
  border-bottom: 1px solid #000 !important;
}

[type='text'].form-control { color: #000; }

#register label, #otpcontainer label { color: #000; }

.pull-right button { margin-bottom: 20px !important }
@media screen and (max-width: 991px)
{
  .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
}

@media screen and (max-width: 580px)
{
  .card { max-width:90%;margin-left: 5%; }
}

#member-detail { display: none; }
#member-detail .row{ width: 50%; background-color: #eee }
@media screen and (max-width: 768px){
  #member-detail .row{ width: 90% }
}

#login-head { width: 100% !important; background-color: #1b4f82 }
#otpcontainer { display: none; }
#member-detail {display: none;}
</style>

<style>
  .package_list li h3{
    font-weight: bold;
    padding: 1%;
   margin-top: 15px;

  }

.member_text{
  /*margin: 1% ;*/
  width: 100%;
  padding: 10px;
   background-color: green;
    color: #fff;
}

  .package_list li{
    background-color: #fff;
    height: 210px;
    float: left;
    padding: 7px;
     margin: 10px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  }

  /*.packages-section{

    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    width: 100%;
  }
*/
  .package_list li:hover{
    
    zoom: 1.02;
  }

  input[type="radio"]{
    width: auto;
    height: auto;
  }

  .select_package{
    margin-top: 35px;
    text-align: center;
    padding: 10px;
    margin-left: 20px;
   background-color: green;
    color: #fff;
    width: fit-content;
  }

  .package_list li a {
    margin-top: 10px;
    font-weight: bold;

  }

  #packages h2{
    text-align: center;
    color: #333333;
    font-weight: bold;
    margin-bottom: 45px;
  }

  .membership{
    padding: 75PX;
    text-align: center;
    color: #333333;
    font-weight: bold;
  }
  .form-control{
    margin-bottom: 25px;
  }
  label{
    color: #777777;
    font-weight: bold;
  }
  .family_detail { 
  display: none;
   }
</style>

@section('title')
Register - {{ config('app.name') }}
@stop

 
@section('content')




<!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
 <div class="login-register-area pt-85 pb-90">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                        <div class="login-register-wrapper">
                            <div class="login-register-tab-list nav">
                                <a  href="/login">
                                    <h4> login </h4>
                                </a>
                                <a class="active" href="/register">
                                    <h4> register </h4>
                                </a>
                            </div>
      <div class="login-form-container" style="background-color: #f5f5f5">
  <div id="formcontainer" class="container m-b-30 tab-content" style="max-width: 100%;">
    <form class="form-floating action-form" id="register" http-type="post" data-url="{{ route('registerforuse', ['lang' => \App::getLocale()]) }}" enctype="multipart/form-data">
      <div class="card-content">
        <div class="m-b-30">
          <!-- <div class="card-title strong black-text" style="font-weight: bold;text-transform: uppercase !important;">Registration Form - Become a member</div> -->
        </div>

<div class="packages-section">
@if(!empty($packages))
<div class="form-group" id="packages">
    <h2>MEMBERSHIP PLANS</h2>
  <div class="row">
    <ul class="package_list">
      @foreach($packages as $package)
      <li>
        <div class="member_text">Member Registration</div>
        <h3> Rs. {{number_format((float)$package->package_amount, 0, '.', '')}} /- </h3>
        <div class="select_package"><input type="radio" name="package_id" id="package_id" value="{{$package->id ?? ''}}" required="required">&nbsp;&nbsp;<span>Select Now</span></div>
        <a href="{{ url('products_detail/'.$package->id) }}"><u>PD</u></a>
      </li>
      @endforeach
    </ul>
  </div>
</div>
@endif
</div>


 <h2 class="membership">MEMBERSHIP  FORM</h2>



<div class="row">
  <div class="col-s">
          <label for="referal_id">Sponsor ID</label>
          <input type="text" style="text-transform: uppercase;" @if (isset($_GET['referal_id'])) {{'readonly'}} @endif value="@if (isset($_GET['referal_id'])) {{$_GET['referal_id']}} @endif" name="referal_id" class="form-control" id="referal_id" required="required" onchange = "getUserDetail()">
        </div>

  <div class="col-sm-6">
    <label>Pan  Number *</label>
    <input type="text" name="pan" class="form-control" id="pan" required="required">
  </div>
  <div class="col-sm-6">
    <label>Upload Passport Size Photo *</label>  
    <input type="file" name="profimage" class="form-control" id="profimage" required="required">  
  </div>
  <div class="col-sm-6">
    <label>Name Mr./Mrs./Miss. *</label>    
    <select name="gender_type" id="gender_type" class="form-control">
      <option></option>
      <option value="Mr">Mr</option>
      <option value="Mrs">Mrs</option>
      <option value="Miss">Miss</option>
    </select>
    <input type="text" name="fullname" class="form-control" id="fullname" required="required">
  </div>
  <div class="col-sm-6">
    <label>Father Name / Husband Name *</label>
    <select name="fh_type" id="fh_type" class="form-control">
      <option></option>
      <option value="Father">Father</option>
      <option value="Husband">Husband</option>
    </select>
    <input type="text" name="fh_name" class="form-control" id="fh_name" required="required">
  </div>


  <div class="col-sm-3">
    <label>D. O. B *</label>
    <input type="text" name="date_of_birth" class="form-control" id="date_of_birth" required="required">

  </div>
  <div class="col-sm-3">
    <label>Mobile/Telephone No. *</label>
    <input type="text" name="mobile" class="form-control" id="mobile" required="required">
  </div>
  <div class="col-sm-3">
    <label>Email *</label>
    <input type="text" name="email" class="form-control" id="email" required="required">
  </div>
  <div class="col-sm-3">
    <label>Martial Status *</label>
    <select name="marital_status" id="marital_status" class="form-control">
      <option value=""> Select Marital Status</option>
      <option value="Married">Married</option>
      <option value="Unmarried">Unmarried</option>
    </select>
  </div>



  <div class="col-sm-12">
    <label>Address *</label>
    <input type="text" name="address" class="form-control" id="address" required="required">
  </div>
  
<div class="col-sm-4">
    <label>City *</label>
    <input type="text" name="city" class="form-control" id="city" required="required">
  </div>
  <div class="col-sm-4">
    <label>State *</label>
    <input type="text" name="state" class="form-control" id="state" required="required">
  </div>
  <div class="col-sm-4">
    <label>Pin Code *</label>
    <input type="text" name="pin_code" class="form-control" id="pin_code" required="required">
  </div>
  
  
  <div class="col-sm-4">
    <label>Family Member *</label>
    <select name="family_member" id="family_member" class="form-control" onclick="getFamily()">
      <option value="">Select</option>
      <option value="0">No</option>
      <option value="1">Yes</option>
    </select>
  </div>






</div>
<div class="family_detail">


  <h2 class="membership">DETAILS OF FAMILY MEMBERS</h2>
<div class="row">
  
<div class="col-sm-3">
  <label>Name *</label>
  <input type="text" name="member_name" class="form-control" id="member_name" required="required">
</div>
<div class="col-sm-3">
  <label>Relation *</label>
  <input type="text" name="member_relation" class="form-control" id="member_relation" required="required">
</div>
<div class="col-sm-3">
  <label> Married / Unmarrid *</label>
  <select name="member_marital_status" id="member_marital_status" class="form-control">
      <option value="">Select Marital Status</option>
      <option value="Married">Married</option>
      <option value="Unmarried">Unmarried</option>
    </select>
</div>
<div class="col-sm-3">
  <label> D. O. B *</label>
  <input type="text" name="member_date_of_birth" class="form-control" id="member_date_of_birth" required="required">
</div>

</div>
</div>




  <h2 class="membership">Occupation DETAILS</h2>
<div class="row">
  
<div class="col-sm-4">
  <label>Occupation / Office Name *</label>
  <input type="text" name="occupation_name" class="form-control" id="occupation_name" required="required">
</div>
<div class="col-sm-4">
  <label>Occupation / Office Address and Details *</label>
  <input type="text" name="occupation_address" class="form-control" id="occupation_address" required="required">
</div>
<div class="col-sm-4">
  <label>Type of Occupation *</label>
  <input type="text" name="occupation_type" class="form-control" id="occupation_type" required="required">
</div>


</div>














        <div class="form-group">
          <label for="referal_id">Sponsor ID</label>
          <input type="text" style="text-transform: uppercase;" @if (isset($_GET['referal_id'])) {{'readonly'}} @endif value="@if (isset($_GET['referal_id'])) {{$_GET['referal_id']}} @endif" name="referal_id" class="form-control" id="referal_id" required="required" onchange = "getUserDetail()">
        </div>



        <div class="form-group" id="member-detail">
          <div class="container">
            <div class="row" style="color: #000; font-weight: bold; border-bottom: 1px solid #fff">
              &nbsp;&nbsp;&nbsp;&nbsp;<span id="uname" width="200px"; style="color: #000; font-weight: normal;"></span><br>
              {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Email : <span id="uphone">1234</span><br> --}}

              {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Contact : <span id="uemail">@def.com</span> --}}
            </div>
          </div>
        </div>
         {{-- <div class="form-group">
          <label for="placement">Placement</label>
          <select  class="form-control" name="placement">
            <option value="left">Left</option>
            <option value="right">Right</option>
          </select>
          <input type="text" name="username" class="form-control" id="username" required="required"> 
        </div> --}}

        <div class="form-group">
          <label for="fullname">Name</label>
          <input type="text" name="fullname" class="form-control" id="fullname" required="required">
        </div>
			<div class="form-group">
          <label for="pan">Pan Number</label>
          <input type="text" name="pan" class="form-control" id="pan" required="required">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text" name="email" class="form-control" id="email" required="required">
        </div>

        <!--<div class="form-group">-->
        <!--  <label for="username">Username</label>-->
        <!--  <input type="text" name="username" class="form-control" id="username" required="required" onchange="checkExistance()">-->
        <!--</div>-->

        <p id="check-availability" style="color: #800000; font-weight: bold;"></p>

        <div class="form-group">
          <label for="mobile">Mobile</label>

          <input type="text" name="mobile" class="form-control" id="mobile" pattern="[789][0-9]{9}" required="required">
        </div>

<div class="form-group">
          <label for="occupation">Occupation</label>

          <input type="text" name="occupation" class="form-control" id="occupation" pattern="[789][0-9]{9}" required="required">
        </div>

        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" class="form-control" id="password" required="required" minlength="5" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
        </div>

        <div class="form-group">
          <label for="password">Confirm Password</label>
          <input type="password" name="cpassword" class="form-control" id="cpassword" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
        </div>

		{{-- <div class="form-group">
          <label for="password">Transaction Password</label>
          <input type="password" name="secret_password" class="form-control" id="secret_password" required="required">
        </div>

        <div class="form-group">
          <label for="password">Confirm Transaction Password</label>
          <input type="password" name="spassword" class="form-control" id="spassword" required="required">
        </div> --}}
        <div class="form-group">
          <label for="dob">DOB</label>
          <input type="date" name="dob" class="form-control" id="dob" required="required">
        </div>
		<div class="form-group">
          <label for="address">Address</label>
          <textarea style="border:1px solid #dddddd;padding:5px" name="address" class="form-control" id="inputAddress" ></textarea>
        </div>
        
		<div class="form-group">
          <label for="city">City</label>
          <input type="text" name="city" class="form-control" id="city" required="required">
        </div>
        
        	<div class="form-group">
          <label for="district">District</label>
          <input type="text" name="district" class="form-control" id="district" required="required">
        </div>
		
			<div class="form-group">
          <label for="state">State</label>
          <input type="text" name="state" class="form-control" id="state" required="required">
        </div>
		<div class="form-group">
          <label for="pin">Pin Code</label>
          <input type="text" name="pin" class="form-control" id="pin" required="required">
        </div>
        
        <!--<div class="form-group">-->
        <!--              <div class="checkbox">-->
        <!--                <label>-->
        <!--                  <input type="checkbox" required="" name="terms"> @lang('register.agreed')-->
        <!--                </label>-->
        <!--              </div>-->
        <!--            </div>-->

        {{-- <div class="form-group">
          <label for="username">PAN Number</label>
          <input type="text" name="pan" class="form-control" id="pan" pattern="[A-Z]{5}[0-9]{4}[A-Z]{1}" required="required">
        </div> --}}
        
        
      </div>
      
      

      <div class="card-action clearfix">
       

        <div class="pull-left">
          <span id="submit" onclick="showDiv()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
    </form>
    <br>
  </div>

  <div id="otpcontainer" class="container m-b-30" style="max-width: 100%;">
    <form id="otpform" class="form-floating action-form" http-type="post" data-url="{{ route('verifyotp', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong black-text" style="font-weight: bold;">Verify Your OTP</div>
        </div>
        <input type="hidden" name="username" id="emailvalue">
        <input type="hidden" name="sponsoredId" id="sponsoredId">

        <div class="form-group">
          <label for="username">Enter OTP</label>
          <input type="text" name="otp" class="form-control" id="otp" required="">
        </div>
      </div>
      <input type="hidden" name="verify_otp" class="form-control" id="verify_otp" required="">

      <div class="card-action clearfix"> 
        <div class="pull-right" >
          <span id="verify" onclick="verifyOTP()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


@stop
 
@section('script')
<script>     

   
function getFamily(){
  var data = $('#family_member').val();

  if(data == '1'){
  $('.family_detail').show();
  }
  else{
  $('.family_detail').hide();
  }
}




  function getUserDetail(){
      var name = $('#referal_id').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          // alert(msg);
          if(msg.message == 'Success'){
            $('#member-detail').show();
            $('#uname').html("Name : "+msg.name);
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }          
        }
      });
    }

    function checkExistance(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-availability",
        data:{ uname: name },
        success:function(msg){
            if(msg.message == 'Username already exists. Try another'){

              $('#check-availability').show();
          $('#check-availability').html(msg.message);
            }
        else{
          $('#check-availability').hide();
        }
        }
      });
    }


    function showDiv(){    
     var request = $.ajax({
      type: "POST",
      url: "/register",
      data: $('#register').serialize(),

      success:function(data){
        if(data.message == "Registerd Successfully"){
            $('#formcontainer').hide();
            $('#emailvalue').val(data.username);
       $('#sponsoredId').val($('#sponsorId').val());
            $('#otpcontainer').show();
        //   Swal(data.message);
        // setTimeout(function(){ window.location.href = "/en/login"; }, 3000);
        }
        else{
          if(data.success == 'false'){
            if(data.message.mobile)
            Swal(data.message.mobile[0]);
          if(data.message.email)
            Swal(data.message.email[0]);
          }
          else{

          Swal(data.message);
          }
        }
      },

      failure:function(data){

        if(data.success == 'false'){
            Swal(data.success);
          }
          else{
            
          Swal(data.success);
          }
          
      }
    });
}

//     request.done(function(msg) {
//     if (msg.message == 'Registerd Successfully'){
//       alert('hii');
//       // $('#formcontainer').hide();
//       // $('#emailvalue').val($('#email').val())
//       // $('#sponsoredId').val($('#sponsorId').val())
//       console.log(msg);
//       $("#otpcontainer").show();
//     }
//     else{
//       alert('not');
//       Swal(msg.message);                  
//     }
// });
//     request.fail(function(msg) {
//     alert('fail');
//     Swal(msg.message);
//   });


function verifyOTP(){    
  var request = $.ajax({
    type: "POST",
    url: "verifyotp",
    data: $('#otpform').serialize(),  
  });
  request.done(function(msg) {
    if (msg.message == 'Registered Successfully.'){
      console.log(msg);
      Swal(msg.message);
      // window.location.href = "/";
      setTimeout(function(){ window.location.href = "/"; }, 3000);     
      // window.location.href = "/register/membership-fee/"+msg.user_id;      
    }
    else{
      Swal(msg.message);
    }
  });
  

  request.fail(function(msg) {
    Swal(msg.message);
  });
}  




//     sucess:function(msg){
//       if (msg.message == 'Registered Successfully.'){
//         console.log(msg);
//         Swal(msg.message);
//         setTimeout(function(){ window.location.href = "/"; }, 2000); }

//       // window.location.href = "/register/membership-fee/"+msg.user_id;      

//     else{
//       Swal(msg.message);
//     }  
//     }
// });
// }
</script>
@endsection
