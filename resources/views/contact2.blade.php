@extends('layouts.ecommerce2')
@section('content')
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);
button {
  overflow: visible;
}
body {
	background-color: #f2f3eb;
}
button, select {
  text-transform: none;
}

button, input, select, textarea {
  color: #5A5A5A;
  font: inherit;
  margin: 0;
}

input {
  line-height: normal;
}

textarea {
  overflow: auto;
}

#container {
  border: solid 3px #474544;
  max-width: 800px;
  margin: 60px auto;
  position: relative;
  background-color: #fff;
}

form {
  padding: 37.5px;
  margin: 50px 0;
}

h1 {
  color: #474544;
  font-size: 32px;
  font-weight: 700;
  letter-spacing: 7px;
  text-align: center;
  text-transform: uppercase;
  padding-top: 20px;
}

.underline {
  border-bottom: solid 2px #474544;
  margin: -0.512em auto;
  width: 80px;
}

.icon_wrapper {
  margin: 50px auto 0;
  width: 100%;
}

.icon {
  display: block;
  fill: #474544;
  height: 50px;
  margin: 0 auto;
  width: 50px;
}

.email {
	float: right;
	width: 45%;
}

input[type='text'], [type='email'], select, textarea {
	background: none;
  border: none;
	border-bottom: solid 2px #474544;
	color: #474544;
	font-size: 1.000em;
  font-weight: 400;
  letter-spacing: 1px;
	margin: 0em 0 1.875em 0;
	padding: 0 0 0.875em 0;
  text-transform: uppercase;
	width: 100%;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-o-box-sizing: border-box;
	box-sizing: border-box;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	-ms-transition: all 0.3s;
	-o-transition: all 0.3s;
	transition: all 0.3s;
}

input[type='text']:focus, [type='email']:focus, textarea:focus {
	outline: none;
	padding: 0 0 0.875em 0;
}

.message {
	float: none;
}

.name {
	float: left;
	width: 45%;
}

select {
  background: url('https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-arrow-down-32.png') no-repeat right;
  outline: none;
  -moz-appearance: none;
  -webkit-appearance: none;
}

select::-ms-expand {
  display: none;
}

.subject {
  width: 100%;
}

.telephone {
  width: 100%;
}

textarea {
	line-height: 150%;
	height: 150px;
	resize: none;
  width: 100%;
}

::-webkit-input-placeholder {
	color: #474544;
}

:-moz-placeholder { 
	color: #474544;
	opacity: 1;
}

::-moz-placeholder {
	color: #474544;
	opacity: 1;
}

:-ms-input-placeholder {
	color: #474544;
}

#form_button {
  background: none;
  border: solid 2px #474544;
  color: #474544;
  cursor: pointer;
  display: inline-block;
  font-family: 'Helvetica', Arial, sans-serif;
  font-size: 0.875em;
  font-weight: bold;
  outline: none;
  padding: 20px 35px;
  text-transform: uppercase;
  -webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	-ms-transition: all 0.3s;
	-o-transition: all 0.3s;
	transition: all 0.3s;
}

#form_button:hover {
  background: #474544;
  color: #F2F3EB;
}

@media screen and (max-width: 768px) {
  #container {
    margin: 20px auto;
    width: 95%;
  }
}

@media screen and (max-width: 480px) {
  h1 {
    font-size: 26px;
  }
  
  .underline {
    width: 68px;
  }
}

@media screen and (max-width: 420px) {
  h1 {
    font-size: 18px;
  }
  
  .icon {
    height: 35px;
    width: 35px;
  }
  
  .underline {
    width: 53px;
  }
  
  input[type='text'], [type='email'], select, textarea {
    font-size: 0.875em;
  }
}
a {
  text-decoration: none;
}
.social-box {
  margin: 0 auto;
  display: flex;
  justify-content: space-around;
  width: 80%;
}

.sharer {
  display: flex;
  position: relative;
  width: 80px; 
  height: 80px;
  overflow: hidden;
  border-radius: 28%;
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.08);
  background-color: #fff;
  font-size: 28px;
}

.sharer, .sharer::before, .sharer::after {
  font-family: helvetica, arial, sans-serif;
}

.sharer:hover .fa {
  display: none;
}

.sharer:hover::before {
  top: -40%;
}

.sharer.success::before {
  top: -208%;
}

.sharer:hover::after {
  display: flex;
}

.sharer.success.success::after {content: "Success!";}
.fb.sharer.success::after {color: #3B5998;}
.tr.sharer.success::after {color: #00ACED;}
.pi.sharer.success::after {color: #C92228;}


.sharer::before {
  display: block;
  content: "";
  z-index: 5;
  position: absolute;
  top: 127%;
  left: -40%;
  align-items: center;
  width: 180%;
  height: 180%;
  transition: all .65s cubic-bezier(.64,0,.34,1.5); 
  transform: rotate(45deg);
}

.fb.sharer::before {background-color: #3B5998;}
.tr.sharer::before {background-color: #00ACED;}
.pi.sharer::before {background-color: #C92228;}

.sharer::after {
  display: none;
  position: relative;
  margin: auto;
  z-index: 10;
  font-size: .45em;
  line-height: 1.3em;
  color: #fff;
  transition: all .3s cubic-bezier(.64,0,.34,1.5); 
}

.fb.sharer::after {content:"3k likes";}
.tr.sharer::after {content:"4.2k rets";}
.pi.sharer::after {content:"2.1k pins";}

.fa {
  margin: auto;
}

.fa-facebook #big {
  color: #3B5998;
  font-size: .88em;
}
.fa-twitter #big {
  color: #00ACED;
}
.fa-pinterest-p #big {
  color: #C92228;
  font-size: .95em;
}

</style>
<div class="container">
<div id="container">
  <h1>&bull; Keep in Touch &bull;</h1>
  <div class="underline">
  </div>
  <div class="icon_wrapper">

  </div>
  <form action="#" method="post" id="contact_form">
    <div class="name">
      <label for="name"></label>
      <input type="text" placeholder="My name is" name="name" id="name_input" required>
    </div>
    <div class="email">
      <label for="email"></label>
      <input type="email" placeholder="My e-mail is" name="email" id="email_input" required>
    </div>
    <div class="telephone">
      <label for="name"></label>
      <input type="text" placeholder="My number is" name="telephone" id="telephone_input" required>
    </div>
    <div class="subject">
      <label for="subject"></label>
      <select placeholder="Subject line" name="subject" id="subject_input" required>
        <option disabled hidden selected>Subject line</option>
        <option>I'd like to start a project</option>
        <option>I'd like to ask a question</option>
        <option>I'd like to make a proposal</option>
      </select>
    </div>
    <div class="message">
      <label for="message"></label>
      <textarea name="message" placeholder="I'd like to chat about" id="message_input" cols="30" rows="5" required></textarea>
    </div>
    <br>
    <div class="submit">
      <input type="submit" value="Send Message" id="form_button" />
    </div>
  </form><!-- // End form -->
</div><!-- // End #container -->
</div>
<div style="width: 100%; margin-top: 50px;">
   <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14009.132984805128!2d77.0613266!3d28.6212718!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xefaf47ed8e5b0456!2sBackstage+Supporters+pvt+ltd!5e0!3m2!1sen!2sin!4v1553057377771" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
@endsection