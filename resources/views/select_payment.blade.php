@extends('layouts.ecommerce2')

@section('content')
<style type="text/css">
	.section-associate{
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		height: 400px;     
        padding: 15px;                                                                                                                    
	}
    .associate_content{
        padding: 20px;
    }

    .payment_method{
        list-style-type: none;
        padding: 25px;
    }

    input[type='radio'] {
        height: 15px !important;
        margin-top: 5px !important; 
}

.form-group span{
    padding-top: 20px; 
}
.form-group img{
    height: 35px; width: auto;
}
.payment_section{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
<div class=" mt-30 mb-30">
           <img class="img-responsive"  src="asset/images/bg/breadcrumb.jpg">
        </div>
<div class="container mt-40 mb-50">
    <h2 class="sechead">Select Payment Mode</h2>
     <hr class="hrstyle">
     <br><br>
     <div class="row">
     	
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            
        <!-- <h2>Select Payment Mode</h2> -->
<div class="row payment_section">
    <div class="col-sm-12">
        <ul class="payment_method">

            <li class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <input type="radio" name="payment_method" id="payment_method" value="paytm">
                    </div>
                    <div class="col-sm-4">
                        <span> PayTM </span>
                    </div>
                    <div class="col-sm-5">
                        <img src="{{ asset('images/paytm.png') }}">
                    </div>
                </div>
            </li>

            <li class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <input type="radio" name="payment_method" id="payment_method" value="GooglePay">
                    </div>
                    <div class="col-sm-4">
                        <span> GooglePay </span>
                    </div>
                    <div class="col-sm-5">
                        <img src="{{ asset('images/gpay.png') }}">
                    </div>
                </div>
            </li>

            <li class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <input type="radio" name="payment_method" id="payment_method" value="PhonePay">
                    </div>
                    <div class="col-sm-4">
                        <span> PhonePay </span>
                    </div>
                    <div class="col-sm-5">
                        <img src="{{ asset('images/phonepay.png') }}">
                    </div>
                </div>
            </li>
<!-- 
            <li class="form-group">
                <div class="row">
                    <div class="col-sm-1">
                        <input type="radio" name="payment_method" id="payment_method" value="other">
                    </div>
                    <div class="col-sm-10">
                        <span> Other </span>
                    </div>
                </div>
            </li> -->
            <li>
                <br><br>
                <button class="btn btn-success" value="checkout">Proceed to Pay</button>
            </li>

            
        </ul>
    </div>
</div>

        </div>
        <div class="col-sm-4"></div>
     </div>

 </div>
@endsection