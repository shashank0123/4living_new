@extends('layouts.ecommerce2')

@section('content')
<style type="text/css">
	.section-associate,.section-product{
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		height: 300px;     
        padding: 15px;                                                                                                                    
	}
    .associate_content{
        padding: 20px;
    }
    
</style>
<div class=" mt-30 mb-30">
           <img class="img-responsive"  src="/asset/images/bg/breadcrumb.jpg">
        </div>
<div class="container mt-40 mb-50">
    <h2 class="sechead">Products Under Plan Rs. {{number_format((float)$package->package_amount, 0, '.', '')}}</h2>
     <hr class="hrstyle">

     <div class="row" >
     	@if(count($products)>0)


     	@foreach($products as $product)

     	<div class="col-sm-3">
            <div class=" section-product">
     		<div class="associate_image" style="height: 200px; text-align: center;margin-bottom: 20px;">
                @if(!empty($product->image1))
     			<img src="{{ asset('/images/products/'.$product->image1) }}" style="width: 100%; height: 200px;">
                @else
                <img src="{{ asset('/images/products/default.png') }}" style="width: 100%; height: 200px;">
                @endif
     		</div>
     		<div class="associate_content">
     			<h3>{{ $product->name ?? '' }}</h3>
     		</div>
        </div>
     	</div>

     	@endforeach

        @else
        <div style="text-align: center;">
        <p style="text-align: center;padding: 45px;">No Product Available</p>
    </div>

     	@endif
     </div>

 </div>
@endsection