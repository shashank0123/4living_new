<?php
use App\Models\MemberDetail;
$route = \Route::currentRouteName();
if (is_null($route)) $route = 'memberhome';
if ($route == 'announcement.read' || $route == 'coin.wallet.detail' || $route == 'coin.transaction.detail') {
  $routeEN = route($route, ['lang' => 'en', 'id' => $model->id]);
  $routeCHS = route($route, ['lang' => 'chs', 'id' => $model->id]);
  $routeCHT = route($route, ['lang' => 'cht', 'id' => $model->id]);
} else {
  $routeEN = route($route, ['lang' => 'en']);
  $routeCHS = route($route, ['lang' => 'chs']);
  $routeCHT = route($route, ['lang' => 'cht']);
}

$profile = MemberDetail::where('member_id',$member->id)->first();

?>


<style>
  .profile-pic{
    height: 45px; width: 45px; border-radius: 50%; background-color: #fff; text-align: center;
    margin-top: 8px;
  }
  .profile-pic img{
    height: 35px; width: 35px; padding: 3px 2px;
  }

  .header-top-right { text-align: right; }

  .header-top-right ul{
    list-style-type: none;
  }
  .header-top-right ul li {
    float: left;
  }
  .header-top-right li .fa-caret-down{
    margin-top: 25px;
  }
  .header-top-right li h4{
    padding: 10px; color: #fff; font-size: 16px; font-weight: bold;
  }
  .menu-dropdown ul{
    list-style-type: none;
    padding: 15px 20px;
  }
  .menu-dropdown ul li{
    color: #000 !important;
    padding: 5px ;
  }
  .menu-dropdown ul li a{
    color: #000 !important;
  }
  .menu-dropdown ul li:hover a, .menu-dropdown ul li:hover i{
    color: #1b4f82 !important;
  }



  .menu-dropdown ul li i{ padding-right: 15px; }

  .menu-dropdown{
    display: none;
    position: absolute;
    right: 10px;
    top: 50px;
    width: 225px;
    background-color: #fff;
     box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
     border-radius: 3px;

  }
</style>

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: #1b4f82 !important">
  <div class="container-fluid">
    <div class="navbar-header pull-left">
      <div class="container">


        <div class="row">
          <div class="col-sm-8">
            <button type="button" class="navbar-toggle pull-left m-15" data-activates=".sidebar"> <span class="sr-only">@lang('common.toggleNav')</span> 
              <i class="fa fa-bars" style="color: #fff; !important"></i>
              <!-- <span class="icon-bar"></span> -->
              <!-- <span class="icon-bar"></span> -->
              <!-- <span class="icon-bar"></span> -->
            </button>
          </div>
          <div class="col-sm-3 header-top-right">
            <ul>
              <li>
                <div class="profile-pic">                  
                  @if(!empty($profile->profimage))          
                  <img src="/assetsss/images/AdminProduct/{{$profile->profimage}}">          
                  @else
                  <img src="{{ asset('images/partner/defalut.png')}}">
                  @endif
                </div>
              </li>
              <li>
                <h4>
                 {{$member->gender_type ?? ''}} {{$member->name ?? ''}} - {{ $member->username ?? '' }}</h4>
               </li>
               <li onclick="getDropdownMenu()">
                <i class="fa fa-caret-down"></i>
              </li>
            </ul>
          </div>
        </div>


        <div class="menu-dropdown">
            <ul>
              <li>
                <span><i class="fa fa-user"></i></span>
                <span><a href="{{ url('en/settings/account') }}">My Profile</a></span>
              </li>
              <li>
                <span><i class="fa fa-lock"></i></span>
                <span><a href="{{ url('en/settings/account#accountBasicForm') }}">Change Password</a></span>
              </li>
              <li>
                <span><i class="fa fa-power-off"></i></span>
                <span><a href="{{ route('logout', ['lang' => \App::getLocale()]) }}">Logout</a></span>
              </li>
            </ul>
        </div>


      </div>
    </div>
  </div>
</nav>

