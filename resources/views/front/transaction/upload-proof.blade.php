@extends('front.app')

@section('title')
  @lang('settings.title2') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/login">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.settingsUpload')</li>
  </ul>
@stop
<style>

  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }

  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>
@section('content')

  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">

        
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.uploadProof')</h1>
            <p class="lead">@lang('settings.uploadProof2')</p>
          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary" >
      <p style="color: #800000; font-size: 14px;background-color: #eee; padding: 20px ">{{ $message }}</p>
    </div>
  @endif
              <div class="well" style="background-color: #fff">
                <form method="POST" action="{{ route('proof.postUploadProof') }}" enctype="multipart/form-data">
                  {{-- <form data-parsley-validate="" role="form" class="form-floating action-form" id="accountBankForm" http-type="post" data-url="{{ route('proof.postUploadProof') }}" enctype="multipart/form-data"> --}}
                  <fieldset>
                    <input type="hidden" name="member_id" value={{$member->id}}>
                    <div class="form-group">
                    <label class="control-label">UTR Number</label>
                    <input type="text" name="utr_number" class="form-control" required=""  id="utr_number">
                  </div>

                  <?php $countries = config('misc.countries'); ?>
                    <div class="form-group">
                      <label class="control-label" for="inputBankName">@lang('settings.bank.name')</label>
                      <select class="form-control" name="bank" id="inputBankName" autocomplete>
                        @foreach ($countries as $country => $value)
                          <optgroup label="{{ \Lang::get('country.' . $country) }}">
                            @foreach ($value['banks'] as $bank)
                              <option value="{{ $bank }}" @if ($member->detail->bank_name == $bank) selected="" @endif>{{ $bank }}</option>
                            @endforeach
                          </optgroup>
                        @endforeach
                      </select>
                    </div>

                  <div class="form-group">
                      <label class="control-label" for="inputAmount">@lang('withdraw.amount')</label>
                      <input type="text" name="amount" id="amount" class="form-control" required="">
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label" for="date">Date</label>
                      <input type="date" name="date" id="date" class="form-control" required="" value="">
                      
                    </div>

                    <div class="form-group">
                      <label class="control-label" style="margin-bottom: 20px;">@lang('withdraw.photo')</label>
                      <input type="file" name="utr_photo" id="utr_photo" class="form-control" required="">
                    </div>                    

                   
                    <input type="hidden" name="s" class="form-control" required="" value="{{$member->secret_password}}">
                    {{-- <div class="form-group">
                      <label class="control-label">@lang('settings.secret')</label> --}}
                      <input type="hidden" name="password" class="form-control" required="" value="{{$member->secret_password}}">
                    {{-- </div> --}}

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <script>
    function getUserDetail(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/en/user-name",
        data:{ uname: name },
        success:function(msg){
          $('#member-detail').show();
          $('#uname').html(msg.name);
          $('#uemail').text(msg.email);
        }
      });
    }
  </script>
@stop
