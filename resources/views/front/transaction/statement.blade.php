@extends('front.app')

@section('title')
@lang('wdStatement.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('transaction.withdraw', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.withdraw')</a></li>
  <li class="active">@lang('breadcrumbs.withdrawStatement')</li>
</ul>
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="row">
          <div class="col-sm-8">
        <div class="page-header">
          <h1><i class="md md-account-balance"></i> @lang('wdStatement.title')</h1>
          <p class="lead">@lang('wdStatement.subTitle')</p>
        </div>            
          </div>
          <div class="col-sm-4">
            {{-- <a href = "/en/download" clawss="btn btn-danger">Download</a> --}}
          </div>
        </div>

        <div class="card" style="background-color: #fff">
          <div class="card-content">
            <div class="datatables"style="background-color: #fff" >
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('withdraw.list', ['lang' => \App::getLocale()]) }}">
                <thead style="background-color: #fff">
                  <tr>
                    <th data-id="created_at">@lang('wdStatement.create')</th>
                    <th data-id="amount">@lang('wdStatement.amount')</th>
                    <th data-id="admin">@lang('wdStatement.adminFee')</th>
                    <th data-id="tds">TDS Charges</th>
                    <th data-id="credit">Credited Amount</th>
                    <th data-id="status">@lang('wdStatement.status')</th>
                  </tr>
                </thead>
                <tbody style="background-color: #fff">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>
@stop
