<style type="text/css">
@media screen and (max-width: 580px){
  #binary_box { overflow: auto; }
}.tf-tree{font-size:16px;overflow:auto}.tf-tree *{color:black;box-sizing:border-box;margin:0;padding:0}.tf-tree ul{display:inline-flex}.tf-tree li{align-items:center;display:flex;flex-direction:column;flex-wrap:wrap;padding:0 1em;position:relative}.tf-tree li ul{margin:2em 0}.tf-tree li li:before{border-top:5px solid #007bff;content:"";display:block;height:.0625em;left:-.03125em;position:absolute;top:-1.03125em;width:100%}.tf-tree li li:first-child:before{left:calc(50% - .03125em);max-width:calc(50% + .0625em)}.tf-tree li li:last-child:before{left:auto;max-width:calc(50% + .0625em);right:calc(50% - .03125em)}.tf-tree li li:only-child:before{display:none}.tf-tree li li:only-child>.tf-nc:before,.tf-tree li li:only-child>.tf-node-content:before{height:1.0625em;top:-1.0625em}.tf-tree .tf-nc,.tf-tree .tf-node-content{border:.0625em solid #eee;display:inline-block;padding:.5em 1em;position:relative}.tf-tree .tf-nc:before,.tf-tree .tf-node-content:before{top:-1.03125em}.tf-tree .tf-nc:after,.tf-tree .tf-nc:before,.tf-tree .tf-node-content:after,.tf-tree .tf-node-content:before{border-left: 5px solid #007bff;content:"";display:block;height:1em;left:calc(50% - .03125em);position:absolute;width:.0625em}.tf-tree .tf-nc:after,.tf-tree .tf-node-content:after{top:calc(100% + .03125em)}.tf-tree .tf-nc:only-child:after,.tf-tree .tf-node-content:only-child:after,.tf-tree>ul>li>.tf-nc:before,.tf-tree>ul>li>.tf-node-content:before{display:none}.tf-tree.tf-gap-sm li{padding:0 .6em}.tf-tree.tf-gap-sm li>.tf-nc:before,.tf-tree.tf-gap-sm li>.tf-node-content:before{height:.6em;top:-.6em}.tf-tree.tf-gap-sm li>.tf-nc:after,.tf-tree.tf-gap-sm li>.tf-node-content:after{height:.6em}.tf-tree.tf-gap-sm li ul{margin:1.2em 0}.tf-tree.tf-gap-sm li li:before{top:-.63125em}.tf-tree.tf-gap-sm li li:only-child>.tf-nc:before,.tf-tree.tf-gap-sm li li:only-child>.tf-node-content:before{height:.6625em;top:-.6625em}.tf-tree.tf-gap-lg li{padding:0 1.5em}.tf-tree.tf-gap-lg li>.tf-nc:before,.tf-tree.tf-gap-lg li>.tf-node-content:before{height:1.5em;top:-1.5em}.tf-tree.tf-gap-lg li>.tf-nc:after,.tf-tree.tf-gap-lg li>.tf-node-content:after{height:1.5em}.tf-tree.tf-gap-lg li ul{margin:3em 0}.tf-tree.tf-gap-lg li li:before{top:-1.53125em}.tf-tree.tf-gap-lg li li:only-child>.tf-nc:before,.tf-tree.tf-gap-lg li li:only-child>.tf-node-content:before{height:1.5625em;top:-1.5625em}.tf-tree li.tf-dotted-children .tf-nc:after,.tf-tree li.tf-dotted-children .tf-nc:before,.tf-tree li.tf-dotted-children .tf-node-content:after,.tf-tree li.tf-dotted-children .tf-node-content:before{border-left-style:dotted}.tf-tree li.tf-dotted-children li:before{border-top-style:dotted}.tf-tree li.tf-dotted-children>.tf-nc:before,.tf-tree li.tf-dotted-children>.tf-node-content:before{border-left-style:solid}.tf-tree li.tf-dashed-children .tf-nc:after,.tf-tree li.tf-dashed-children .tf-nc:before,.tf-tree li.tf-dashed-children .tf-node-content:after,.tf-tree li.tf-dashed-children .tf-node-content:before{border-left-style:dashed}.tf-tree li.tf-dashed-children li:before{border-top-style:dashed}.tf-tree li.tf-dashed-children>.tf-nc:before,.tf-tree li.tf-dashed-children>.tf-node-content:before{border-left-style:solid}


 .tf-tree{ text-align: center; } 
  .tf-tree img { height: 50px; width: 50px; border-radius: 50%; border: 1px solid #ccc;   }
  
  
  .binary-detail { position : absolute; top: 0; left: 100%; background-color: #000; color: #fff; z-index : 999;}
  
  .tooltip { width: 250px !important; }
  /*.tooltip { top: 15 !important; left: 450px!important; }*/


</style>

<div id="binary_box">
<?php
  use App\Models\Package;
  $packages = Package::all();
  $packagelist = array(); 
  foreach ($packages as $key => $value) {
    $packagelist[$value->id] = $value->package_color;
  }
  use App\Repositories\MemberRepository;
  $repo = new MemberRepository;

  

?>
<style>
  @media screen and (max-width: 580px){

  .binary-tree-responsive { overflow: auto !important; }
  }
</style>
<div class="binary-tree-responsive">
<ul id="mainTree" style="overflow: auto !important;">
  <li>
    <!--<a href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" class="tf-tree" data-id="{{ $model->username }}"><span class="tf-nc"> <img src="http://im.rediff.com/movies/2019/sep/23hrithik-roshan-01.jpg"> <br><p class="text-center">{{$model->username}}</p></a>-->
    <?php
        $jd = explode(' ',$model->created_at);
        ?>
        <a href="/en/network/binary?q={{$model->username}}" tabindex="0" role="button" class="tf-tree" data-html="true" data-placement="right" data-id="{{ $model->username }}"  data-toggle="tooltip"
        title="Name : {{$model->name}} <br/>Joining Date : {{$jd[0]}} <br/>Member ID : {{$model->referral_id}}">
            <span class="tf-nc"> 
        @if(!empty($model->profimage))
        <img src="{{url('/')}}/assetsss/images/AdminProduct/{{$model->profimage}}">
        @else
        
        @if($model->package_id>1)
        <img src="{{url('/')}}/assetsss/images/AdminProduct/user-icon.jpg">
        @else
        <img src="{{url('/')}}/assetsss/images/AdminProduct/user-blocked.jpeg">
        @endif
        
        
        <!--<img src="{{url('/')}}/assetsss/images/AdminProduct/user-icon.jpg">-->
        @endif
        <br><p class="text-center">{{$model->username}}</p>
        
        
        </a>
    @if ($children = $repo->findChildren($model)) <!-- render 4 levels -->
      <ul>
        @if (count($children) <= 0)
          <li>
            <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
          </li>
          <li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class=" tf-tree"><span class="tf-nc">Register<p class="text-center"></p></span></a>
              </li><li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class=" tf-tree"><span class="tf-nc">Register<p class="text-center"></p></span></a>
              </li><li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class=" tf-tree"><span class="tf-nc">Register<p class="text-center"></p></span></a>
              </li><li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class=" tf-tree"><span class="tf-nc">Register<p class="text-center"></p></span></a>
              </li><li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class=" tf-tree"><span class="tf-nc">Register<p class="text-center"></p></span></a>
              </li>
        @else
            <?php $count = count($children)?>
          @foreach ($children as $child)
          <?php
        $jd = explode(' ',$child->created_at);
        ?>
              <li>
        <!--        <a class=" tf-tree"  href="{{ route('network.binary', ['lang' => \App::getLocale()]) . '?u=' . $child->username  }}" tabindex="0" role="button"  data-html="true" data-placement="right" data-id="{{ $child->username }}" style="@if($child->package_id=='2'){{'background-color: #32CD32'}}@else{{'background-color: #ff0000'}}@endif" data-toggle="tooltip"-->
        <!--title="Name : {{$child->name}} <br/>Joining Date : {{$jd[0]}} <br/>Member ID : {{$child->referral_id}}"><span class="tf-nc">-->
             <a class=" tf-tree"  onclick="clickSubmit('{{$child->username}}')" tabindex="0" role="button"  data-html="true" data-placement="right" data-id="{{ $child->username }}" data-toggle="tooltip"
        title="Name : {{$child->name}} <br/>Joining Date : {{$jd[0]}} <br/>Member ID : {{$child->referral_id}}"><span class="tf-nc"> 
                 
                @if(!empty($child->profimage))
        <img src="{{url('/')}}/assetsss/images/AdminProduct/{{$child->profimage}}">
        @else
        @if($child->package_id>1)
        <img src="{{url('/')}}/assetsss/images/AdminProduct/user-icon.jpg">
        @else
        <img src="{{url('/')}}/assetsss/images/AdminProduct/user-blocked.jpeg">
        @endif
        @endif
                <!--<img src="http://im.rediff.com/movies/2019/sep/23hrithik-roshan-01.jpg"> -->
                <br><p class="text-center">{{$child->username}}</p></span></a>
              </li>
            
            
          @endforeach
          @for($i = 1;$i<= 6 - $count;$i++)
            <li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class=" tf-tree"><span class="tf-nc">Register<p class="text-center"></p></span></a>
              </li>
          @endfor
        @endif
      </ul>
    @endif
  </li>
</ul>
</div>
</div>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});


function clickSubmit(user)
{
    $('#inputMember').val(user);   
    getMemberDetail();
}

function getMemberDetail(){
  var CSRF_TOKEN = $('meta[name="_t"]').attr('content');
  var user = $('#inputMember').val();

  // alert(user);
  $.ajax({
    /* the route pointing to the post function */
    url: '/en/getMemberDetail',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, username: user},
    success: function (data) { 
      // alert(data)
      // window.location.href = '/en';
      $("#MemberNameGet").val(data);
      
    $('#sub').click();
    },
    failure: function(data){
      // alert(data)
      // window.location.href = '/en';
      $("#MemberName").val(data);
      
    $('#sub').click();
    }
  }); 
}

</script>
