<?php
use App\Models\Member;
use App\Models\Package;
$roots = array(); 

if (\Input::has('rid')) {
  $sMember = \App\Models\Member::where('id', trim(\Input::get('rid')))->first();
} else {
  $sMember = $member;
}

$arrId = array();
$arrId[0] = $member->id;
$srno = 1;

?>

@extends('front.app')
<style>
  aside.sidebar {
    background-color: #1b4f82!important;
  }
</style>
@section('title')
@lang('breadcrumbs.directgeneology') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.levelwise')</li>
</ul>
@stop
<style>
  ul li , ul li a { color: #fff !important; }
  .level1 img{
    height: 45px; width: 45px; border-radius: 50%;border: 1px solid #ccc;
  }
  .level2 img{
    height: 45px; width: 45px; border-radius: 50%;border: 1px solid #ccc;
  }
  .sublevel2{
    margin: 25px 0;
  }
  .level2{
    margin-left: 22px;
    border-left: 1px solid #ccc;
  }

  .sublevelleft2{
    border-bottom: 1px solid #ccc; margin-top: 45px;
  }
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-swap-vert"></i> @lang('breadcrumbs.directgeneology')</h1>
          <p class="lead">@lang('breadcrumbs.directgeneology')</p>
        </div>

        <div class="row m-b-40">

          <div class="card" style="background-color: #fff">
            <div>
              <div class="datatables">




                <div class="row">
                  <form class="floating-label"  id="directForm" data-url="" onsubmit="return false;">
                    <fieldset>
                      <div class="container">
                        <div class="row">

                          <div class="col-sm-3">
                            <div class="form-group">
                              <label class="control-label" for="inputMember">@lang('binary.member')</label>
                              <input type="text" name="u" class="form-control" required="" id="inputMember" value="" onchange="getMemberDetail()" style="padding: 0 10px;">
                            </div>
                          </div>

                          <div class="col-sm-3">
                           <div class="form-group">
                            <label class="control-label" for="fromDate">From Date</label>
                            <input type="text" class="form-control" id="fromDate" name="form_date" style="padding: 0 10px;">
                          </div>
                        </div>

                        <div class="col-sm-3">
                         <div class="form-group">
                          <label class="control-label" for="toDate">To Date</label>
                          <input type="text" class="form-control" id="toDate" name="form_date" style="padding: 0 10px;">
                        </div>
                      </div>

                      <div class="col-sm-3">
                        <div class="form-group">

                          <button type="submit" id="sub" class="btn btn-primary">
                            <span class="btn-preloader">
                              <i class="md md-cached md-spin"></i>
                            </span>
                            <span>@lang('common.submit')</span>
                          </button>

                        </div>
                      </div>

                    </div>
                  </div>

                </fieldset>
              </form>
            </div>



          </div>
        </div>
      </div>


      <div class="container">              
        <div class="level1">                
          <img src="{{url('/')}}/assetsss/images/AdminProduct/user-icon.jpg"> - <a href="{{ url('en/network/direct-member-geneology/'.$active_member->id) }}">[ {{ $active_member->username ?? '' }} ] @if(!empty($active_member->name)) - [ {{ $active_member->name ?? '' }} ] @endif</a> -[T.BV.: 000] - [M.BV.: 000] - [Self Rank: 1] - [RAP.: 24%] - [Roy: N.A.]
        </div>
        @if(count($submembers))
        @foreach($submembers as $sub )
        <div class="level2">   
          <div class="row">
            <div class="col-sm-1 col-xs-1" >
              <div class="sublevelleft2"></div>
            </div>
            <div class="col-sm-11 col-xs-11">

              <div class="row sublevel2">
                <img src="{{url('/')}}/assetsss/images/AdminProduct/user-icon.jpg">  - <a href="{{ url('en/network/direct-member-geneology/'.$sub->id) }}">[ {{ $sub->username ?? '' }} ] @if(!empty($sub->name)) - [ {{ $sub->name ?? '' }} ] @endif</a> -[T.BV.: 000] - [M.BV.: 000] - [Self Rank: 1] - [RAP.: 24%] - [Roy: N.A.]
              </div>

<?php

  $last_members = Member::where('parent_id',$sub->id)->get();

?>

              @if(count($last_members))
        @foreach($last_members as $last )
        <div class="level2">   
          <div class="row">
            <div class="col-sm-1 col-xs-1" >
              <div class="sublevelleft2"></div>
            </div>
            <div class="col-sm-11 col-xs-11">

              <div class="row sublevel2">
                <img src="{{url('/')}}/assetsss/images/AdminProduct/user-icon.jpg">  - <a href="{{ url('en/network/direct-member-geneology/'.$last->id) }}">[ {{ $last->username ?? '' }} ] @if(!empty($last->name)) - [ {{ $last->name ?? '' }} ] @endif</a> -[T.BV.: 000] - [M.BV.: 000] - [Self Rank: 1] - [RAP.: 24%] - [Roy: N.A.]
              </div>


            </div>
          </div>    
        </div>
        @endforeach
        @else
        <div class="level2">   
          <div class="row">
            <div class="col-sm-1 col-xs-1" >
              <div class="sublevelleft2"></div>
            </div>
            <div class="col-sm-11 col-xs-11">
              <br>No member found
            </div>
          </div>    
        </div>
        @endif


            </div>
          </div>    
        </div>
        @endforeach
        @else
       <div class="level2">   
          <div class="row">
            <div class="col-sm-1 col-xs-1" >
              <div class="sublevelleft2"></div>
            </div>
            <div class="col-sm-11 col-xs-11">
              <br>No member found
            </div>
          </div>    
        </div>
        @endif

      </div>


    </div>
  </section>
</div>
</div>
</main>
@stop
