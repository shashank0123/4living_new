<style type="text/css">
  aside.sidebar {
    background-color: #1b4f82!important;
}
@media screen and (max-width: 580px){
  #binary_box { overflow: auto; }
}


</style>

<div id="binary_box">
<?php
  use App\Models\Package;
  $packages = Package::all();
  $packagelist = array(); 
  foreach ($packages as $key => $value) {
    $packagelist[$value->id] = $value->package_color;
  }
  use App\Repositories\MemberRepository;
  $repo = new MemberRepository;

  function renderTree ($member, $level, $html, $packagelist) {
    if ($level >= 1) return;
    $repo = new MemberRepository;
    $html = '';
    if ($children = $repo->findChildren($member)) {
      $html .= '<ul>';
      if (count($children) <= 0) {
        $html .= '<li>
            <a href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=left" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
          </li>
          <li>
            <a href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=right" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
          </li>';
      } else {
        foreach ($children as $child) {
          if (count($children) < 2 && $child->position == 'right') {
            $html .= '<li>
              <a href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=left" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
              </li>';
          }

          if ($level < 0) {
            $html .= '<li>
              <a style="background:'.$packagelist[$child->package_id].'"  href="#" data-toggle="modal" data-target="#showModal" tabindex="0" role="button" data-id="' . $child->username . '"><i class="md md-accessibility"></i> ' . $child->username . ' (' . strtoupper($child->position[0]) . ')</a>';
          } else {
            $html .= '<li>
              <a style="background:'.$packagelist[$child->package_id].'"  href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" data-id="' . $child->username . '"><i class="md md-accessibility"></i>' . $child->username . ' (' . strtoupper($child->position[0]) . ')</a><a href="#" data-username="' . $child->username . '" data-more="true" title="More"><span class="md md-add"></span></a>';
          }

          $next = $level + 1;
          $html .= renderTree($child, $next, $html, $packagelist);
          $html .= '</li>';

          if (count($children) < 2 && $child->position == 'left') {
            $html .= '<li>
              <a style="'.$packagelist[$child->package_id].'"  href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=right" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
              </li>';
          }
        }
      }
      $html .= '</ul>';
    }
    return $html;
  }
?>
<style>
  @media screen and (max-width: 580px){

  .binary-tree-responsive { overflow: auto !important; }
  }
</style>
<div class="binary-tree-responsive">
<ul id="mainTree" style="overflow: auto !important;">
  <li>
    <a style="background:@if(isset($packagelist[$model->package_id])){{ $packagelist[$model->package_id] }}@endif "  href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" data-id="{{ $model->username }}"><i class="md md-accessibility"></i>{{$model->username}}</a>
    @if ($children = $repo->findChildren($model)) <!-- render 4 levels -->
      <ul>
        @if (count($children) <= 0)
          <li>
            <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
          </li>
          <li>
            <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=right' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
          </li>
        @else
          @foreach ($children as $child)
            @if (count($children) < 2 && $child->position == 'right')
              <li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
              </li>
            @endif
            <li>
              <a style="background:{{ $packagelist[$child->package_id] }} " href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" data-id="{{ $child->username }}"><i class="md md-accessibility"></i> {{$child->username}} ({{ strtoupper($child->position[0]) }})</a>
              {!! renderTree($child, 0, '', $packagelist) !!}
            </li>
            @if (count($children) < 2 && $child->position == 'left')
              <li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=right' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
              </li>
            @endif
          @endforeach
        @endif
      </ul>
    @endif
  </li>
</ul>
</div>
</div>
