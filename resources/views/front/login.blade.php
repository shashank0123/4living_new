<?php

    $captcha = rand(100000,888888);
?>
<!--@extends('front.app_living')-->
@extends('layouts.ecommerce2')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')

<!-- header end here -->
<div class=" mt-30 mb-30">
   <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
</div>


<style type="text/css">
  #loginNew .heading-ul{
    background-color: green;
    height: 45px;
}

#loginNew  ul{
    list-style-type: none;
    background-color: green;
    margin-left: 40%;
}
#loginNew  ul li a,
#loginNew  ul li{
    float: left;
    margin-right: 5px;
    color: #fff;
    text-align: center;
}

#loginNew  ul li:hover{
    background-color: #71ad71
}

.hr-top{
    height: 1px;
     width: 100%;
    background-color: #ccc;
    margin: 3px 0;
}

#loginNew table{
    margin-top: 45px;
}
#loginNew .table tr{
    margin-top: 5px;
    background-color: #cafdca;
}
.table thead th ,
.table tbody td {
    vertical-align: bottom;
    border-bottom: 2px solid #fff !important;
}

.table tbody td {
    border-left: 2px solid #fff !important;
}

.td-left{
    width: 46%;
    text-align: right;
}

.td-right{
    width: 50%;
    text-align: left;
}
.td-center{
    width: 5%;
    text-align: center;
}

#loginNew input{
    height: 25px !important;
    background-color: #fff;
    border-radius: 5px;
    font-size: 14px;
    width: 250px !important;
    border: 1px solid #999;

}

.full-td{
    text-align: center;
}
.btn-danger{
    background-color: red;
    padding:5px 20px ;
    border-radius: 5px;
}

.full-td .captcha_code{
     border: 1px solid #fff; padding: 7px 10px; width: 125px;
     letter-spacing: 5px;
     font-weight: bold;
     font-size: 16px;
     border-radius: 5px;
     background-color: #fff;
}
</style>


<div class="login-register-area pt-0 pb-90" id="loginNew">
    <div class="hr-top"></div>
    <div class="heading-ul">
        <ul>
            <li>Member &nbsp;&nbsp;</li>
            <li><a class="active" data-toggle="tab" href="#lg1">
            Login</a> &nbsp;/ </li>
            <li>
            <a  href="/register" style="color:blue">Register</a></li>
        </ul>

    </div>
    <div class="hr-top"></div>
      <form class="form-floating action-form" http-type="post" id="form" data-url="{{ route('login.post', ['lang' => \App::getLocale()]) }}">
    <table class="table" >

        <thead>
            <tr>
                <th colspan='13'>
                </th>
            </tr>
        </thead>


        <tbody>
          
                
                <tr>
                    <td class="td-left">Login ID</td>
                    <td class="td-center">:</td>
                    <td class="td-right"><input type="text" style="text-transform: uppercase;" name="username"  id="username" placeholder="Member ID" required="" style="width: 250px"></td>
                </tr>

                <tr>
                    <td class="td-left">Password</td>
                    <td class="td-center">:</td>
                    <td class="td-right">
                        <input type="password" name="password" id="password" required="" placeholder="PASSWORD" style="width: 250px;">
                    </td>
                </tr>

                <tr>
                    <td colspan='13' class="full-td"><span class="captcha_code">{{ $captcha }}</span></td>
                </tr>

                <tr>
                    <td class="td-left">Captcha</td>
                    <td class="td-center">:</td>
                    <td class="td-right"><input type="text" size="6" maxlength="6" name="captcha" id="captcha" value="" placeholder='Enter captcha code here'></td>
                </tr>

                <tr>
                    <td colspan='13'></td>
                </tr>

                <tr>
                    <td colspan='13' class="full-td">
                            <button type="button"  class="btn-danger" onclick="checkCaptcha()">
                                          <span class="btn-preloader">
                                            <i class="md md-cached md-spin"></i>
                                        </span>
                                        @lang('login.title')
                                    </button>

                                    <button type="submit" style="display: none" id="login-click">
                                      <span class="btn-preloader">
                                        <i class="md md-cached md-spin"></i>
                                    </span>
                                    @lang('login.title')
                                </button>
                    </td>
                </tr>

                <tr>
                    <td colspan="13"></td>
                </tr>

                <tr>
                    <td colspan="13" class="full-td">
                        <a href="/passwordReset"><b>Forgot / Reset Password</b></a>
                    </td>
                </tr>

                <tr>
                    <td colspan="13"></td>
                </tr>

                 <tr>
                    <td colspan="13" class="full-td">
                        Don't have an account ? <a href="{{ url('register') }}"><b style="color: blue;">Create Here</b></a>
                    </td>
                </tr>

                <tr>
                    <td colspan="13"></td>
                </tr>
            
        </tbody>

    </table>
</form>
</div>











<!-- 

<div class="login-register-area pt-85 pb-90">
    <div class="container">



        <div class="row">
            <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                <div class="login-register-wrapper">
                    <div class="login-register-tab-list nav">
                        <a class="active" data-toggle="tab" href="#lg1">
                            <h4> login </h4>
                        </a>
                        <a  href="/register">
                            <h4> register </h4>
                        </a>
                    </div>
                    <div class="tab-content">
                        <div id="lg1" class="tab-pane active">
                            <div class="login-form-container">
                                <div class="login-register-form">
                                  <form class="form-floating action-form" http-type="post" id="form" data-url="{{ route('login.post', ['lang' => \App::getLocale()]) }}">
                                    <label>Login ID</label>

                                    <input type="text" style="text-transform: uppercase;" name="username" class="form-control" id="username" placeholder="Member ID" required="">

                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" id="password" required="" placeholder="PASSWORD">

                                    <p><img src="/capcha.php" width="120" height="30" border="1" alt="CAPTCHA"></p>
                                    <p><input type="text" size="6" maxlength="5" name="captcha" id="captcha" value="" placeholder='Enter captcha code here'><br>
                                        <small>copy the digits from the image into this box</small></p>		

                                        <div class="button-box">
                                            <div class="login-toggle-btn">
                                             <input type="checkbox" name="remember">  <label>@lang('login.remember') </label>

                                             <a href="/passwordReset">Forgot Password?</a>
                                         </div>
                                         <button type="button" onclick="checkCaptcha()">
                                          <span class="btn-preloader">
                                            <i class="md md-cached md-spin"></i>
                                        </span>
                                        @lang('login.title')
                                    </button>

                                    <button type="submit" style="display: none" id="login-click">
                                      <span class="btn-preloader">
                                        <i class="md md-cached md-spin"></i>
                                    </span>
                                    @lang('login.title')
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div> -->

<script>
    function checkCaptcha(){

        var captcha = $('#captcha').val();
        var CSRF_TOKEN = $('meta[name="_t"]').attr('content');
            // alert(CSRF_TOKEN);
            
            if(captcha == ""){
                alert('First enter the captcha code');
            }
            else{
                $('#login-click').click();
            // $.ajax({
            //   type:'POST',
            //   url:'/verify-captcha',
            //   data:{_token: CSRF_TOKEN, captcha : captcha},
            //   success:function(data) {
            //       $('#login-click').click();
            //   }
            // });
        }
    }
</script>

@stop
