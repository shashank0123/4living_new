<?php
use App\Models\MemberDetail;
use App\Models\Member;
?>

@extends('front.app')
@section('title')
@lang('registerHistory.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">{{$side}} @lang('breadcrumbs.team')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
 .table {max- width:90% !important;}
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
          
          <div class="page-header">
          <div class="row">
              <div class="col-sm-6">                
          <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> {{ $side }} Member</h1>
          <p class="lead">@if($side!='My'){{'YOUR'}}@endif <?php echo $side; ?> MEMBERS REGISTRATION HISTORY.</p>
              </div>
              <div class="col-sm-6" style="text-align: right;">
                <form method="get" action="/en/my-members">
                  <input type="hidden" name="side" value="{{$side}}">
              <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
              <button type="submit" class="btn btn-primary" >Search</button>
            </form>
              </div>
          </div>
        </div>         
          
          
        
        <div class="card">
          <div>
            <div class="">
             <table class="table table-full" data-url="{{ route('member.registerleftHistoryList') }}">
              <thead>
                <tr>
                  <th>@lang('registerHistory.sr')</th>
                  <th>@lang('registerHistory.join')</th>
                  <th>@lang('registerHistory.name')</th>
                  <th>@lang('registerHistory.member_id')</th>
                  
                  @if($side == 'My')
                  <th>@lang('registerHistory.mobile')</th>
                  @endif
                  <th>Sponsor By</th>
                  <th>Status</th>
                  <!--<th>@lang('registerHistory.package')</th>-->
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                
                @if(!empty($data))
                @foreach ($data as $datas)
                <?php
                $detail = MemberDetail::where('member_id',$datas->id)->first();
                $sponsor = Member::where('id',$datas->parent_id)->first();
                ?>
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$datas->created_at}}</td>
                  <td>{{$datas->name}}</td>
                  <td>{{$datas->referral_id}}</td>
                  
                  @if($side == 'My')
                  <td>@if(!empty($detail->mobile_phone)){{$detail->mobile_phone}}@endif</td>
                  @endif
                  <td>
                     @if(!empty($sponsor))
                         @if(!empty($sponsor->username))
                         {{$sponsor->username}}
                         @endif
                         @endif
                         </td>
                         <td>@if ($datas->package_id == 1) {{'Inactive'}} @else {{ 'Active' }} @endif</td>
                  <!--<td>{{$datas->package_amount}}</td>-->
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>

            @if($i == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
<script>
    function getvalue(){
        var search= $('#search').val();
        alert(search);
    }
</script>
@stop
