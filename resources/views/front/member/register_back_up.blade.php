<?php
use App\Models\Package;
use App\Models\Epoint;
$packages = Package::all();
$epoints = Epoint::where('member_id', $member->id)->first();
?>

@extends('front.app')

@section('title')
@lang('register.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.hierarchy')</a></li>
  <li class="active">@lang('breadcrumbs.register')</li>
</ul>
@stop

@section('content')
<style>
  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-person-add"></i> @lang('register.title')</h1>
          {{--  <p class="lead">@lang('register.notice')</p> --}}
        </div>

        <div class="row m-b-40">
          <div class="col-md-2">
            <!--<div class="row">-->
            <!--  <div class="col-xs-6 col-md-12">-->
            <!--    <label>@lang('common.register')</label>-->
            <!--    <h2 class="theme-text" style="margin-top:0;"><span data-count data-start="0.00" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h2>-->
            <!--  </div>                -->
            <!--</div>-->
          </div>

          <div class="col-md-6">
            <div class="well" style="background-color: #fff">
              <form class="action-form" data-parsley-validate="" role="form" id="registerForm" http-type="post" data-url="{{ route('member.postRegister') }}" data-nationality="true">
                <fieldset>
                  


                  <div class="form-group">
                    <label class="control-label" for="inputUpline">@lang('register.direct')</label>
                    <input type="text" class="form-control" required="" name="direct_id" id="inputDirect" onchange="getUserDetail()">
                    <span class="help-block">
                      {{-- <span class="help-block">
                        <div class="btn-group">
                          <a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}" target="_blank" class="btn btn-primary"><span class="md md-swap-vert"></span> @lang('register.uplineLink')</a>
                          <button type="button" class="btn btn-success btn-show" data-toggle="modal" data-target="#showModal" data-url="{{ route('member.showModal', ['lang' => \App::getLocale()]) }}">
                            <span class="md md-help"></span> @lang('register.checkID')
                          </button>
                        </div>
                      </span> --}}
                    </div>                    

                    <div class="form-group" id="member-detail">
                      <div class="container">
                        <div class="row" style="color: #000; font-weight: bold; border-bottom: 1px solid #fff">
                          &nbsp;&nbsp;&nbsp;&nbsp;<span id="uname" style="color: #000; font-weight: normal;"></span><br>
                          {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Email : <span id="uphone">1234</span><br> --}}

                          {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Contact : <span id="uemail">@def.com</span> --}}
                        </div>
                      </div>
                    </div>



                    <div class="form-group">
                      <label class="control-label">@lang('register.name')</label>
                      <input type="text" name="name" class="form-control" required="">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('register.email')</label>
                      <input type="email" name="email" class="form-control" required="">
                    </div>
					 <div class="form-group">
                      <label class="control-label">@lang('register.pan')</label>
                      <input type="pan" name="pan" class="form-control" required="">
                    </div>
				
                    <div class="form-group">
                      <label class="control-label" for="inputMobile">@lang('register.mobile')</label>
                      <input type="text" class="form-control" required="" name="mobile_phone" id="inputMobile">
                    </div>
					 
					  
					  <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('register.password')</label>
                      <input type="password" id="inputPassword" name="password" class="form-control" required="" minlength="5" >
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('register.repassword')</label>
                      <input type="password" data-parsley-equalto="#inputPassword" class="form-control" required="" minlength="5" >
                    </div>
					 <div class="form-group">
                      <label class="control-label">@lang('register.address')</label>
                      <input type="address" name="address" class="form-control" required="">
                    </div>
					<div class="form-group">
                      <label class="control-label">@lang('register.city')</label>
                      <input type="city" name="city" class="form-control" required="">
                    </div>
					<div class="form-group">
                      <label class="control-label">@lang('register.pincode')</label>
                      <input type="pincode" name="pincode" class="form-control" required="">
                    </div>
					
                    <div class="form-group">
                      <label class="control-label" for="inputState">@lang('register.state')</label>
                      <select name="state" class="form-control" id="inputState" required="">
                        <option value="">Select State</option>
                        <option value="AP">Andhra Pradesh</option>
                        <option value="AR">Arunachal Pradesh</option>
                        <option value="AS">Assam</option>
                        <option value="BH">Bihar</option>
                        <option value="CT">Chhattisgarh</option>
                        <option value="DL">Delhi</option>
                        <option value="GA">Goa</option>
                        <option value="GJ">Gujarat</option>
                        <option value="HR">Haryana</option>
                        <option value="HP">Himachal Pradesh</option>
                        <option value="JK">Jammu and Kashmir</option>
                        <option value="JH">Jharkhand</option>
                        <option value="KA">Karnataka</option>
                        <option value="KL">Kerala</option>
                        <option value="MP">Madhya Pradesh</option>
                        <option value="MH">Maharashtra</option>
                        <option value="MN">Manipur</option>
                        <option value="ML">Meghalaya</option>
                        <option value="MZ">Mizoram</option>
                        <option value="NL">Nagaland</option>
                        <option value="OR">Orissa</option>
                        <option value="Pondicherry">Pondicherry</option>
                        <option value="PB">Punjab</option>
                        <option value="RJ">Rajasthan</option>
                        <option value="SK">Sikkim</option>
                        <option value="TN">Tamil Nadu</option>
                        <option value="TR">Tripura</option>
                        <option value="UK">Uttaranchal</option>
                        <option value="UP">Uttar Pradesh</option>
                        <option value="WB">West Bengal</option>
                      </select>
                    </div>

                    <?php $countries = config('misc.countries'); ksort($countries); ?>
                   <!-- <div class="form-group">
                      <label class="control-label" for="inputNationality">@lang('register.nationality')</label>
                      <select class="form-control dd-icon" name="nationality" id="inputNationality">
                        @foreach ($countries as $country => $value)
                          <option value="{{ $country }}" data-imagesrc="{{ asset('assets/img/flags/' . $country . '.png') }}" data-description="{{ \Lang::get('country.selected') }}">@lang('country.' . $country)</option>
                        @endforeach
                      </select>
                    </div>-->

                  

				  {{--<div class="form-group">
                      <label class="control-label" for="inputTPassword">@lang('register.tpassword')</label>
                      <input type="password" id="inputTPassword" name="transaction" class="form-control" required="" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('register.retpassword')</label>
                      <input type="password" data-parsley-equalto="#inputTPassword" class="form-control" required="" minlength="5">
				  </div> --}}

                    

                    
                    <!--<div class="form-group">
                      <label class="control-label" for="inputPoint">@lang('register.registerPoint')</label>
                      <div class="input-group">
                        <select class="form-control" name="point_amount" id="inputPoint">
                          @for ($i=0; $i<=50; $i+=10)
                            <option value="{{ $i }}">{{ $i }}</option>
                          @endfor
                        </select>
                        <span class="input-group-addon">%</span>
                      </div>
                      <span class="help-block">
                        <p style="margin-bottom:5px;"><small>@lang('register.registerNeed') <span class="theme-text" id="registerPointValue"></span></small></p>
                        <p><small>@lang('register.promoNeed') <span class="theme-text" id="promotionPointValue"></span></small></p>
                      </span>
                    </div>-->

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputS">@lang('register.security')</label>
                      <input type="password" class="form-control" required="" name="s" id="inputS">
                    </div> --}}

                    <!--<div class="form-group">-->
                    <!--  <div class="checkbox">-->
                    <!--    <label>-->
                    <!--      <input type="checkbox" required="" name="terms"> @lang('register.agree')-->
                    <!--    </label>-->
                    <!--  </div>-->
                    <!--</div>-->

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">
            <span class="md md-accessibility"></span> @lang('register.modal.title')
          </h4>
        </div>
        <div class="modal-body">
          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">@lang('common.modal.load')</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">@lang('common.modal.error')</small>
          </div>

          <div id="modalContent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>
  <script>
    function getUserDetail(){
      var name = $('#inputDirect').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Success'){
            $('#member-detail').show();
            $('#uname').html("Name : "+msg.name);
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }
        }
      });
    }

    function checkExistance(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-availability",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Username already exists. Try another'){

            $('#check-availability').show();
            $('#check-availability').html(msg.message);
          }
          else
            $('#check-availability').hide();
        }
      });
    }

  </script>
  @stop
