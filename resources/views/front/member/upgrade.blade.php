<?php
use App\Models\Package;
use App\Models\Epoint;
$packages = Package::get();
$epoints = Epoint::where('member_id', $member->id)->first();

?>

@extends('front.app')

@section('title')
@lang('upgrade.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.upgrade')</li>
</ul>
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
  #user-name { display: none; }
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-person-add"></i> @lang('upgrade.title')</h1>
          {{-- <p class="lead">@lang('upgrade.subTitle')</p> --}}
        </div>

        <div class="row m-b-40">
          <div class="col-md-2">
            <div class="row">
              <div class="col-xs-6 col-md-12">
                <!--<label>Funds</label>-->
                <!--<h2 class="theme-text" style="margin-top:0;"><span data-count data-start="0.00" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h2>-->
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="well" style="background-color: #fff">
              <form class="action-form" data-parsley-validate="" role="form" http-type="post" data-url="{{ route('member.postUpgrade') }}">
                <fieldset>

                  <div class="form-group">
                    <label class="control-label" for="inputPackage">@lang('upgrade.memberid')</label>
                    <div class="input-group">
                      <input type="text" name="username" id="username"  onchange="getUserDetail()" class="form-control">                        
                    </div>
                  </div>

                  <div class="form-control" id="user-name" style="background-color: #fff">
                    <p id="member-detail"></p>
                  </div>

                  <div class="form-group">
                    <br>
                    <label class="control-label" for="inputPackage">@lang('upgrade.epin')</label>
                    <div class="input-group">
                        <select class="form-control" name="package_id" id="inputPackage" required=""style="width: 100%">
                            <option>Select</option>
                        </select>
                      <!--<select class="form-control" name="package_id" id="inputPackage" required="">-->
                      <!--  @if (count($packages) > 0)-->
                      <!--  @foreach ($packages as $package)-->
                      <!--  <option value="{{ $package->id }}">{{ number_format($package->package_amount, 0) }} @if ($member->package_amount == $package->package_amount) (@lang('upgrade.renew')) @endif</option>-->
                      <!--  @endforeach-->
                      <!--  @endif-->
                      <!--</select>-->
                      <!--<span class="input-group-addon">INR</span>-->
                    </div>
                  </div>

                  {{-- <div class="form-group">
                    <label class="control-label" for="inputS">@lang('register.tpassword')</label>
                    <input type="password" class="form-control" required="" name="s" id="inputS">
                  </div> --}}

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span>@lang('common.submit')</span>
                    </button>
                    <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>

<script>
  function getUserDetail(){
    var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          // alert(msg);
          if(msg.message == 'Success'){
            $('#user-name').show();
            if(msg.name != null){
              $('#member-detail').html("Name : "+msg.name);
            }
            else{
              $('#member-detail').html("Information of this User ID is not Stored.");
            }
            
          }
          else{
            $('#uname').html(msg.message);
          }          
        }
      });
    }

    function getvalue(){
      var value=$('#selet-package').val();
      $('#package_id').val(value);
    }

    function getPayment(){
      var value = $('#package_id').val();
      window.location.href = "/register/membership-fee/{{$member->user_id}}/"+value;
    }
  </script>



  @stop
