<?php
use App\Models\MemberDetail;
use App\Models\Member;
?>

@extends('front.app')
@section('title')
@lang('registerHistory.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">{{$side}} @lang('breadcrumbs.team')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
 .table {max- width:90% !important;}
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
          
          <div class="page-header">
          <div class="row">
              <div class="col-sm-6">                
          <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> {{ $side }} Member</h1>
          <p class="lead">@if($side!='My'){{'YOUR'}}@endif <?php echo $side; ?> MEMBERS REGISTRATION HISTORY.</p>
              </div>
              <div class="col-sm-6" style="text-align: right;">
                <form method="get" action="/en/my-members">
                  <input type="hidden" name="side" value="{{$side}}">
              <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
              <button type="submit" class="btn btn-primary" >Search</button>
            </form>
              </div>
          </div>
        </div>         
          
          
        
        <div class="card">
          <div>
            <div class="">
             <table class="table table-full" data-url="{{ route('member.registerleftHistoryList') }}">
              <thead>
                <tr>
                  <th>Sr no</th>
                  <th>Level</th>
                  <th>No of ID</th>
                  <th>Month</th>
                  <th>Total</th>
                  <th>TDS</th>
                  <th>Transfer Payment</th>
                  
                  <!--<th>@lang('registerHistory.package')</th>-->
                </tr>
              </thead>
              <tbody>
                <?php $i=1; $sum = 0; ?>
                
                @if(!empty($rowsdata))
                <tr>
                  <td>{{$i++}}</td><td>Level 1</td><td>{{ $rowsdata['level1c'] }}</td><td>June</td><td>{{ $rowsdata['level1'] }}</td><td>{{ $rowsdata['level1']*0.1 }}</td><td>{{ $rowsdata['level1']*0.1 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 2</td><td>{{ $rowsdata['level2c'] }}</td><td>June</td><td>{{ $rowsdata['level2'] }}</td><td>{{ $rowsdata['level2']*0.1 }}</td><td>{{ $rowsdata['level2']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 3</td><td>{{ $rowsdata['level3c'] }}</td><td>June</td><td>{{ $rowsdata['level3'] }}</td><td>{{ $rowsdata['level3']*0.1 }}</td><td>{{ $rowsdata['level3']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 4</td><td>{{ $rowsdata['level4c'] }}</td><td>June</td><td>{{ $rowsdata['level4'] }}</td><td>{{ $rowsdata['level4']*0.1 }}</td><td>{{ $rowsdata['level4']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 5</td><td>{{ $rowsdata['level5c'] }}</td><td>June</td><td>{{ $rowsdata['level5'] }}</td><td>{{ $rowsdata['level5']*0.1 }}</td><td>{{ $rowsdata['level5']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 6</td><td>{{ $rowsdata['level6c'] }}</td><td>June</td><td>{{ $rowsdata['level6'] }}</td><td>{{ $rowsdata['level6']*0.1 }}</td<td>{{ $rowsdata['level6']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 7</td><td>{{ $rowsdata['level7c'] }}</td><td>June</td><td>{{ $rowsdata['level7'] }}</td><td>{{ $rowsdata['level7']*0.1 }}</td><td>{{ $rowsdata['level7']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 8</td><td>{{ $rowsdata['level8c'] }}</td><td>June</td><td>{{ $rowsdata['level8'] }}</td><td>{{ $rowsdata['level8']*0.1 }}</td><td>{{ $rowsdata['level8']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 9</td><td>{{ $rowsdata['level9c'] }}</td><td>June</td><td>{{ $rowsdata['level9'] }}</td><td>{{ $rowsdata['level9']*0.1 }}</td><td>{{ $rowsdata['level9']*0.9 }}</td>
                </tr>
                <tr>
                  <td>{{$i++}}</td><td>Level 10</td><td>{{ $rowsdata['level10c'] }}</td><td>June</td><td>{{ $rowsdata['level10'] }}</td><td>{{ $rowsdata['level10']*0.1 }}</td><td>{{ $rowsdata['level10']*0.9 }}</td>
                </tr>
                 
                @endif
              </tbody>
            </table>
             <div class="card">
          <div>
            <div class="">
             <table class="table table-full" >
              <thead>
                <tr>
                  <th>Sr no</th>
                  <th>Royality</th>
                  
                  
                  <!--<th>@lang('registerHistory.package')</th>-->
                </tr>
              </thead>
              <tbody>
                  <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
                  </tbody>
             </table>
            

            @if($i == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
<script>
    function getvalue(){
        var search= $('#search').val();
        alert(search);
    }
</script>
@stop
