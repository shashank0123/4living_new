<?php
use App\Models\Package;
use App\Models\Epoint;
// $packages = Package::all();
$packages = Package::where('package_amount','>',0)->where('package_amount','<',10000)->get();
$epoints = Epoint::where('member_id', $member->id)->first();
?>


@extends('front.app')

@section('title')
@lang('register.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.hierarchy')</a></li>
  <li class="active">@lang('breadcrumbs.register')</li>
</ul>
@stop

@section('content')
<style>
  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>

<style>
  .package_list li h3{
    font-weight: bold;
    padding: 1%;
    margin-top: 15px;

  }

  .member_text{
    /*margin: 1% ;*/
    width: 100%;
    padding: 10px;
    background-color: #1b4f82 !important;
    color: #fff;
  }

  .package_list li{
    background-color: #fff;
    height: 210px;
    float: left;
    padding: 7px;
    margin: 10px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    text-align: center;
  }

  /*.packages-section{

    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    width: 100%;
  }
  */
  .package_list li:hover{

    zoom: 1.02;
  }

 /* input[type="radio"]{
    height: 25px; width: auto;
  }
*/
  .select_package{
    margin-top: 35px;
    text-align: center;
    padding: 10px;
    margin-left: 32px;
    background-color: #1b4f82 !important;
    color: #fff;
    width: fit-content;
  }

  .package_list li a {
    margin-top: 10px;
    font-weight: bold;

  }

  #packages h2{
    text-align: center;
    color: #333333;
    font-weight: bold;
    margin-bottom: 45px;
  }



  .membership{
    padding: 35px;
    font-size: 28px !important;
    text-align: center;
    color: #333333;
    font-weight: bold;
  }
  .form-control{
    margin-bottom: 25px;
    height: 35px !important;   
  }
  label{
    color: #777777;
    font-weight: bold;
  }
  .family_detail { 
    display: none;
  }

  .well{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    padding: 45px;
    background-color: #fff !important;


  }

  input::placeholder, select{
    font-size: 12px !important;
  }
</style>



<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-person-add"></i> @lang('register.title')</h1>
          {{--  <p class="lead">@lang('register.notice')</p> --}}
        </div>

        <div class="row m-b-40">
          <!-- <div class="col-md-2"> -->
            <!--<div class="row">-->
            <!--  <div class="col-xs-6 col-md-12">-->
            <!--    <label>@lang('common.register')</label>-->
            <!--    <h2 class="theme-text" style="margin-top:0;"><span data-count data-start="0.00" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h2>-->
            <!--  </div>                -->
            <!--</div>-->
          <!-- </div> -->

          <div class="col-md-12">
            <div class="well" style="background-color: transparent">
             <form class="action-form" data-parsley-validate="" role="form" id="registerForm" http-type="post" data-url="{{ route('member.postRegister') }}" data-nationality="true">

                <div class="card-content">
                  <div class="m-b-30">
                    <!-- <div class="card-title strong black-text" style="font-weight: bold;text-transform: uppercase !important;">Registration Form - Become a member</div> -->
                  </div>


                  <div class="membership_section">
                    @if(!empty($packages))
                    <div class="form-group" id="packages">
                      <h2>MEMBERSHIP</h2>
                      <div class="row">
                        <ul class="package_list">
                          @foreach($packages as $package)
                          <li>
                            <div class="member_text">Member Registration</div>
                            <h3> Rs. {{number_format((float)$package->package_amount, 0, '.', '')}} /- </h3>
                            <div class="select_package">
                              <span><input type="radio" name="package_id" id="package_id" value="{{$package->id ?? ''}}" required="required">&nbsp;&nbsp;&nbsp;&nbsp;Select Now</span></div>
                            <a href="{{ url('products_detail/'.$package->id) }}"><u>PD</u></a>
                          </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                    @endif


                    <h2 class="membership">MEMBERSHIP  FORM</h2>



                    <div class="row">
                      <div class="col-sm-12">
                        <label for="referal_id">Sponsor ID</label>
                        <input type="text" style="text-transform: uppercase;" @if (isset($_GET['referal_id'])) {{'readonly'}} @endif value="@if (isset($_GET['referal_id'])) {{$_GET['referal_id']}} @endif" name="direct_id" class="form-control" id="inputDirect" required="required" onchange = "getUserDetail()" placeholder="Enter Sponsor ID Here">
                      </div>

                       <div class="col-sm-12" id="member-detail">
                      <div class="container">
                        <div class="row" style="color: #000; font-weight: bold; border-bottom: 1px solid #fff">
                          &nbsp;&nbsp;&nbsp;&nbsp;<span id="uname" style="color: #000; font-weight: normal;"></span><br>
                          {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Email : <span id="uphone">1234</span><br> --}}

                          {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Contact : <span id="uemail">@def.com</span> --}}
                        </div>
                      </div>
<br>
                    </div>


                      <div class="col-sm-6">
                        <label>Pan  Number *</label>
                        <input type="text" name="pan" class="form-control" id="pan" required="required" Placeholder="Enter Pan Card Number Here">
                      </div>
                      <div class="col-sm-6">
                        <label>Upload Passport Size Photo </label>  
                        <input type="file" name="profimage" class="form-control" id="profimage" Placeholder = "Upload Profile Pic">  
                      </div>
                      <div class="col-sm-6">
                        <label>Name Mr./Mrs./Miss. *</label>   
                        <div class="row"> 
                          <div class="col-sm-3">  
                            <select name="gender_type" id="gender_type" class="form-control" required="required">
                              <option>Select</option>
                              <option value="Mr">Mr</option>
                              <option value="Mrs">Mrs</option>
                              <option value="Miss">Miss</option>
                            </select>
                          </div>
                          <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" id="fullname" required="required" placeholder="Enter Name Here">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label>Father Name / Husband Name *</label>
                        <div class="row">
                          <div class="col-sm-3">
                            <select name="fh_type" id="fh_type" class="form-control" required="required">
                              <option> Select</option>
                              <option value="Father">Father</option>
                              <option value="Husband">Husband</option>
                            </select>
                          </div>
                          <div class="col-sm-9">
                            <input type="text" name="fh_name" class="form-control" id="fh_name" required="required" placeholder="Enter Name Here">
                          </div>
                        </div>


                      </div>


                      <div class="col-sm-3">
                        <label>D. O. B *</label>
                        <input type="date" name="date_of_birth" class="form-control" id="date_of_birth" placeholder="Enter DOB Here">

                      </div>
                      <div class="col-sm-3">
                        <label>Mobile/Telephone No. *</label>
                        <input type="text" name="mobile_phone" class="form-control" id="mobile_phone"  placeholder="Enter Mobile Number Here">
                      </div>
                      <div class="col-sm-3">
                        <label>Email *</label>
                        <input type="text" name="email" class="form-control" id="email"  placeholder="Enter Email Here">
                      </div>
                      <div class="col-sm-3">
                        <label>Martial Status *</label>
                        <select name="marital_status" id="marital_status" class="form-control" >
                          <option value=""> Select Marital Status</option>
                          <option value="Married">Married</option>
                          <option value="Unmarried">Unmarried</option>
                        </select>
                      </div>



                      <div class="col-sm-12">
                        <label>Address *</label>
                        <input type="text" name="address" class="form-control" id="address" required="required" placeholder="Enter Address Here">
                      </div>

                      <div class="col-sm-4">
                        <label>City *</label>
                        <input type="text" name="city" class="form-control" id="city" required="required" placeholder="Enter City Here">
                      </div>
                      <div class="col-sm-4">
                        <label>State *</label>
                        <input type="text" name="state" class="form-control" id="state" required="required" placeholder="Enter State Here">
                      </div>
                      <div class="col-sm-4">
                        <label>Pin Code *</label>
                        <input type="text" name="pincode" class="form-control" id="pincode" required="required" placeholder="Enter Pin Code Here">
                      </div>

                      <div class="col-sm-6">
          <label for="password">Password</label>
          <!--<input type="password" name="password" class="form-control" id="password" required="required" minlength="5" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">-->
          <input type="password" name="password" class="form-control" id="password" required="required" minlength="5" >
          <!--<p>First letter Capital, atleast one symbol , one lower letter and numberic value</p>-->
        </div>

        <div class="col-sm-6">
          <label for="password">Confirm Password</label>
          <input type="password" name="cpassword" class="form-control" id="cpassword" required="required" >
          <!--<input type="password" name="cpassword" class="form-control" id="cpassword" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">-->
        </div>


                      <div class="col-sm-4">
                        <label>Nominee *</label>
                        <select name="family_member" id="family_member" class="form-control" required="required" onclick="getFamily()">
                          <option value="">Select</option>
                          <option value="0">No</option>
                          <option value="1">Yes</option>
                        </select>
                      </div>
                    </div>





                    <div class="family_detail">


                      <h2 class="membership">DETAILS OF NOMINEE </h2>
                      <div id="familyDetail">
                        <div class="row">

                          <div class="col-sm-3">
                            <label>Name *</label>
                            <input type="text" name="member_name[]" class="form-control" id="member_name" placeholder="Enter Name  Here">
                          </div>
                          <div class="col-sm-3">
                            <label>Relation *</label>
                            <input type="text" name="member_relation[]" class="form-control" id="member_relation" placeholder="Enter Relation Here">
                          </div>
                          <div class="col-sm-3">
                            <label> Married / Unmarrid *</label>
                            <select name="member_marital_status[]" id="member_marital_status" class="form-control">
                              <option value="">Select Marital Status</option>
                              <option value="Married">Married</option>
                              <option value="Unmarried">Unmarried</option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label> D. O. B *</label>
                            <div class="row">
                              <div class="col-sm-8">
                                <input type="date" name="member_date_of_birth[]" class="form-control" id="member_date_of_birth" placeholder="Enter DOB Here">
                              </div>
                              <div class="col-sm-4">
                                <span class="btn btn-success" id="addFamily" onclick="getMoreFamily()">Add</span>
                              </div>
                            </div>

                          </div>

                        </div>
                      </div>
                    </div>



                    <h2 class="membership">OCCUPATION DETAIL</h2>
                    <div class="row">

                      <div class="col-sm-4">
                        <label>Occupation Name *</label>
                        <input type="text" name="occupation_name" class="form-control" id="occupation_name" required="required" placeholder="Enter Your Occupation">
                      </div>
                      <div class="col-sm-4">
                        <label>Occupation Details *</label>
                        <input type="text" name="occupation_address" class="form-control" id="occupation_address" required="required" placeholder="Enter Details Here Eg. Address">
                      </div>
                      <div class="col-sm-4">
                        <label>Type of Occupation *</label>
                        <input type="text" name="occupation_type" class="form-control" id="occupation_type" required="required" placeholder="Enter Occupation Type Here">
                      </div>
                      <input type="hidden" name="id_type" id="id_type" value="Red">

                    </div>














                    <div class="card-action clearfix">

 <button type="submit" style="font-weight: bold; color: #fff" class="btn btn-danger">@lang('login.savetitle')</button>
 
                      <div class="pull-left">
                        <span  onclick="showDiv()" class="btn btn-link black-text">
                          <span class="btn-preloader">
                            <i class="md md-cached md-spin"></i>
                          </span>
                         
                        </span>
                      </div>
                    </div>



                  </div>

                </div>


              </form>
              <br>
            </div>
<!-- 
            <div id="otpcontainer" class="container m-b-30" style="max-width: 100%;">
              <form id="otpform" class="form-floating action-form" http-type="post" data-url="{{ route('verifyotp', ['lang' => \App::getLocale()]) }}">
                <div class="card-content">
                  <div class="m-b-30">
                    <div class="card-title strong black-text" style="font-weight: bold;">Verify Your OTP</div>
                  </div>
                  <input type="hidden" name="username" id="emailvalue">
                  <input type="hidden" name="sponsoredId" id="sponsoredId">

                  <div class="form-group">
                    <label for="username">Enter OTP</label>
                    <input type="text" name="otp" class="form-control" id="otp" required="">
                  </div>
                </div>
                <input type="hidden" name="verify_otp" class="form-control" id="verify_otp" required="">

                <div class="card-action clearfix"> 
                  <div class="pull-right" >
                    <span id="verify" onclick="verifyOTP()" class="btn btn-link black-text">
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span style="font-weight: bold; color: #f00" >@lang('login.savetitle')</span>
                    </span>
                  </div>
                </div>
              </form>
              </div> -->
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">
            <span class="md md-accessibility"></span> @lang('register.modal.title')
          </h4>
        </div>
        <div class="modal-body">
          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">@lang('common.modal.load')</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">@lang('common.modal.error')</small>
          </div>

          <div id="modalContent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>
  <script>

  function removerField(id){
    $('#form'+id).hide();
  }

  var count_form = 1;
  function getMoreFamily(){
    var CSRF_TOKEN = $('meta[name="_t"]').attr('content');
    $.ajax({
      type: "post",
      url: "/getMoreFields",
      crossDomain: true,
      data:{ _token: CSRF_TOKEN, show:'page',id: count_form },       
      dataType: 'html',
      success:function(data){
        // alert('yes')
        $('#familyDetail').append(data);
        count_form++;

      },
      failure:function(data){
       // alert('no')
       $('#familyDetail').append(data);
     }
   });
  }



  function getFamily(){
    var data = $('#family_member').val();

    if(data == '1'){
      $('.family_detail').show();
    }
    else{
      $('.family_detail').hide();
    }
  }


    function getUserDetail(){
      var name = $('#inputDirect').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Success'){
            $('#member-detail').show();
            $('#uname').html("Name : "+msg.name);
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }
        }
      });
    }

    function checkExistance(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-availability",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Username already exists. Try another'){

            $('#check-availability').show();
            $('#check-availability').html(msg.message);
          }
          else
            $('#check-availability').hide();
        }
      });
    }

  </script>
  @stop
