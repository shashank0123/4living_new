<?php
use App\Models\MemberDetail;
use App\Models\Member;
?>

@extends('front.app')
@section('title')
@lang('registerHistory.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">{{$side}} @lang('breadcrumbs.team')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
 .table {max- width:90% !important;}
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
          
          <div class="page-header">
          <div class="row">
              <div class="col-sm-6">                
          <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> {{ $side }} Member</h1>
          <p class="lead">@if($side!='My'){{'YOUR'}}@endif <?php echo $side; ?> MEMBERS REGISTRATION HISTORY.</p>
              </div>
              <div class="col-sm-6" style="text-align: right;">
                <form method="get" action="/en/my-members">
                  <input type="hidden" name="side" value="{{$side}}">
              <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
              <button type="submit" class="btn btn-primary" >Search</button>
            </form>
              </div>
          </div>
        </div>         
          
          
        
        <div class="card">
          <div>
            <div class="">
             <table class="table table-full" data-url="{{ route('member.registerleftHistoryList') }}">
              <thead>
                <tr>
                  <th>Sr no</th>
                  <th>UserId</th>
                  <th>Name</th>
                  <th>Month</th>
                  <th>Total</th>
                  <th>TDS</th>
                  <th>Final</th>
                  <th>Status</th>
                  <th>Details</th>
                  <!--<th>@lang('registerHistory.package')</th>-->
                </tr>
              </thead>
              <tbody>
                <?php $i=1; $sum = 0; ?>
                
                @if(!empty($rowsdata))
                @foreach ($rowsdata as $datas)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$datas['userid']}}</td>
                  <td>{{$datas['name']}}</td>
                  <?php $sum = $datas['level1'];?>
                  <?php $sum += $datas['level2'];?>
                  <?php $sum += $datas['level3'];?>
                  <?php $sum += $datas['level4'];?>
                  <?php $sum += $datas['level5'];?>
                  <?php $sum += $datas['level6'];?>
                  <?php $sum += $datas['level7'];?>
                  <?php $sum += $datas['level8'];?>
                  <?php $sum += $datas['level9'];?>
                  <?php $sum += $datas['level10'];?>
                  <td>June</td>
                  <td>{{$sum}}</td>
                  <td>{{$sum*0.05}}</td>
                  <td>{{$sum*0.95}}</td>
                  <td>Due</td>
                  <td><a href="right-history?username={{$datas['userid']}}">Click Here</a></td>
                  
                 
                  
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>

            @if($i == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
<script>
    function getvalue(){
        var search= $('#search').val();
        alert(search);
    }
</script>
@stop
