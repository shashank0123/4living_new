@extends('layouts.ecommerce2')

@section('content')
<style type="text/css">
	.section-associate{
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		height: 400px;     
        padding: 15px;                                                                                                                    
	}
    .associate_content{
        padding: 20px;
    }
</style>
<div class=" mt-30 mb-30">
           <img class="img-responsive"  src="asset/images/bg/breadcrumb.jpg">
        </div>
<div class="container mt-40 mb-50">
    <h2 class="sechead">Associates</h2>
     <hr class="hrstyle">

     <div class="row">
     	@if(!empty($associates))


     	@foreach($associates as $associate)

     	<div class="col-sm-3">
            <div class=" section-associate">
     		<div class="associate_image" style="height: 200px; text-align: center;margin-bottom: 20px;">
                @if(!empty($associate->image))
     			<img src="{{ asset('images/partner/'.$associate->image) }}" style="width: auto; height: 200px;">
                @else
                <img src="{{ asset('images/partner/default.png') }}" style="width: auto; height: 200px;">
                @endif
     		</div>
     		<div class="associate_content">
     			<h3>{{ $associate->company_name ?? '' }}</h3>
     			<p>{{ $associate->address ?? '' }},{{ $associate->city ?? '' }},{{ $associate->state ?? '' }} ({{ $associate->pincode ?? '' }})</p>
     			<p>{{ $associate->website_link ?? '' }}</p>
     			<p>{{ $associate->company_email ?? '' }}</p>
     		</div>
        </div>
     	</div>

     	@endforeach


     	@endif
     </div>

 </div>
@endsection