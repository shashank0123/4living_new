
<?php

    $total = 0;

    $countCart = 0;
    if(!empty(session()->get('cart'))) {
        $countCart = count(session()->get('cart'));
    }

?>
<div >
                                <div class="shopping-cart-top">
                                    <h4>Your Cart</h4>
                                    <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                </div>
                                @if(!empty($cartProducts))
                                <ul>

                                  @foreach($cartProducts as $product)
                                    <li class="single-shopping-cart" id="mini{{$product->product_id}}">
                                        <div class="shopping-cart-img">
                                            <a href="{{ url($product->page_url ??'') }}"><img alt="" src="{{ asset($product->image ??'') }}"></a>
                                            <div class="item-close">
                                                <a href="{{ url($product->page_url ??'') }}"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4 style="font-size: 18px; font-weight: bold;"><a href="#">{{ $product->product_name ?? '' }}</a></h4>
                                            <span> Rs. {{ $product->price  ?? ''}} * {{$product->quantity}}</span>
                                            <!-- <p></p> -->
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a onclick="deleteCart({{ $product->product_id }})"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                    @php $total = $total + ($product->quantity * $product->price); @endphp
                                    @endforeach
                                   
                                </ul>
                                @else
                                <p> Cart Is Empty </p>
                                @endif
                                <div class="shopping-cart-bottom">
                                    <div class="shopping-cart-total">
                                        <h4>Subtotal <span class="shop-total" id="subtotal"> Rs. {{$total}}</span></h4>
                                    </div>
                                    <div class="shopping-cart-btn btn-hover default-btn text-center">
                                        <a class="black-color" href="@if($countCart>0){{ url('checkout-products') }} @endif">Continue to Checkout</a>
                                    </div>
                                </div>
                            </div>