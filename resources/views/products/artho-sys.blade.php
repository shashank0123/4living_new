@extends('layouts.ecommerce2')

<style type="text/css">
  .quantity {
    margin-bottom: 20px;
  }

  .quantity i {
    cursor: pointer;
  }
</style>

@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>

<div class="container-fluid mt-40 mb-50">
    <h2 class="sechead">Ayurvedic Medicines: ARTHO-SYS </h2>
     <hr class="hrstyle">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            
               <div class="img-hover-zoom img-hover-zoom--xyz pdsec1_1">
  <img src="../asset/images/product/3.jpg" alt="Another Image zoom-on-hover effect" width="300" height="350">
</div>
           
        </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="pdsec1_2">
               <h4 class="phead"> ARTHO-SYS</h4>
			   <hr class="hrstyle2">
			   <h4 class="phead"> MRP:  Rs. 1800/</h4><br>
         <p><b>Availability : </b> In Stock</p>
         
			   
               <ul class="pddtls">
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Effective and Safe Ayurveda Remedy for Joint Pains.</span></li>
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Reduces Edema of Joints & Joint Pains effectively.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Enhances Joint Mobility.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Reduces Muscular Spasm & Morning Stiffness.</span></li>
                                      

               </ul>

                 <div class="row">

                  <div class="col-sm-4 col-12">
                    <div class="quantity">

                 <i class="fa fa-minus" onclick="decrement()"></i> &nbsp;
                 
                 <input type="text" name="pro_quantity" id="quantity" value="1" min='1' style="width: 100px; text-align: center;"> &nbsp;

                 <i class="fa fa-plus" onclick="increment()"></i>

            
                
               </div>
                  </div>

                  <div class="col-sm-2 col-6" style="padding: 0!important">
                    <span class="btn btn-success" onclick="addToCart()">Add To Cart</span>
                  </div>
                  <div class="col-sm-2 col-6"  style="padding: 0!important">
                    <span class="btn btn-danger" onclick="buyNow()">Buy Now</span>
                    
                  </div>
               
             </div>
             <div class="alert alert-success" id="successCart" style="display: none;"></div>

            </div>
        </div>
        
    </div>

      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Introduction</h4>
               <hr class="hrstyle2">
               <p>Arthritis is a common disease which causes chronic pain and loss of movement because of damage to the joints of the body. Billions of people are suffering from Arthritis in India itself. These patients suffer from severe Joint Pains resulting limited joint movements. Dangerous Arthritis leads people gradually towards physical disability. However in allopathy, there are yet no drugs that have reliable treatment for Arthritis. Allopathic treatment is just symptomatic. Ayurveda believes that an imbalanced dosha (either vata,pitta or kapha) is the root cause of Joint Pains and also indicates effective herbal remedy for that. After a wide research on Ayurvedic Science regarding joint pains, 4Living has launched Artho Rich, a perfect Ayurvedic formula to fight against any joint disorder.</p>
            
        </div>
      </div>
       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> There are over 100 different forms of arthritis. The most common forms are</h4>
               <hr class="hrstyle2">
               <ol>
                   <li> Rheumatoid arthritis</li>
                    <li>Psoriatic arthritis</li>
                     <li>Autoimmune diseases</li>
                      <li>Septic arthritis etc.</li>

               </ol>
            
        </div>
      </div>


       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">

             <h4 class="phead">Signs and Symptoms</h4>
              <hr class="hrstyle2">
            <p>Regardless of the type of arthritis, the common symptoms for all arthritis disorders include varied levels of pain, swelling, joint stiffness and sometimes a constant ache around the joint(s).</p>
           
               
               <ol><li> Inability to use the hand or walk</li>
    <li> Malaise and a feeling of tiredness</li>
    <li> Fever</li>
    <li> Weight loss</li>
    <li> Poor sleep</li>
    <li> Muscle aches and pains</li>
    <li> Tenderness</li>
    <li> Difficulty moving the joint</li>
               </ol>
            
        </div>
      </div>


<div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro4">

             <h4 class="phead">Ingredient</h4>
              <hr class="hrstyle2">
         
           
               
               <ol><li>Rasnasaptakam Kwath</li>
				<li>Navjeevan Rasa</li>
					<li>Tinospora Cordifolia</li>
						<li>Curcuma Amada Roch</li>
							<li>Withania Somnifera Dunal</li>
								<li>Gum Acasia</li>
									<li>Starch</li>
									
               </ol>
          
        </div>
      </div>


      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro5">

             <h4 class="phead">Benefits of Capsule Artho Sys</h4>
              <hr class="hrstyle2">
          
           
               
               <ul>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>	Reduces Oedema of joints & Joint Pains effectively</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Increases joint mobility.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Reduces muscular spam</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>	Reduces morning stiffness.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Validate by blood testing.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Effective & safe drug.
</li>
    
               </ul>
            
        </div>
      </div>

       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro6">

             <h4 class="phead">Dosage</h4>
              <hr class="hrstyle2">
          
           
               
               <ol>
    <li><b>ARTHO-SYS:</b> &nbsp;  2 to 3 Tablets in a day. after eating food.</li>
   
               </ol>
            
        </div>
      </div>

    
</div>
@include('products.products-list');
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection





@section('scripts')

<script type="text/javascript">

function increment(){
  var qty = $('#quantity').val();
  qty = parseInt(qty)+1;

  $('#quantity').val(qty);
}

function decrement(){
  var qty = $('#quantity').val();
  if(qty>1)
  qty = parseInt(qty)-1;

  $('#quantity').val(qty);
}

  function addToCart(){
    var product_id = '4';
    var product = 'Artho Sys';
    var price = '1800';
    var quantity = $('#quantity').val();
    var image = 'asset/images/product/3.jpg';
    var page_url = 'product/artho-sys';

  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    /* the route pointing to the post function */
    url: '/add-to-cart',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, product_id: product_id, product: product, quantity: quantity, image: image, price: price, page_url: page_url},
    success: function (data) { 
      // window.location.href = '/en';
      $("#cartCount").text(data.countCart);
      $('#successCart').show();
      $("#successCart").text('Product successfully added to cart');
    }
  }); 

  }
  
   
  function buyNow(){
   var product_id = '4';
    var product = 'Artho Sys';
    var price = '1800';
    var quantity = $('#quantity').val();
    var image = 'asset/images/product/3.jpg';
    var page_url = 'product/artho-sys';
    
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    /* the route pointing to the post function */
    url: '/add-to-cart',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, product_id: product_id, product: product, quantity: quantity, image: image, price: price, page_url: page_url},
    success: function (data) { 
      // window.location.href = '/en';
      $("#cartCount").text(data.countCart);
    //   $('#successCart').show();
    //   $("#successCart").text('Product successfully added to cart');
      
      window.location.href='/checkout-products';
    }
  }); 

  }
</script>


@endsection