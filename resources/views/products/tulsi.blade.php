@extends('layouts.ecommerce2')

<style type="text/css">
  .quantity {
    margin-bottom: 20px;
  }

  .quantity i {
    cursor: pointer;
  }
</style>

@section('content')
<!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
<div class="container-fluid mt-40 mb-50">
    <h2 class="sechead">Ayurvedic Medicines: TULSI </h2>
     <hr class="hrstyle">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            
               <div class="img-hover-zoom img-hover-zoom--xyz pdsec1_1">
  <img src="../asset/images/product/2.jpg" alt="Another Image zoom-on-hover effect" width="300" height="350">
</div>
           
        </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="pdsec1_2">
               <h4 class="phead"> TULSI</h4>
               <hr class="hrstyle2">
			   <h4 class="phead"> MRP:  Rs. 325/</h4><br>
         <p><b>Availability : </b> In Stock</p>
         <br>
              
            </div>
             <div class="row">

                  <div class="col-sm-4 col-12">
                    <div class="quantity">

                 <i class="fa fa-minus" onclick="decrement()"></i> &nbsp;
                 
                 <input type="text" name="pro_quantity" id="quantity" value="1" min='1' style="width: 100px; text-align: center;"> &nbsp;

                 <i class="fa fa-plus" onclick="increment()"></i>

            
                
               </div>
                  </div>

                  <div class="col-sm-2 col-6" style="padding: 0!important">
                    <span class="btn btn-success" onclick="addToCart()">Add To Cart</span>
                  </div>
                  <div class="col-sm-2 col-6"  style="padding: 0!important">
                    <span class="btn btn-danger" onclick="buyNow()">Buy Now</span>
                    
                  </div>
               
             </div>
             <div class="alert alert-success" id="successCart" style="display: none;"></div>
        </div>
        


    </div>

      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Panch Tulsi Benefits:</h4>
               <hr class="hrstyle2">
               <p>Tulsi has a lot of benefits. It has super natural health curing qualities and helpful in all types of Allergies. It has beneficial effect on mind and body. It gives relaxation to mind and body. Scientists have also acknowledged the medicinal properties of tulsi.<br>It keeps away the deadly fevers like Dengue, Malaria, Swine Flu etc . The intake of tulsi is the best way of intestine cleaning.<br>It is very effective in any kind of gynaecological problem. Panch Tulsi is very effective in cough, cold, acidity, constipation, stomach pain, abdominal pain, and Viral or Seasonal fever, swelling in Lungs, hypertension, chest congestion, fatigue, vomiting, obesity, arthritis, asthma, anaemia and ulcers.
It removes the impurities of blood and increases the immunity of human body. It is very helpful in controlling diabetes. It has Anti- Bacterial and Anti- Viral Action.<br><br>Panch Tulsi is disease resistant. It has world’s best anti-oxidant, anti aging, anti-bacterial, anti – septic, anti-viral, anti-flu , anti-biotic , anti – inflammatory, and anti-disease properties.
               <ul>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Panch Tulsi Drop is made by using extract of 5 types of Tulsi like Shyam Tulsi, Rama Tulsi, Shwet Sursa, Van Tulsi and Nimbu Tulsi.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>It is useful in more than 200 diseases like flu, swine flu, dengue , joint pain, stone, blood pressure excess weight, sugar, allergy, hepatitis, burning, urine related problem, gout, piles, pariah, haemorrhage, swollen lungs, ulcer, stress, lack of semen, tiredness,loss of appetite , vomiting , hiccups, stomach ache, stomach bug etc.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Mix 2 drops of T5 Tulsi Drop with honey to get relief from cold, cough, sneezes and headache. It’s very effective in winters in unconscious condition put drops of Panch Tulsi Drop with salt in nose. It’s very effective.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Beneficial for cancer patients. Mix 2-3 drops of Panch Tulsi Drop with curd take it. Take maximum curd & milk as food.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Beneficial for a child suffering from cold, cough or loose motions, Mix one or two drops of Panch Tulsi Drop with honey & give to the child.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span> After lunch have one drop of Panch Tulsi Drop , it helps to cure stomach related diseases.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Panch Tulsi Drop relieves in itching, eczema etc and all other skin related diseases & wounds.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Panch Tulsi Drop purifies blood, and much helpful to increase haemoglobin.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Daily intake of 4-5 drops of Panch Tulsi Drop controls nausea & vomiting in pregnancy. A friend of women in all problems.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Especially helps in fire burns or any poisonous insect attack.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>To control hair fall & premature greying of hair & dandruff, use 8 -10 drops of Panch Tulsi Drop with aloe herbal hair oil and apply on the head & forehead. For best results apply this at night & wash it in the morning.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>In ear pain and ear flow, pour one drop of Panch Tulsi Drop in each ear.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>In Tooth pain, cavity, bleeding gums, have gargles with warm water mixed with 4-5 drops of Panch Tulsi Drop.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>To get rid of bad breath have 1-2 drops of Panch Tulsi Drop daily after meals.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Throat pain, ulcers can be relieved by Panch Tulsi Drop.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>In asthma and cough have Panch Tulsi Drop with ginger ras and honey in the morning and evening.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span> At night mix 8-10 drops of Panch Tulsi Drop in body oil, it will relieve you from mosquito bite and you will have a sound sleep.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Mix 8-10 drops of Panch Tulsi Drop in water cooler. It helps to keep your house free from mosquitoes and makes the environment bacteria free, clean and pure.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Lice: Equal quantity of Panch Tulsi Drop & lime juice to be applied to the hair & kept for three hours. Wash hair, the lice will be dead.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span> Panch Tulsi Drop is very useful for heart. With the use of this regularly cholesterol level decreases and reduces blood clots, heart attack and stroke.</li>
	 <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>At the time of bath mix 8-10 drops of Panch Tulsi Drop in water and have bath you will have disease free skin. It is very useful for skin problems & eliminates stress.</li>
	 
               </ul>
            
			</p>
            
        </div>
      </div>
       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Dosage</h4>
               <hr class="hrstyle2">
              <p>Use 1 drop Panch Tulsi Drop in one glass of water or tea or 2 drops in 1 litre water. With this, water will be bacteria free, healthy and hygienic.</p>
            
        </div>
      </div>


       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">

             <h4 class="phead">IMPORTANT</h4>
              <hr class="hrstyle2">
            <p>As with any supplement, if you are pregnant, nursing or taking medication, consult your doctor before use, Discontinue use two weeks prior to surgery.</p>
			<p>All product statements on this website provides general information . It is not designed to treat or diagnose disease or injury , nor it is a substitute for sound medical advice or treatment from a healthcare professional. Readers are advised to seek medical advice for any specific health concern . Individual results may vary.</p>
           
               
               
            
        </div>
      </div>


    
</div>

	@include('products.products-list');
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection



@section('scripts')

<script type="text/javascript">

function increment(){
  var qty = $('#quantity').val();
  qty = parseInt(qty)+1;

  $('#quantity').val(qty);
}

function decrement(){
  var qty = $('#quantity').val();
  if(qty>1)
  qty = parseInt(qty)-1;

  $('#quantity').val(qty);
}

  function addToCart(){
    var product_id = '2';
    var product = 'Tulsi';
    var price = '325';
    var quantity = $('#quantity').val();
    var image = 'asset/images/product/2.jpg';
    var page_url = 'product/tulsi';

  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    /* the route pointing to the post function */
    url: '/add-to-cart',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, product_id: product_id, product: product, quantity: quantity, image: image, price: price, page_url: page_url},
    success: function (data) { 
      // window.location.href = '/en';
      $("#cartCount").text(data.countCart);
      $('#successCart').show();
      $("#successCart").text('Product successfully added to cart');
    }
  }); 

  }
  
   function buyNow(){
     var product_id = '2';
    var product = 'Tulsi';
    var price = '325';
    var quantity = $('#quantity').val();
    var image = 'asset/images/product/2.jpg';
    var page_url = 'product/tulsi';


  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    /* the route pointing to the post function */
    url: '/add-to-cart',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, product_id: product_id, product: product, quantity: quantity, image: image, price: price, page_url: page_url},
    success: function (data) { 
      // window.location.href = '/en';
      $("#cartCount").text(data.countCart);
    //   $('#successCart').show();
    //   $("#successCart").text('Product successfully added to cart');
      
      window.location.href='/checkout-products';
    }
  }); 

  }
  
  
</script>


@endsection