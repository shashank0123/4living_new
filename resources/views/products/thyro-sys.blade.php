@extends('layouts.ecommerce2')

<style type="text/css">
  .quantity {
    margin-bottom: 20px;
  }

  .quantity i {
    cursor: pointer;
  }
</style>

@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>

<div class="container-fluid mt-40 mb-50">
    <h2 class="sechead">Ayurvedic Medicines: THYRO-SYS </h2>
     <hr class="hrstyle">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            
               <div class="img-hover-zoom img-hover-zoom--xyz pdsec1_1">
  <img src="../asset/images/product/5.jpg" alt="Another Image zoom-on-hover effect" width="300" height="350">
</div>
           
        </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="pdsec1_2">
               <h4 class="phead"> THYRO-SYS</h4>
               <hr class="hrstyle2">
			   <h4 class="phead"> MRP:  Rs. 1800/</h4><br>
          <p><b>Availability : </b> In Stock</p>
         <br>
              
            </div>

             <div class="row">

                  <div class="col-sm-4 col-12">
                    <div class="quantity">

                 <i class="fa fa-minus" onclick="decrement()"></i> &nbsp;
                 
                 <input type="text" name="pro_quantity" id="quantity" value="1" min='1' style="width: 100px; text-align: center;"> &nbsp;

                 <i class="fa fa-plus" onclick="increment()"></i>

            
                
               </div>
                  </div>

                  <div class="col-sm-2 col-6" style="padding: 0!important">
                    <span class="btn btn-success" onclick="addToCart()">Add To Cart</span>
                  </div>
                  <div class="col-sm-2 col-6"  style="padding: 0!important">
                    <span class="btn btn-danger" onclick="buyNow()">Buy Now</span>
                    
                  </div>
               
             </div>
             <div class="alert alert-success" id="successCart" style="display: none;"></div>
        </div>
        
    </div>

      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Ingredients:</h4>
               <hr class="hrstyle2">
               <p> <ol>
                   <li> Allium Sativum Linn.</li>
                    <li>Andrographis Paniculate</li>
                     <li>Compound Drug</li>
                      <li>Gum</li>
					  <li>Sodium Benzoate</li>
					  <li>Starch</li>
					  <li>Magnesium Stearate</li>
					  

               </ol> 
			   </p>
            
        </div>
      </div>
     

    
</div>
        @include('products.products-list');
    <!-- inventor end -->
    <!-- inventor end -->
@endsection


@section('scripts')

<script type="text/javascript">

function increment(){
  var qty = $('#quantity').val();
  qty = parseInt(qty)+1;

  $('#quantity').val(qty);
}

function decrement(){
  var qty = $('#quantity').val();
  if(qty>1)
  qty = parseInt(qty)-1;

  $('#quantity').val(qty);
}

  function addToCart(){
    var product_id = '5';
    var product = 'Thyro Sys';
    var price = '1800';
    var quantity = $('#quantity').val();
    var image = 'asset/images/product/5.jpg';
    var page_url = 'product/thyro-sys';

  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    /* the route pointing to the post function */
    url: '/add-to-cart',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, product_id: product_id, product: product, quantity: quantity, image: image, price: price, page_url: page_url},
    success: function (data) { 
      // window.location.href = '/en';
      $("#cartCount").text(data.countCart);
      $('#successCart').show();
      $("#successCart").text('Product successfully added to cart');
    }
  }); 

  }
  
  
  
  function buyNow(){
     var product_id = '5';
    var product = 'Thyro Sys';
    var price = '1800';
    var quantity = $('#quantity').val();
    var image = 'asset/images/product/5.jpg';
    var page_url = 'product/thyro-sys';

  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    /* the route pointing to the post function */
    url: '/add-to-cart',
    type: 'POST',
    /* send the csrf-token and the input to the controller */
    data: {_token: CSRF_TOKEN, product_id: product_id, product: product, quantity: quantity, image: image, price: price, page_url: page_url},
    success: function (data) { 
      // window.location.href = '/en';
      $("#cartCount").text(data.countCart);
    //   $('#successCart').show();
    //   $("#successCart").text('Product successfully added to cart');
      
      window.location.href='/checkout-products';
    }
  }); 

  }
  
</script>


@endsection