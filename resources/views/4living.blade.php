@extends('layouts/ecommerce2')

@section('content')
<div class=" mt-30 mb-30">
           <img class="img-responsive"  src="asset/images/bg/breadcrumb.jpg">
        </div>


<!-- header end here -->

<div class="container-fluid">
    <div class="row lrmargin">
        <div class="livingsec1 pagemarginsec">
             <h2 class="sechead">Total Health Forever </h2>
              <hr class="hrstyle">
              <p>True health is created through mindful daily actions and consistent habits, and it starts with premium supplementation and ayurveda. To get the most from your supplements or ayurveda, you must embrace a lifestyle that includes a diet of whole foods, consistent movement, hydration, stress management, proper sleep, and overall mindfulness. This is what it means to 4LivinG, get inspired and join our community.</p>
        </div>
        
    </div>
     <div class="livingsec1 pagemarginsec2">
    <div class="row lrmargin">
       
             <div class="col-lg-3 col-md-3 col-sm-4 col-12">
                 <img class="img-responsive" src="asset/images/living/1.jpg">
             </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-12">
                    <img class="img-responsive" src="asset/images/living/2.jpg">
              </div>
               <div class="col-lg-3 col-md-3 col-sm-4 col-12">
                    <img class="img-responsive" src="asset/images/living/4.jpg"> 
               </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-12">
                    <img class="img-responsive" src="asset/images/living/5.jpg">  
                </div>
        </div>
        
    </div>
    
</div>

 <div class="about-us-area pt-90 pb-90">
       <h2 class="sechead">Partners In Health </h2>
         <hr class="hrstyle">
            <div class="container">
			<div style="text-align:center; font-size:18px">4LivinG partners and Collaborate with leading experts in the Fields of Health and Wellness and share that expertise with you.</div><br><br>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="about-us-img text-center">
                            <a href="#">
                                <img src="asset/images/premp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 align-self-center">
                        <div class="about-us-content">
                            <h2>Dr. Prem Prakash</h2>
                           
                            <p>Dr. PremPrakash association with the Modern System of Medicine for the last more than 50 Years and also advised and believe to treat any health problem with Ayurveda, Naturopathy.</p>
                            
                        </div>
                    </div>
                </div>
                 <div class="row">
                     <div class="col-lg-6 col-md-6 align-self-center">
                        <div class="about-us-content">
                            <h2>Dr. Manoj Sharma</h2>
                           
                            <p>4LivinG partner With Dr. Manoj Sharma B.A.M.S., in Ayurveda. Also, board Members With us. He received his medical training At the nation’s top institution.</p>
                            
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="about-us-img text-center">
                            <a href="#">
                                <img src="asset/images/manojs.jpg" alt="">
                            </a>
                        </div>
                    </div>
                   
                </div>
				<div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="about-us-img text-center">
                            <a href="#">
                                <img src="asset/images/rparsad.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 align-self-center">
                        <div class="about-us-content">
                            <h2>R. Parshad</h2>
                           
                            <p>4LivinG partners with R. Parshad. He is a lot of experience in Naturopathy, Ayurveda and Indian Herbs. Also, Board - Member and CEO 4LivinG Company.  He is dedicated and determined to relieve human suffering.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- inventor end -->
@endsection