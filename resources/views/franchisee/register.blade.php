<?php
use App\Models\Package;

$packages = Package::where('package_amount','>',10000)->get();

?>

@extends('layouts.ecommerce2')

@section('content')
<style>
  .package_list li h3{
    font-weight: bold;
    padding: 1%;
    margin-top: 15px;

  }

  .member_text{
    /*margin: 1% ;*/
    width: 100%;
    padding: 10px;
    background-color: green;
    color: #fff;
  }

  .package_list li{
    background-color: #fff;
    height: 210px;
    float: left;
    padding: 7px;
    margin: 10px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    text-align: center;
  }

  /*.packages-section{

    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    width: 100%;
  }
  */
  .package_list li:hover{

    zoom: 1.02;
  }

  input[type="radio"]{
    width: auto;
    height: auto;
  }

  .select_package{
    margin-top: 35px;
    text-align: center;
    padding: 10px;
    margin-left: 20px;
    margin-right: 20px;
    background-color: green;
    color: #fff;
    width: fit-content;
  }

  .package_list li a {
    margin-top: 10px;
    font-weight: bold;

  }

  #packages h2{
    text-align: center;
    color: #333333;
    font-weight: bold;
    margin-bottom: 45px;
  }



  .membership{
    padding: 35px;
    font-size: 28px !important;
    text-align: center;
    color: #333333;
    font-weight: bold;
  }
  .form-control{
    margin-bottom: 25px;
    height: 35px !important;   
  }
  label{
    color: #777777;
    font-weight: bold;
  }
  .family_detail { 
    display: none;
  }

  .membership_section{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    padding: 45px;


  }

  input::placeholder, select{
    font-size: 12px !important;
  }
</style>


<style type="text/css">
	
    .section_form{
        padding: 45px;
    }
    
    .payment_section{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div class=" mt-30 mb-30">
 <img class="img-responsive"  src="/asset/images/bg/breadcrumb.jpg">
</div>
<div class="container mt-40 mb-50">
    <h2 class="sechead">Franchise Registration</h2>
    <hr class="hrstyle">
    <br><br>
    <div class="row">

        <!-- <div class="col-sm-4"></div> -->
        <div class="col-sm-12">

            <!-- <h2>Select Payment Mode</h2> -->
            <div class="row payment_section">
                <div class="col-sm-12" >
                    <div class="section_form">

                        @if(!empty(session()->get('message')))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}    
                        </div>
                        @endif

<form action="{{ url('franchisee/postRegister') }}" method="POST" enctype="multipart/form-data">

                        @if(!empty($packages))
                    <div class="form-group" id="packages">
                      <h2>FRANCHISEE PLANS</h2>
                      <div class="row">
                        <ul class="package_list">
                          @foreach($packages as $package)
                          <li>
                            <div class="member_text">Franchisee Plans</div>
                            <h3> Rs. {{number_format((float)$package->package_amount, 0, '.', '')}} /- </h3>
                            <div class="select_package"><input type="radio" name="package_id" id="package_id" value="{{$package->id ?? ''}}" required="required">&nbsp;&nbsp;<span>Select Now</span></div>
                            <a href="{{ url('products_detail/'.$package->id) }}"><u>PD</u></a>
                          </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                    @endif
                    <br><br>

                        <p>Click To Download Franchisee Registration Form. <a href="/legal_documents/FranchiseeForm.pdf" class="btn btn-danger" download> Download </a></p>
                        <br><br>
                        <p>Fill the form and upload it Here with following other documents.</p>
                        <div>
                            
                                @csrf

                                <div class="row">

                                    <div class="form-group col-sm-6">
                                        <label>Upload Scanned Form</label>
                                        <input type="file" name="reg_form" id="reg_form" class="form-control" required="required">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Passport Size Photo</label>
                                        <input type="file" name="profile_pic" id="profile_pic" class="form-control" required="required">
                                    </div>
                                    
                                    <div class="form-group col-sm-6">
                                        <label>Adhaar (Front)</label>
                                        <input type="file" name="adhaar_front" id="adhaar_front" class="form-control" required="required">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Adhaar (Back)</label>
                                        <input type="file" name="adhaar_back" id="adhaar_back" class="form-control" required="required">
                                    </div>
                                    
                                    <div class="form-group col-sm-6">
                                        <label>Pan Card</label>
                                        <input type="file" name="pan_card" id="pan_card" class="form-control" required="required">
                                    </div>

                                    <div class="col-sm-12">
                                        <button type="submit" name="save" class="btn btn-danger">Submit</button>
                                    </div>
                                    
                                </div>

                            
                        </div>



</form>
                    </div>
                </div>
            </div>

        </div>
        
    </div>

</div>
@endsection