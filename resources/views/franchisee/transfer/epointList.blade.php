<?php
use App\Models\Member;
?>

@extends('franchisee.app')

@section('title')
All Transfer | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="/en/login">Dashboard</a></li>
  <li class="active">Funds Transfer List</li>
</ul>
@stop

@section('content')
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data" style="padding: 1% 5%">
        <div class="page-header">
          <div class="row">
              <div class="col-sm-5">                
          <h1 style="font-size: 22px"><i class="md md-account-balance"></i> Funds Transfer List</h1>
          <p class="lead">Funds Transfer status.</p>
              </div>
              <div class="col-sm-7" style="text-align: right;">
                <form method="get" action="/en/searched-funds">
                  <select name="category" class="form-group" style="padding: 5px; border: 2px solid #ddd">
                    <option value="0">Select</option>  
                    <option value="sent">Sent</option>  
                    <option value="received">Received</option>
                  </select>
              <input type="search" class="form-group" name="search" style="padding: 5px;width: 40%" placeholder="Enter username here" required/>
              <button type="submit" class="btn btn-primary">Search</button>
            </form>
              </div>
          </div>
        </div>
        
            <input type="text" value='SENT' class="form-group" style="background-color:rgba(255,0,0,0.5); color: #fff; width: 150px; padding: 5px; text-align: center; " readonly>
            <input type="text" value='RECEIVED' class="form-group" style="background-color:rgba(0,153,153,0.5); color: #fff; width: 150px; padding: 5px;text-align: center;" readonly>
          
       

        <div class="card">
          <div class="card-content">
            <table class="table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Funds</th>

                </tr>
              </thead>
              <tbody>
                @if(!empty($epoints))
                @foreach($epoints as $epoint)
                <tr style=" background-color: @if($member->id == $epoint->sender_id) {{'rgba(0,153,153,0.5)'}} @else {{'rgba(255,0,0,0.5)'}} @endif ; color: #fff; ">
                  <td>{{$epoint->created_at}}</td>
                  <td>
                    @if($epoint->sender_id == 'admin')
                    {{'admin'}}
                    @else

                    <?php
                    $sender = Member::where('id',$epoint->sender_id)->first();
                    
                    ?>               
                    {{$sender->username}}     
                    @endif
                    
                  </td>
                  <td>
                    @if($epoint->receiver_id == 'admin')
                    {{'admin'}}
                    @else

                    <?php
                    
                    $receiver = Member::where('id',$epoint->receiver_id)->first();
                    ?>               
                    {{$receiver->username}}    
                    @endif
                  </td>
                  <td>{{$epoint->no_of_epoint}}</td>

                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
@stop
