<?php
use App\Repositories\SharesRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;
$sharesRepo = new SharesRepository;
$announcementModel = new \App\Models\Announcement;
use App\Models\Epoint;
$epoints = Epoint::where('member_id', $member->id)->first();
?>

<style type="text/css">
  body {
   background: url({{ \URL::current() }}/../assets/img/login.jpg) no-repeat center center fixed;
 }

 /*.breadcrumb { background-color: #EBF5FB !important }*/
 .set-margin { margin-top: 20px; }
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,input,div { text-transform: uppercase !important; }
 ul li , ul li a { color: #fff !important; }
  
</style>
@extends('front.app')

@section('title')
{{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#" style="color: #fff">@lang('breadcrumbs.front')</a></li>
  <li class="active" style="color: #fff">@lang('breadcrumbs.package')</li>
</ul>
@stop

@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <div class="col-md-12">
        <div class="row" style=" background-color: #EBF5FB !important ">
          <div class="col-sm-3">
            {{-- <h2>Share Link</h2> --}}
          </div>
          
          
        </div>

       
        {{-- <span>{{url('/register').'?referal_id='.$member->username}}</span> --}}
      </div>
      <div class="dashboard grey lighten-3">
        <div class="row no-gutter">
          <div class="col-sm-12 col-md-12 col-lg-12" style="background-color:  #EBF5FB ;">
            <div class="p-20 clearfix">
              <div class="pull-right">
                

              </div>

              <h4 class="grey-text">
                {{-- <i class="md md-dashboard"></i> <span class="hidden-xs">@lang('home.qTitle')</span> --}}
              </h4>
            </div>

            <div class="p-20 no-p-t">
              
              <div class="row" style="background-color:  #EBF5FB ">
                <div class="col-md-2"></div>
                <div class="col-md-5" style="text-align: left;">
                   <h4><b>Select User Name & Package Amount</b></h4><br>
                  <div class="bs-component">
                    
                    <label for="username">User Name</label>
                    <input type="text" name="username" id="username" class="form-control" onchange="getUserDetail()">
                      
                     
                  </div><br><br>
                  <p id="check-availability"></p>


                  <div class="bs-component">
                    <input type="hidden" id="package_id" name="package_id">
                    <select name="package" class="form-control" id="selet-package" onchange="getvalue()">
                      
                      <option >Select Package</option>
                      <option value="499">Rs. 499</option>
                      <option value="599">Rs. 599</option>
                    </select>
                  </div><br><br>
                
          
            <input type="button" name="activate" value="Pay" onclick="getPayment()" class="btn btn-success">
          
         
                </div>
              </div>
              

                {{-- <div class="row gutter-14 kpi-dashboard">
                  <div class="col-md-4">
                    <div class="card small">
                      <div class="theme-lighten-1 p-10">
                        <div class="pull-right">
                          <div> <i class="md md-account-balance-wallet text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.pointTitle')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i> @lang('common.pointSub')</div>
                      </div>
                      <div class="card-content p-10">
                        <div class="row">
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span style="font-size: small">{{ explode(' ', $member->created_at)[0] }}</span></h3>
                            <p class="grey-text w600">@lang('common.joining_date')</p>
                          </div>
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span data-count data-start="0" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h3>
                            <p class="grey-text w600">@lang('common.register')</p>
                          </div>
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span style="font-size: small">{{ explode(' ', $member->created_at)[0] }}</span></h3>
                            <p class="grey-text w600">@lang('common.paid_date')</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> --}}

                  

                  <div class="col-md-4">
                    <div class="card small">
                      
                      
                    </div>
                  </div>
                </div>                



                
                
               
              </div>

              
              {{-- for Amount --}}
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>

<?php
$annNew = \Cache::remember('announcement.new', 60, function () {
  return \App\Models\Announcement::whereRaw('Date(created_at) = CURDATE()')->first();
});
?>

<script>
function getUserDetail(){
      var name = $('#referal_id').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          alert(msg);
          if(msg.message == 'Success'){
            $('#member-detail').show();
            $('#uname').html("Name : "+msg.name);
          }
          else{
            $('#uname').html(msg.message);
          }          
        }
      });
    }

  function getvalue(){
    var value=$('#selet-package').val();
    $('#package_id').val(value);
  }

  function getPayment(){
    var value = $('#package_id').val();
    window.location.href = "/register/membership-fee/{{$member->user_id}}/"+value;
  }
</script>



@stop
