<style>
	li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
</style>
<a href="{{ route('announcement.read', ['id' => $model->id, 'lang' => \App::getLocale()]) }}" class="btn btn-info btn-flat-border">
  <i class="md md-input"></i> @lang('common.read')
</a>
