<?php
  use App\doctorspanel;
 
  $doctorTeams = DB::table('doctorspanels')
                ->where('panel_type', 'panelofdoctors')
				->orderBy('rank', 'asc')
                ->get();
  
?>
@extends('layouts/ecommerce2')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
@if (count($doctorTeams) > 0)
<div class="container-fluid mt-40 mb-50 ">
    <h2 class="sechead">Panel Of  Doctor's </h2>
     <hr class="hrstyle">
    <div class="row lrmargin pagemarginsec" >
	@foreach ($doctorTeams as $doctors)
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
           
<div class="panelcard">
  <img src="assetsss/images/doctorspanel/{{ $doctors->image }}" alt="{{ $doctors->name }}" style="width:100%">
  <h1>{{ $doctors->name }}</h1>
  <p class="title">( {{ $doctors->designation }} )</p>
  
 
  <p><button  onclick="location.href = 'panel-of-doctors/details/{{ $doctors->id }}';">Contact</button></p>
</div>
        </div>
		 @endforeach
        
        
        
    </div>
    
</div>
  @endif
    <!-- inventor end -->
    <!-- inventor end -->
@endsection