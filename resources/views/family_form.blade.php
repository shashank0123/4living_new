<div class="row" id="form{{$id}}">
<div class="col-sm-3">
  <label>Name *</label>
  <input type="text" name="member_name[]" class="form-control" id="member_name" required="required" placeholder="Enter Name  Here">
</div>
<div class="col-sm-3">
  <label>Relation *</label>
  <input type="text" name="member_relation[]" class="form-control" id="member_relation" required="required" placeholder="Enter Relation Here">
</div>
<div class="col-sm-3">
  <label> Married / Unmarrid *</label>
  <select name="member_marital_status[]" id="member_marital_status" class="form-control" required="required">
      <option value="">Select Marital Status</option>
      <option value="Married">Married</option>
      <option value="Unmarried">Unmarried</option>
    </select>
</div>
<div class="col-sm-3">
  <label> D. O. B *</label>
  <div class="row">
    <div class="col-sm-8">
      <input type="date" name="member_date_of_birth[]" class="form-control" id="member_date_of_birth" required="required" placeholder="Enter DOB Here">
    </div>
    <div class="col-sm-4">
      <span class="btn btn-danger" id="addFamily" onclick="removerField({{$id}})">Remove</span>
    </div>
  </div>
  
</div>
</div>