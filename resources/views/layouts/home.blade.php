<!DOCTYPE html>
<html>

<!-- Mirrored from envato.megadrupal.com/html/couponday/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Sep 2019 09:23:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <title>Home</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="/asset/home/css/font.css"/>
    <link rel="stylesheet" href="/asset/home/css/font-awesome.css"/>
    <link rel="stylesheet" href="/asset/home/css/normalize.css"/>
    <!--css plugin-->
    <link rel="stylesheet" href="/asset/home/css/flexslider.css"/>
    <link rel="stylesheet" href="/asset/home/css/jquery.nouislider.css"/>
    <link rel="stylesheet" href="/asset/home/css/jquery.popupcommon.css"/>

    <link rel="stylesheet" href="/asset/home/css/style.css"/>
    <link rel="stylesheet" href="/asset/home/css/style-dark.css">
    <link rel="stylesheet" href="/asset/home/css/style-gray.css">
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie8.css"/>
    <![endif]-->

    <link rel="stylesheet" href="/asset/home/css/res-menu.css"/>
    <link rel="stylesheet" href="/asset/home/css/responsive.css"/>
    <!--[if lte IE 8]>
        <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" crossorigin="anonymous"></script>
      
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <style>
  /* Make the image fully responsive */
  @media (min-width: 980px){
  .bannerimg img {
      width: 100%;
      height: 75vh;
  }}
   @media (max-width: 980px){
  .bannerimg img {
      width: 100%;
      height: 30vh;
  }}


  .button3:hover {
  background-color: white; 
  color: black; 
 
}

.button3 {
     background-color: #04BFBF; /* Green */
  border: none;
  color: white;
  padding: 8px 22px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 24px 2px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;

  color: white;
   border: 2px solid #04BFBF;
}
  </style>

</head>
<body class="gray"><!--<div class="alert_w_p_u"></div>-->
<div class="container-page">
    <div class="mp-pusher" id="mp-pusher">
        <header class="mod-header">
            <div class="grid_frame">
                <div class="container_grid clearfix">
                    <div class="grid_12">
                        <div class="header-content clearfix">
                            <h1 id="logo" class="rs">
                                <a href="index.php">
                                    <span style="color:#FFBE19 ;font-weight: bold; font-size: 28px;" >4</span><span style="color: #04BFBF; font-weight: bold; font-size: 28px;">LIVING</span>
                                    <!-- <img src="images/logo.png" alt="$SITE_NAME"/> -->
                                </a>
                            </h1>
                           <!--  <a id="sys_head_login" class="btn btn-green type-login btn-login" href="#">Login</a> -->
                            <nav class="main-nav">
                                <ul id="main-menu" class="nav nav-horizontal clearfix">
                                    <li class="active">
                                        <a href="index-2.####">Home</a>
                                    </li>
                                    <li>
                                        <a href="coupon.####">Product</a>
                                    </li>
                                     <li>
                                        <a href="coupon.####">Company</a>
                                    </li>
                                     <li>
                                        <a href="coupon.####">Oppertunity</a>
                                    </li>
                                     <li>
                                        <a href="coupon.####"> Achievers</a>
                                    </li>
                                    
                                    <li >
                                        <a href="brand-list.####">Brands</a>
                                       
                                    </li>
                                    <li><a href="blog.####">Gallery</a></li>
                                    
                                </ul>
                                <a id="sys_btn_toogle_menu" class="btn-toogle-res-menu" href="#alternate-menu"></a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
        </header><!--end: header.mod-header -->
        <nav id="mp-menu" class="mp-menu alternate-menu">
            <div class="mp-level">
                <h2>Menu</h2>
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="coupon.php">Coupons</a></li>
                    <li class="has-sub">
                        <a href="coupon-code.####">Coupons Code</a>
                        <div class="mp-level">
                            <h2>Coupons Code</h2>
                            <a class="mp-back" href="#">back</a>
                            <ul>
                                <li><a href="coupon-code.####">Coupons Code 1</a></li>
                                <li><a href="coupon-code-2.####">Coupons Code 2</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="has-sub">
                        <a href="brand-list.php">Brands</a>
                        <div class="mp-level">
                            <h2>Brands</h2>
                            <a class="mp-back" href="#">back</a>
                            <ul>
                                <li><a href="brand-detail-1.####">Brand Detail 1</a></li>
                                <li><a href="brand-detail-2.####">Brand Detail 2</a></li>
                                <li><a href="brand-detail-3.####">Brand Detail 3</a></li>
                                <li><a href="brand-detail-4.####">Brand Detail 4</a></li>
                                <li><a href="brand-detail-5.####">Brand Detail 5</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="blog.####">Blog</a></li>
                    <li><a href="my-coupon.####">My coupons(12)</a></li>
                    <li><a href="login.####">Login</a></li>
                </ul>
            </div>
        </nav><!--end: .mp-menu -->
		
		  <!-- Indicators -->
		   <div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item bannerimg active">
      <img src="/asset/home/images/ex/banner1.jpg" alt="Los Angeles" width="1100" height="500">
    </div>
    <div class="carousel-item bannerimg">
      <img src="/asset/home/images/ex/banner2.jpg" alt="Chicago" width="1100" height="500">
    </div>
    <div class="carousel-item bannerimg">
      <img src="/asset/home/images/ex/ayurvedik.jpg" alt="New York" width="1100" height="500">
    </div>
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
		
		@yield('content')
		
		  <footer class="mod-footer">
            <div class="footer-top">
                <div class="grid_frame">
                    <div class="container_grid clearfix">
                        <div class="grid_3">
                            <div class="company-info">
                                 <span style="color:#FFBE19 ;font-weight: bold; font-size: 28px;" >4</span><span style="color: #04BFBF; font-weight: bold; font-size: 28px;">LIVING</span>
                               <!--  <img src="images/logo.png" alt="CouponDay"/> -->
                                <p class="rs">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud</p>
                                <p class="rs">
                                    1200 Balh Blah Avenue <br />
                                    Hanoi, Vietnam 12137
                                </p>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="block social-link">
                                <h3 class="title-block">Follow us</h3>
                                <div class="block-content">
                                    <ul class="rs">
                                        <li>
                                            <i class="fa fa-facebook-square fa-2x"></i>
                                            <a href="#" target="_blank">Our Facebook page</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-twitter-square fa-2x"></i>
                                            <a href="#" target="_blank">Follow our Tweets</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-pinterest-square fa-2x"></i>
                                            <a href="#" target="_blank">Follow our Pin board</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--end: Follow us -->
                        <div class="grid_3">
                            <div class="block intro-video">
                                <h3 class="title-block">Intro Video</h3>
                                <div class="block-content">
                                    <div class="wrap-video" id="sys_wrap_video">
                                        <div class="lightbox-video">
                                                <a class="####5lightbox" href="http://player.vimeo.com/video/36932496" title=""><i class="btn-play"></i><img src="asset/home/images/video-img.png" alt=""></a>     
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: Intro Video -->
                        <div class="grid_3">
                            <div class="block blog-recent">
                                <h3 class="title-block">Address</h3>
                                <div class="block-content">
                                   <p>Dhanwantari Distributors Pvt. Ltd.<br>
 
Heart Line, First floor, Plot No.24,<br>
Behind Sanjeevan ICU, Sadar bazar,<br>
Satara - 415002. M.S. (India).</p>
                                </div>
                            </div>
                        </div><!--end: blog-recent -->
                    </div>
                </div>
            </div><!--end: .foot-top-->
            <div class="foot-copyright">
                <div class="grid_frame">
                    <div class="container_grid clearfix">
                        <div class="left-link">
                            <a href="#">Home</a>
                            <a href="#">Term of conditions</a>
                            <a href="#">Privacy</a>
                            <a href="#">Support</a>
                            <a href="#">Contact</a>
                        </div>
                        <div class="copyright">
                            Copyright &copy; 2014 by www.DhanMantri.com
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

<script type="text/javascript" src="/asset/home/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/asset/home/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="/asset/home/js/jquery.nouislider.js"></script>
<script type="text/javascript" src="/asset/home/js/jquery.popupcommon.js"></script>

<!--//js for responsive menu-->
<script type="text/javascript" src="/asset/home/js/modernizr.custom.js"></script>
<script type="text/javascript" src="/asset/home/js/classie.js"></script>
<script type="text/javascript" src="/asset/home/js/mlpushmenu.js"></script>

<script type="text/javascript" src="/asset/home/js/script.js"></script>

<!--[if lte IE 9]>
<script type="text/javascript" src="js/jquery.placeholder.js"></script>
<script type="text/javascript" src="js/create.placeholder.js"></script>
<![endif]-->
<!--[if lte IE 8]>
<script type="text/javascript" src="js/ie8.js"></script>
<![endif]-->

</body>

</html>