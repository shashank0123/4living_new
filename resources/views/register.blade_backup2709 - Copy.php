@extends('front.app')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important; text-transform: uppercase !important;
  }
  .pink-text {  font-size: 24px;  }

  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .card.bordered .card-action {
   border-top: none !important; 
 }
 .form-control { color: #000 !important; }
 select option { color: #000; }
 select {text-transform: uppercase !important;}
 label { text-transform: uppercase !important; }
 .card.bordered .card-header {
  border-bottom: 1px solid #000 !important;
}

[type='text'].form-control { color: #000; }

#register label, #otpcontainer label { color: #fff; }

.pull-right button { margin-bottom: 20px !important }
@media screen and (max-width: 991px)
{
  .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
}

@media screen and (max-width: 580px)
{
  .card { max-width:90%;margin-left: 5%; }
}

#member-detail { display: none; }
#member-detail .row{ width: 38%; background-color: #eee }
@media screen and (max-width: 768px){
  #member-detail .row{ width: 90% }
}

#login-head { width: 100% !important; background-color: #1b4f82 }
#otpcontainer { display: none; }
#member-detail {display: none;}
</style>

@section('title')
Register - {{ config('app.name') }}
@stop

@section('content')
<div class="clearfix"></div>
<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: #1b4f82; position: absolute; top: 0;">
    <div style="background-color: #1b4f82;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
    <ul>
      <li style="list-style: none">
        <a href="#" style="color: #fff; font-size: 14px;text-transform: uppercase !important;">Help Line:  +919315728925 / +917838753662</a><br>
      </li>

    </ul>
  </div>
</div>

<div class="card bordered z-depth-2" style="margin-top: 70px;background-color: #1b4f82;">
  <div class="card-header" style="background-color: #1b4f82">
    <div class="brand-logo" style="text-align: center;">

      <a href="/"><img src="{{ asset('/images/logo.gif') }}" width="auto"; height="100px"></a>
    </div>
  </div>
  <div id="formcontainer" class="container m-b-30" style="max-width: 100%;">
    <form class="form-floating action-form" id="register" http-type="post" data-url="{{ route('registerforuse', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong black-text" style="font-weight: bold;text-transform: uppercase !important;">Registration Form - Become a member</div>
        </div>

        <div class="form-group">
          <label for="username">Sponsor ID</label>
          <input type="text" style="text-transform: uppercase;" @if (isset($_GET['referal_id'])) {{'readonly'}} @endif value="@if (isset($_GET['referal_id'])) {{$_GET['referal_id']}} @endif" name="referal_id" class="form-control" id="referal_id" required="required" onchange = "getUserDetail()">
        </div>



        <div class="form-group" id="member-detail">
          <div class="container">
            <div class="row" style="color: #000; font-weight: bold; border-bottom: 1px solid #fff">
              &nbsp;&nbsp;&nbsp;&nbsp;<span id="uname" style="color: #000; font-weight: normal;"></span><br>
              {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Email : <span id="uphone">1234</span><br> --}}

              {{-- &nbsp;&nbsp;&nbsp;&nbsp;User Contact : <span id="uemail">@def.com</span> --}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="username">Placement</label>
          <select  class="form-control" name="placement">
            <option value="left">Left</option>
            <option value="right">Right</option>
          </select>
          {{-- <input type="text" name="username" class="form-control" id="username" required="required"> --}}
        </div>

        <div class="form-group">
          <label for="username">Name</label>
          <input type="text" name="fullname" class="form-control" id="fullname" required="required">
        </div>

        <div class="form-group">
          <label for="username">Email</label>
          <input type="text" name="email" class="form-control" id="email" required="required">
        </div>

        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" name="username" class="form-control" id="username" required="required" onchange="checkExistance()">
        </div>

        <p id="check-availability" style="color: #800000; font-weight: bold;"></p>

        <div class="form-group">
          <label for="username">Mobile</label>

          <input type="text" name="mobile" class="form-control" id="mobile" pattern="[789][0-9]{9}" required="required">
        </div>

        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" class="form-control" id="password" required="required">
        </div>

        <div class="form-group">
          <label for="password">Confirm Password</label>
          <input type="password" name="cpassword" class="form-control" id="cpassword" required="required">
        </div>

        <div class="form-group">
          <label for="password">Transaction Password</label>
          <input type="password" name="secret_password" class="form-control" id="secret_password" required="required">
        </div>

        <div class="form-group">
          <label for="password">Confirm Transaction Password</label>
          <input type="password" name="spassword" class="form-control" id="spassword" required="required">
        </div>
        
        <div class="form-group">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" required="" name="terms"> @lang('register.agreed')
                        </label>
                      </div>
                    </div>

        {{-- <div class="form-group">
          <label for="username">PAN Number</label>
          <input type="text" name="pan" class="form-control" id="pan" pattern="[A-Z]{5}[0-9]{4}[A-Z]{1}" required="required">
        </div> --}}
      </div>

      <div class="card-action clearfix">
        <div class="pull-left">
          <a href="/en/login" class="btn btn-link black-text">              
            <span style="color:#00f; font-weight: bold;">Login</span>
          </a>
        </div>

        <div class="pull-right">
          <span id="submit" onclick="showDiv()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
    </form>
    <br>
  </div>

  <div id="otpcontainer" class="container m-b-30" style="max-width: 100%;">
    <form id="otpform" class="form-floating action-form" http-type="post" data-url="{{ route('verifyotp', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong black-text" style="font-weight: bold;">Verify Your OTP</div>
        </div>
        <input type="hidden" name="username" id="emailvalue">
        <input type="hidden" name="sponsoredId" id="sponsoredId">

        <div class="form-group">
          <label for="username">Enter OTP</label>
          <input type="text" name="otp" class="form-control" id="otp" required="">
        </div>
      </div>
      <input type="hidden" name="verify_otp" class="form-control" id="verify_otp" required="">

      <div class="card-action clearfix"> 
        <div class="pull-right" >
          <span id="verify" onclick="verifyOTP()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

@stop

@section('script')
<script>     


  function getUserDetail(){
      var name = $('#referal_id').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          // alert(msg);
          if(msg.message == 'Success'){
            $('#member-detail').show();
            $('#uname').html("Name : "+msg.name);
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }          
        }
      });
    }

    function checkExistance(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-availability",
        data:{ uname: name },
        success:function(msg){
            if(msg.message == 'Username already exists. Try another'){

              $('#check-availability').show();
          $('#check-availability').html(msg.message);
            }
        else{
          $('#check-availability').hide();
        }
        }
      });
    }


    function showDiv(){    
     var request = $.ajax({
      type: "POST",
      url: "/register",
      data: $('#register').serialize(),

      success:function(data){
        if(data.message == "Registerd Successfully"){
          Swal(data.message);
        setTimeout(function(){ window.location.href = "/en/login"; }, 3000);
        }
        else{
          if(data.success == 'false'){
            if(data.message.mobile)
            Swal(data.message.mobile[0]);
          if(data.message.email)
            Swal(data.message.email[0]);
          }
          else{

          Swal(data.message);
          }
        }
      },

      failure:function(data){

        if(data.success == 'false'){
            Swal(data.success);
          }
          else{
            
          Swal(data.success);
          }
          
      }
    });

//    request.done(function(msg) {
//     if (msg.message == 'Registerd Successfully'){
//       alert('hii');
//       // $('#formcontainer').hide();
//       // $('#emailvalue').val($('#email').val())
//       // $('#sponsoredId').val($('#sponsorId').val())
//       console.log(msg);
//       $("#otpcontainer").show();
//     }
//     else{
//       alert('not');
//       Swal(msg.message);                  
//     }
// });
//    request.fail(function(msg) {
//     alert('fail');
//     Swal(msg.message);
//   });

}


function verifyOTP(){    
  var request = $.ajax({
    type: "POST",
    url: "verifyotp",
    data: $('#otpform').serialize(),  
  });
  request.done(function(msg) {
    if (msg.message == 'Registered Successfully.'){
      console.log(msg);
      Swal(msg.message);
      // window.location.href = "/";
      setTimeout(function(){ window.location.href = "/en/login"; }, 5000);     
      // window.location.href = "/register/membership-fee/"+msg.user_id;      
    }
    else{
      Swal(msg.message);
    }
  });
  

  request.fail(function(msg) {
    Swal(msg.message);
  });
}  




//     sucess:function(msg){
//       if (msg.message == 'Registered Successfully.'){
//         console.log(msg);
//         Swal(msg.message);
//         setTimeout(function(){ window.location.href = "/"; }, 2000); }

//       // window.location.href = "/register/membership-fee/"+msg.user_id;      

//     else{
//       Swal(msg.message);
//     }  
//     }
// });
// }
</script>
@endsection
