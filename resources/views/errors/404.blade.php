@extends('front.app')

@section('title')
Page Error - {{ config('app.name') }}
@stop

@section('content')
<style>
  body{
    background:url(../assets/img/login.jpg) no-repeat center center fixed !important;
  }
  .pink-text {  font-size: 24px; margin-top: 50px !important }
  .text-center h1 { margin-top: 20px !important }
</style>
</div>
<div class="container-fluid center" style="margin-top:20% ;max-width:100%; background-color: #fff;margin-left: 0 !important ">
  <div class="card bordered z-depth-2">
    <div class="card-content">
      <div class="m-b-30 text-center"> <i class="md md-warning error-icon"></i>
        <h1 class="pink-text uppercase"><br>NOT FOUND</h1>
        <p>You are accessing something that is not here. Please head back.</p><br>
      </div>
    </div>
  </div>
</div>
@stop
