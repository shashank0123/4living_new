@extends('layouts/ecommerce2')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

@section('content')


  

<style>
 

  .faq_section{
    margin:40px auto;
  }
  .FaQ_Each{
    padding-bottom: 10px;
  }
  .box  {
    /*background: rgba(246, 246, 246, 1);*/
    color: #666666;
    padding-top: 15px;
    padding-bottom:15px;
    padding-left: 20px;
    font-size: 13px;
    text-transform: uppercase;
    background-color: #f4f4f4;
    cursor:pointer;
    border: 1px solid #d9d9d9;
  }

  .box span:hover { color: #37bc9b; }
  .draw {
    display: none;
    background: #ffffff;
    padding: 20px;
    border-bottom: 1px solid #d9d9d9;
    border-left: 1px solid #d9d9d9;
    border-right: 1px solid #d9d9d9;
    color: #000000;
    padding-left:30px;
  }
  .pull-right { margin-right: 20px }
  .faq_section{ text-align: center; }
  #other{
    display: none;
  }

  .frequently-desc { padding: 50px 50px 10px; text-align: center; line-height: 1.5; font-size: 16px }
</style>

<div class="container-fluid faq">
  <div class="frequently-content">
    <div class="frequently-desc">
      <h4>Below are frequently asked questions, you may find the answer for yourself</h4>
      
    </div>
  </div>

  <div class="container faq_section">

@if($faqs != null)
    {{-- Row1 --}}
    @foreach($faqs as $faq)
    <div class="FaQ_Each">
      <section class="box">

        <span>{{$faq->questions}}</span>
        <span class="pull-right">
          <i class="fa fa-plus" aria-hidden="true"></i>
          <i class="fa fa-minus" id="other" aria-hidden="true"></i>
        </span>
      </section>
      <section class="draw">
       {{$faq->answers}}
     </section>
   </div>
   @endforeach
   @endif

   {{-- Row 2 --}}

   
</div>
</div>
@endsection 

@section('script')
<script>
  $(document).ready(function(){
    $(".box").click(function(){
      $(this).next().slideToggle("fast");
      $(this).find('i').toggle();
    });  

  });
</script>

@endsection