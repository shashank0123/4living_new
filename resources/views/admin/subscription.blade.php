@extends('layouts.admin')

@section('content')

<style type="text/css">
  #btnsub{
    background-color: transparent;
    border:none;
    color: skyblue;
  }
  #subcription-form #input {
    border: 1px solid #e8e8e8;
    padding: 10px;
    display: block;
    margin: auto;
    border-radius: 8px;
  }
</style>
<section style="margin-top: 50px;">
<div class="container" id="subcription-form">
  <form action="addSub" method="post">
    @csrf
    <input type="text" name="subscription" placeholder="Type new subcription" id="input">
    <input type="submit" name="submit" id="input" style="margin-top: 10px">
  </form>
  <table class="table table-bordered" style="margin-top: 20px" class="table">
    <thead>
      <tr>
        <th>S.No</th>
        <th>Subscription</th>
        <th>Created at</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php $sno = 1;?>
      @foreach($subscription as $sub)
      <tr>
        <td>{{$sno}}</td>
        <td>{{$sub->subscription}}</td>
        <td>{{$sub->created_at}}</td>
        <td>
          <form action="delSub" method="post">
            @csrf
            <input type="hidden" name="id" value="{{$sub->id}}">
            <input type="submit" name="submit" value="Delete" id="btnsub">
          </form>
        </td>
      </tr>
      <?php $sno++;?>
      @endforeach
    </tbody>
  </table>
</div>
</section>


@endsection
