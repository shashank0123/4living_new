<?php
  use App\doctorspanel;
 
  $doctorTeams = DB::table('doctorspanels')
                ->where('panel_type', 'doctors')
                ->get();
  
?>
@extends('layouts/ecommerce2')


@section('content')
 <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="asset/images/bg/breadcrumb.jpg">
        </div>
        <div class="about-us-area pt-90 pb-0">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-12 col-md-12		 align-self-center">
                        <div class="about-us-content">
                            <h2>About Us </h2>
                            <p>
4livinG offers Ayurvedic Formulation and dietary Supplement that supports the immune system, health, body transformation, and general wellness. Achieve your healthy lifestyle goal with 4livinG product to help build your immune system, muscle mass and more. Purchase a product as a customer or build your own business as a 4LivinG Distributor.</p><p>
<p>&nbsp;</p>

4LivinG Organic has been providing high quality, affordable Ayurvedic herbs trusted by health-care professionals and health sears. 4LivinG based on 5 Principles of vision, integrity, opportunity, freedom, and success. The Company has set its mission to create healthy India by offering the best Herbs -Nutritional product for Mankind 4LivinG is a one -of -a- kind, life-changing unique company and behind this company are some amazing leaders pressing forward. Drawing on years of experience and innovative new ideas.
</p>

<p>Thank for your love & belief in us.<br><br></p>



                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        
        <div class="team-area pt-60 pb-60">
            <div class="container">
                <div class="section-title-2 text-center">
                    <!--<h2>Team Members</h2>-->
                    <!--<img src="asset/images/icon-img/title-shape.png" alt="icon-img">-->
                </div>
                <div class="row">
				@if (count($doctorTeams) > 0)
					@foreach ($doctorTeams as $doctors)
                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
           
<div class="panelcard">
  <img src="assetsss/images/doctorspanel/{{ $doctors->image }}" alt="{{ $doctors->name }}" style="width:100%">
  <h1>{{ $doctors->name }}</h1>
  <p class="title">( {{ $doctors->designation }} )</p>
  
 
  <p><button  onclick="location.href = 'panel-of-doctors/details/{{ $doctors->id }}';">Contact</button></p>
</div>
        </div>
					@endforeach
					  @endif 
                    
                   
                   
                </div>
            </div>
        </div>
        
       
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection