<?php
use App\Product;
use App\Address;
use App\Country;
use App\State;
use App\City;
$address = "";
$i=0;
$total = 0;
?>

@extends('layouts.ecommerce2')

@section('content')

<style>
	.page-large-title { padding: 20px 0; color: #37bc9b;}
</style>
<div class="page-large-title">
	<div class="container">
		<br>
		<h3 class="title">Checkout</h3>			
		</div><!-- .container -->
	</div><!-- .page-large-title -->	
	
	<div class="section-common container">
		<form action="/checkout-products" method="POST" class="checkout woocommerce-checkout">
			<div class="row">
				{{csrf_field()}}
				<div class="col-md-8 col-xs-12 section-last">
					
					<div class="woocommerce-billing-fields">
						<h3 class="section-title small-spacing">Billing Infomation</h3>
						<div class="clear"></div>
						<div class="row">
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="address">Name</label>
									<input class="input-text input-text-common" type="text" name="name" id="name" value="" required="required">
								</p>
							</div>

							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="address">Email</label>
									<input class="input-text input-text-common" type="text" name="email" id="email" value="" required="required">
								</p>
							</div>
							
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="country">Country </label>
									<input class="input-text input-text-common" type="text" name="country" id="country" value="India" required="required" readonly="readonly">
								</p>
							</div>							
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="state">State </label>
									<input class="input-text input-text-common" type="text" name="state" id="state" value="" required="required">
								</p>
							</div>
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="city">City <span class="required">*</span></label>
									<input class="input-text input-text-common" type="text" name="city" id="city" value="" required="required">		
								</p>
							</div>
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="pin-code">Pin CODE <span class="required">*</span></label>
									<input class="input-text input-text-common" type="number" name="pin_code" id="pin-code" value="" required="required">
								</p>
							</div>
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="phone-number">Phone <span class="required">*</span></label>
									<input class="input-text input-text-common" type="number" name="phone" id="phone" value="" required="required">
								</p>
							</div>
							
							
						</div><!-- .row -->
					</div><!-- .woocommerce-billing-fields -->
<br><br>
					<div class="row">
						<div class="col-sm-1">
							<input type="checkbox" name="shipping_address" id="shipping_address" style="height: 15px; text-align: center;" onclick="showShipping()"> 
						</div>
						<div class="col-sm-11">
								<h4>Different Shipping Address</h4>
						</div>
					</div>

				 <br>
					
					<div class="woocommerce-billing-fields " id="shipping_div" style="display: none;">
						<h3 class="section-title small-spacing">Shipping Infomation</h3>
						<div class="clear"></div>
						<div class="row">

							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="address">Name</label>
									<input class="input-text input-text-common" type="text" name="name1" id="name1" value="">
								</p>
							</div>

							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="address">Email</label>
									<input class="input-text input-text-common" type="text" name="email1" id="email1" value="">
								</p>
							</div>
							
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="country">Country </label>
									<input class="input-text input-text-common" type="text" name="country1" id="country1" value="India" readonly="readonly">
								</p>
							</div>							
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="state">State </label>
									<input class="input-text input-text-common" type="text" name="state1" id="state1" value="">
								</p>
							</div>
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="city">City <span class="required">*</span></label>
									<input class="input-text input-text-common" type="text" name="city1" id="city1" value="">		
								</p>
							</div>
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="pin-code">Pin CODE <span class="required">*</span></label>
									<input class="input-text input-text-common" type="number" name="pin_code1" id="pin-code" value="">
								</p>
							</div>
							<div class="col-sm-12">
								<p class="form-row form-row-input">
									<label for="phone-number">Phone <span class="required">*</span></label>
									<input class="input-text input-text-common" type="number" name="phone1" id="phone1" value="">
								</p>
							</div>
											
						</div><!-- .row -->
						<br><br>
					</div><!-- .woocommerce-billing-fields -->
					
					
				</div><!-- .col -->
				<div class="col-md-4 col-xs-12 section-last">
					<h3 id="order_review_heading" class="section-title small-spacing">Your Order</h3>
					<div class="clear"></div>
					<div id="order_review" class="woocommerce-checkout-review-order">
						<table class="shop_table woocommerce-checkout-review-order-table">
							<thead>
								<tr>
									<th class="product-name">Product</th>
									<th class="product-total">Quantity</th>
									<th class="product-total">Total</th>
								</tr>
							</thead>
							<tbody style="width:100%; max-height:100px; overflow:width:100%" >
								@php $total = 0 @endphp
								@if(!empty($cartProducts))

								
								@foreach($cartProducts as $product)
								
									

								<tr class="cart_item" style="border-bottom: 1px solid #ccc;">
									<td class="product-name"><img src="{{asset($product->image)}}" style="height: 75px;">&nbsp;&nbsp;{{$product->product_name}}</td>
									<td class="product-name"> &nbsp;&nbsp;{{$product->quantity}}</td>
									<td class="product-total"> <span class="amount">Rs. {{$product->price*$product->quantity}}</span> </td>
								</tr>
								@php $total = $total+ ($product->price*$product->quantity)  @endphp
								@endforeach
								@endif								
							</tbody>
							<tfoot>
								<tr class="cart-subtotal">
									<td colspan='2'>Subtotal products</td>
									<th><span class="amount">Rs. {{$total}}</span></th>
								</tr>
								<!-- <tr class="cart-shipping">
									<td colspan='2'>Shipping</td>
									<th><span class="amount">Rs. 0.00</span></th>
								</tr> -->
								<tr class="cart-tax">
									<td colspan='2'>Tax</td>
									<th><span class="amount">Rs. 0.00</span></th>
								</tr>
								<tr class="order-total">
									<td colspan='2'>Grand Total</td>
									<th><strong><span class="amount">Rs. {{$total}}</span></strong> </th>
								</tr>
							</tfoot>
						</table>
						<input type="hidden" name="total_bill" value="{{$total}}">
						<input type="hidden" name="member_id" value="">
						<br><br>
						<h3>Payment Mode</h3>
						<div id="payment" class="woocommerce-checkout-payment">
							<ul class="wc_payment_methods">
								<li class="wc_payment_method">
									<div class='row'>
										<div class="col-sm-2">
											<input id="payment_method_direct" type="radio" checked="checked" name="payment_method" value="cash_on_delivery" style="height: 20px; text-align: right;">
										</div>
										<div class="col-sm-10">
											<label for="payment_method_direct">Cash On Delivery</label> 
										</div>
									</div>
									
									
									
								</li>

								<li class="wc_payment_method">
									<div class='row'>
										<div class="col-sm-2">
											<input id="payment_method_direct" type="radio"  name="payment_method" value="Payumoney" style="height: 20px; text-align: right;">
										</div>
										<div class="col-sm-10">
											<label for="payment_method_direct">Payumoney</label> 
										</div>
									</div>
									
									
									
								</li>
								
								<!-- <li class="wc_payment_method">
									<input id="payment_method_payumoney" type="radio" class="input-radio" name="payment_method" value="Payumoney">
									<label for="payment_method_payumoney">PayUMoney<img src="/assetsss/images/payumoney-banner.png" alt="" /><a href="#" class="about_payumoney" title="What is PayUMoney">What is PayUMoney?</a></label> 
									<div class="payment_box">
										<p>Pay via PayUMoney; you can pay with your credit card if you don’t have a PayUMoney account.</p>
									</div>
								</li> -->
							</ul>
						</div><!-- #payment -->
						
						<div class="form-row place-order">
							<input type="submit" class="button-green btn-place-order" name="submit" id="place_order" value="Place your order" data-value="Place order">
						</div>
						
					</div><!-- #order_review -->
				</div><!-- .col -->
			</div><!-- .row -->
		</form><!-- .woocommerce-checkout -->
	</div><!-- .container -->

	@endsection
	
	@section('scripts')
	<script>
		function showShipping(){
			var check = $('#shipping_address').val();
			if($('#shipping_address').prop("checked") == true){
                $('#shipping_div').show();
            }
            else if($('#shipping_address').prop("checked") == false){
                $('#shipping_div').hide();                
            }
		}


	</script>
	@endsection