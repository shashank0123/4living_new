@extends('layouts/ecommerce2')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
        <div class="contact-area pt-85 pb-90">
            <div class="container">
                <div class="contact-info-wrap mb-50">
                    <h3>contact info</h3>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-contact-info text-center mb-30">
                                <i class="ti-location-pin"></i>
                                <h4>our address</h4>
                                <p>Hanuman chatti, Gaytri Nagar, Salasar(Rajasthan)<BR> pincode – 331506  </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-contact-info extra-contact-info text-center mb-30">
                                <ul>
                                    <li><i class="ti-mobile"></i>+91-8950603008 </li>
                                    <li><i class="ti-email"></i> <a href="mailto:info@4living.in">info@4living.in</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-contact-info text-center mb-30">
                                <i class=" ti-alarm-clock"></i>
                                <h4>openning hour</h4>
                                <p>Monday - Friday. 9:00am - 5:00pm </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="get-in-touch-wrap">
                    <h3>Get In Touch</h3>
                    <div class="contact-from contact-shadow">
                        <form id="contact-form" action="" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input name="name" type="text" placeholder="Name" id="name" >
                                    <p id="name-error"></p>
                                </div>
                                <div class="col-lg-6">
                                    <input name="email" type="email" placeholder="Email" id="email">
                                    <p id="email-error"></p>
                                </div>
                                <div class="col-lg-12">
                                    <input name="subject" type="text" placeholder="Subject" id="subject">
                                    <p id="subject-error"></p>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="message" placeholder="Your Message" id="message"></textarea>
                                    <p id="message-error"></p>
                                </div>
                                <div class="col-lg-12">
                                    <button class="submit" type="button"onclick="submitForm()">Send Message</button>
                                </div>
                            </div>
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
                <div class="contact-map pt-90">
                    <!-- <div id="map"></div> -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14126.983294256392!2d74.7174046698684!3d27.72513308928842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396b4c26f37502d7%3A0x6489f48d4eeea90!2sBalaji%20garden!5e0!3m2!1sen!2sin!4v1579613688934!5m2!1sen!2sin" width="100%" height="300px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
        <script>
            function submitForm(){
                var name = $('#name').val();
                var email = $('#email').val();
                var subject = $('#subject').val();
                var message = $('#message').val();
                
                if(name == ""){
                    $('#name-error').html('This field is required<br><br>');
                }else{
                    $('#name-error').empty();
                }
                
                if(email == ""){
                    $('#email-error').html('This field is required<br><br>');
                }else{
                    $('#email-error').empty();
                }
                
                if(subject == ""){
                    $('#subject-error').html('This field is required<br><br>');
                }else{
                    $('#subject-error').empty();
                }
                
                if(message == ""){
                    $('#message-error').html('This field is required<br><br>');
                }else{
                    $('#message-error').empty();
                }
                
            }
        </script>
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection