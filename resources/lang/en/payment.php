<?php

return [
	'sr' => 'S No.',
	'amount' => 'Total Amount',
	'received' => 'Received Amount',
	'tds' => 'TDS Charges',
	'admin' => 'Admin Charges',
	'status' => 'Status',
	'date' => 'Request Date',
    'request' => 'Request for a payment',
    'modal.title' => 'Member Detail',
    'modal.info' => 'Member Information',
    'modal.id' => 'User ID',
    'modal.join' => 'Join Date',
    'modal.package' => 'Package',
    'modal.sponsor' => 'Sponsor',
    'modal.info2' => 'Sales Information',
    'modal.left' => 'Left',
    'modal.right' => 'Right',
    'modal.carry' => 'Carry Forward',
    'modal.today' => 'Today Sales',
    'modal.total' => 'Total Sales'
];
